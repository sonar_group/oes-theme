<?php

/**
 * Allowed Gutenberg blocks
 */
add_filter('allowed_block_types_all', function ($allowed_block_types, $block_editor_context) {
    // NOTE to get a full list of registered blocks turn this function off and then in the developer tools add the code below
    //  wp.blocks.getBlockTypes().forEach(item => { console.log(item.name) })
    global $post;

    $block_types = [
        'core/paragraph',
        'core/image',
        'core/heading',
        'core/list',
        'core/code',
        'core/table',
    ];

    $custom_types = [
        'acf/apply-banner',
        'acf/banner',
        'acf/call-out-banner',
        'acf/columns',
        'acf/complex-copy',
        'acf/course-archive',
        'acf/course-archive-multiple',
        'acf/discipline-courses',
        'acf/disciplines',
        'acf/faqs',
        'acf/faqs-archive',
        'acf/footnote',
        'acf/header',
        'acf/history-archive',
        'acf/form-builder',
        'acf/forms-layout',
        'acf/icons',
        'acf/industry-insights',
        'acf/image-gallery',
        'acf/image-gallery-text',
        'acf/interactive-search',
        'acf/job-archive',
        'acf/left-right',
        'acf/levels-of-study',
        'acf/levels-of-study-courses',
        'acf/levels-of-study-infographic',
        'acf/logo-garden-archive',
        'acf/news-archive',
        'acf/parallax-page',
        'acf/portrait-carousel',
        'acf/quick-links',
        'acf/related-courses',
        'acf/related-news',
        'acf/search-courses',
        'acf/search-courses-v2',
        'acf/step-content',
        'acf/sticky-sub-nav',
        'acf/tabbed-content',
        'acf/team',
        'acf/testimonial',
        'acf/testimonial-carousel',
        'acf/tiles',
        'acf/units-archive',
        'acf/video',
    ];

    if ($post->post_type == 'course' && $post->post_parent == true) {
        $custom_types = [
            'acf/brochure-form',
            'acf/complex-copy',
            'acf/course-glance',
            'acf/course-glance-v2',
            'acf/course-overview',
            'acf/course-tab-content',
            'acf/course-tab-content-v2',
            'acf/course-testimonial',
            'acf/faqs',
            'acf/footnote',
            'acf/form-builder',
            'acf/header',
            'acf/industry-insights',
            'acf/inline-brochure-form',
            'acf/left-right',
            'acf/profiles-columns',
            'acf/related-courses',
            'acf/sticky-sub-nav',
            'acf/testimonial',
            'acf/tiles',
            'acf/vertical-accordion',
            'acf/video',
        ];
    }

    if ($post->post_type == 'unit') {
        $custom_types = [
            'acf/header',
            'acf/single-unit',
        ];
    }

    if ($post->post_type == 'faq') {
        $custom_types = [
            'acf/header',
            'acf/single-faq',
        ];
    }

    if ($post->post_type == 'team') {
        $custom_types = [
            'acf/header',
            'acf/single-team',
        ];
    }

    if ($post->post_type == 'job') {
        $custom_types = [
            'acf/header',
            'acf/single-job',
        ];
    }

    if ($post->post_type == 'history') {
        $custom_types = [
            'acf/header',
            'acf/single-history',
        ];
    }

    if ($post->post_type == 'logo_garden') {
        $custom_types = [
            'acf/header',
            'acf/single-logo-garden',
        ];
    }

    if ($post->post_type == 'form') {
        $block_types = [
            'core/heading',
            'core/paragraph',
        ];
        $custom_types = [
            'acf/form-dropdown',
            'acf/form-email',
            'acf/form-hidden',
            'acf/form-html',
            'acf/form-phone',
            'acf/form-post-type-dropdown',
            'acf/form-submit-button',
            'acf/form-text',
            'acf/form-text-area',
            'acf/form-datepicker',
            'acf/form-yes-no',
            'acf/form-dropdown-predefined',
            'acf/form-dropdown-courses',
            'acf/form-file',
            'acf/form-recaptcha',
        ];
    }

    $block_types = array_merge($block_types, $custom_types);

    return $block_types;
}, 1, 2);

/**
 * Remove Gutenberg block editor settings
 */
add_filter('block_editor_settings_all', function ($settings) {
    unset($settings['__experimentalBlockPatterns']);
    return $settings;
}, 100);

/**
 * Course Glance
 */
add_filter('sage/blocks/course-glance/data', function ($block) {
    global $post;

    $course = new SingleCourse();
    $block['fields'] = $course->fields($post->ID);

    return $block;
});

/**
 * Course Glance
 */
add_filter('sage/blocks/course-glance-v2/data', function ($block) {
    global $post;

    $course = new SingleCourse();
    $block['fields'] = $course->fields($post->ID);

    return $block;
});

/**
 * Course Overview
 */
add_filter('sage/blocks/course-overview/data', function ($block) {
    global $post;

    $course = new SingleCourse();
    $block['fields'] = $course->fields($post->ID);

    return $block;
});

/**
 * Brochure Form
 */
add_filter('sage/blocks/brochure-form/data', function ($block) {
    global $post;

    $course = new SingleCourse();
    $block['fields'] = $course->fields($post->ID);

    return $block;
});

/**
 * Inline Brochure Form
 */
add_filter('sage/blocks/inline-brochure-form/data', function ($block) {
    global $post;

    $course = new SingleCourse();
    $block['fields'] = $course->fields($post->ID);

    return $block;
});

/**
 * Team Archive
 */
add_filter('sage/blocks/team-archive/data', function ($block) {

    $team = new Team();
    $block['team'] = $team->getAllTeams();

    return $block;
});

/**
 * Job Archive
 */
add_filter('sage/blocks/job-archive/data', function ($block) {

    $job = new Job();
    $block['job'] = $job->getAllJobs();
    $block['locations'] = $job->getAllLocations();
    $block['departments'] = $job->getAllDepartments();

    return $block;
});

/**
 * FAQs Archive
 */
add_filter('sage/blocks/faqs-archive/data', function ($block) {

    $faqs = new FAQs();
    if (isset($_POST['faqs_category'])) {
        wp_verify_nonce($_POST['faqs_category']);
        $faqs->setFAQsCategory($_POST['faqs_category']);
        $block['selected_faqs_category'] = $_POST['faqs_category'];
    }

    $block['faqs'] = $faqs->getAllFAQs();
    $block['faqs_category'] = $faqs->getAllFAQsCategories();

    return $block;
});

/**
 * Units Archive
 */
add_filter('sage/blocks/units-archive/data', function ($block) {

    $units = new Units();
    $block['units'] = $units->getAllUnits();

    return $block;
});

/**
 * News Archive
 */
add_filter('sage/blocks/news-archive/data', function ($block) {

    $news = new News();
    $block['featured'] = get_field('block');
    if (!empty($block['featured']['featured_news'])) {
        $news->setFeaturedNews($block['featured']['featured_news']);
    }
    if (isset($_POST['category'])) {
        wp_verify_nonce($_POST['category']);
        $news->setCategory($_POST['category']);
        $block['selected_categories'] = $_POST['category'];
    }

    $block['news'] = $news->getAllNews();
    $block['categories'] = $news->getAllNewsCategories();

    if (!empty($block['featured']['featured_news'])) {
        $block['featured'] = $news->getFeaturedNews($block['featured']['featured_news']->ID);
    }

    return $block;
});

/**
 * Course Archive
 */
add_filter('sage/blocks/course-archive/data', function ($block) {

    $course = new Course();
    if (isset($_POST['discipline'])) {
        wp_verify_nonce($_POST['discipline']);
        $course->setDiscipline($_POST['discipline']);
        $block['selected_discipline'] = $_POST['discipline'];
    }

    $block['courses'] = $course->getAllCourses();
    $block['disciplines'] = $course->getAllDisciplines();

    return $block;
});

/**
 * Course Archive
 */
add_filter('sage/blocks/course-archive-multiple/data', function ($block) {

    $course = new Course();

    if (isset($_POST['discipline'])) {
        wp_verify_nonce($_POST['discipline']);
        $course->setDiscipline($_POST['discipline']);
        $block['selected_discipline'] = $_POST['discipline'];
    }

    if (isset($_POST['level_of_study'])) {
        wp_verify_nonce($_POST['level_of_study']);
        $course->setLevelOfStudy($_POST['level_of_study']);
        $block['selected_level_of_study'] = $_POST['level_of_study'];
    }

    $block['courses'] = $course->getAllCourses();
    $block['disciplines'] = $course->getAllDisciplines();
    $block['levels_of_study'] = $course->getAllLevelsOfStudy();
    $block['fields'] = get_field('block');

    return $block;
});

/**
 * Interactive Search Course
 */
add_filter('sage/blocks/interactive-search/data', function ($block) {

    $course = new Course();
    $block['disciplines'] = $course->getAllDisciplines();
    $block['levels_of_study'] = $course->getAllLevelsOfStudy();
    $block['fields'] = get_field('block');
    return $block;
});

/**
 * Discipline
 */
add_filter('sage/blocks/disciplines/data', function ($block) {

    $course = new Course();
    $block['disciplines'] = $course->getAllDisciplines();
    $block['fields'] = get_field('block');
    return $block;
});

/**
 * Levels of Study
 */
add_filter('sage/blocks/levels-of-study/data', function ($block) {

    $course = new Course();
    $block['levels_of_study'] = $course->getAllLevelsOfStudy();
    $block['fields'] = get_field('block');
    return $block;
});

/**
 * Discipline Courses
 */
add_filter('sage/blocks/discipline-courses/data', function ($block) {

    $course = new Course();
    $block['fields'] = get_field('block');
    if ($block['fields']['discipline']) {
        $course->setDiscipline($block['fields']['discipline']->term_id);
    }
    $block['courses'] = $course->getAllCourses();
    $block['levels_of_study'] = $course->getAllLevelsOfStudyInDiscipline();
    return $block;
});

/**
 * Levels of Study Courses
 */
add_filter('sage/blocks/levels-of-study-courses/data', function ($block) {

    $course = new Course();
    $block['fields'] = get_field('block');
    if ($block['fields']['level_of_study']) {
        $course->setLevelOfStudy($block['fields']['level_of_study']->term_id);
    }
    $block['courses'] = $course->getAllCourses();
    $block['disciplines'] = $course->getAllDisciplinesOfLevelsOfStudy();
    return $block;
});

/**
 * Search Courses
 */
add_filter('sage/blocks/search-courses/data', function ($block) {

    $course = new Course();

    $block['fields'] = get_field('block');
    $block['courses'] = $course->getAllCourses();
    $block['disciplines'] = $course->getAllDisciplines();
    $block['disciplines_name'] = '';
    foreach ($block['disciplines'] as $value) {
        $block['disciplines_name'] = $block['disciplines_name'] . $value->name . '|';
    }
    $block['disciplines_name'] = rtrim($block['disciplines_name'], '|');
    $block['courses_name'] = '';
    foreach ($block['courses'] as $value) {
        $block['courses_name'] = $block['courses_name'] . $value['title'] . '|';
    }
    $block['courses_name'] = rtrim($block['courses_name'], '|');
    return $block;
});

/**
 * Search Courses V2
 */
add_filter('sage/blocks/search-courses-v2/data', function ($block) {

    $course = new Course();

    $block['fields'] = get_field('block');
    $block['courses'] = $course->getAllCourses();
    $block['disciplines'] = $course->getAllDisciplines();
    $block['disciplines_name'] = '';
    foreach ($block['disciplines'] as $value) {
        $block['disciplines_name'] = $block['disciplines_name'] . $value->name . '|';
    }
    $block['disciplines_name'] = rtrim($block['disciplines_name'], '|');
    $block['courses_name'] = '';
    foreach ($block['courses'] as $value) {
        $block['courses_name'] = $block['courses_name'] . $value['title'] . '|';
    }
    $block['courses_name'] = rtrim($block['courses_name'], '|');
    return $block;
});

/**
 * Format rendered blocks
 */
add_filter('render_block', function ($block_content, $block) {

    $output = $block_content;
    $block_types = [
        'core/paragraph',
        'core/image',
        'core/heading',
        'core/list',
        'core/code',
        'core/table',
    ];

    if (in_array($block['blockName'], $block_types)) {
        $output = '<section class="block-content">';
        $output .= '<div class="container">';
        $output .= $block_content;
        $output .= '</div>';
        $output .= '</section>';
    }

    return $output;
}, 10, 3);

/**
 * Add Gutenberg block categories
 */
add_filter('block_categories_all', function ($categories, $post) {
    return array_merge(
        array(
            array(
                'slug' => 'course-blocks',
                'title' => __('Course Blocks', 'course-blocks'),
            ),
            array(
                'slug' => 'blocks',
                'title' => __('General Blocks', 'blocks'),
            ),
            array(
                'slug' => 'unit-blocks',
                'title' => __('Unit Blocks', 'blocks'),
            ),
            array(
                'slug' => 'faq-blocks',
                'title' => __('FAQ Blocks', 'blocks'),
            ),
            array(
                'slug' => 'team-blocks',
                'title' => __('Team Blocks', 'blocks'),
            ),
            array(
                'slug' => 'job-blocks',
                'title' => __('Job Blocks', 'blocks'),
            ),
            array(
                'slug' => 'form-fields',
                'title' => __('Form Fields', 'form-fields'),
            ),
        ),
        $categories
    );
}, 10, 2);

/**
 * Add Form Blocks
 */
add_filter('sage-acf-gutenberg-blocks-templates', function ($folders) {
    $folders[] = 'views/form-fields';
    return $folders;
});

/**
 * Add Form Data to Form Blocks
 */
add_filter('sage/blocks/form-text/data', 'addFormData');
add_filter('sage/blocks/form-text-area/data', 'addFormData');
add_filter('sage/blocks/form-email/data', 'addFormData');
add_filter('sage/blocks/form-phone/data', 'addFormData');
add_filter('sage/blocks/form-hidden/data', 'addFormData');
add_filter('sage/blocks/form-html/data', 'addFormData');
add_filter('sage/blocks/form-dropdown/data', 'addFormData');
add_filter('sage/blocks/form-post-type-dropdown/data', 'addFormData');
add_filter('sage/blocks/form-submit-button/data', 'addFormData');
add_filter('sage/blocks/form-datepicker/data', 'addFormData');
add_filter('sage/blocks/form-yes-no/data', 'addFormData');
add_filter('sage/blocks/form-dropdown-courses/data', 'addFormData');
add_filter('sage/blocks/form-dropdown-predefined/data', 'addFormData');
add_filter('sage/blocks/form-file/data', 'addFormData');
function addFormData($block)
{
    global $form_id;

    $form = Form::getData($form_id);

    $block['form'] = [
        'post_id' => $form_id,
        'id' => $form['form_id'],
        'class' => $form['form_class'],
    ];

    return $block;
}
