<?php

namespace App\Modules;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use App;

//use function Env\env;

class FormProcessor extends BaseSingleton
{

    public function __construct()
    {
        add_action('wp_ajax_form_builder_submit', [$this, 'formBuilderProcess']);
        add_action('wp_ajax_nopriv_form_builder_submit', [$this, 'formBuilderProcess']);

        add_action('wp_ajax_form_builder_file_upload', [$this, 'fileUploadProcess']);
        add_action('wp_ajax_nopriv_form_builder_file_upload', [$this, 'fileUploadProcess']);

        add_action( 'wp_enqueue_scripts', [$this,'intlTelInputScripts'] );
    }

    public function intlTelInputScripts() {
        // intl-tel-input
        wp_enqueue_style('intl-tel-input', 'https://cdn.jsdelivr.net/npm/intl-tel-input@19.5.6/build/css/intlTelInput.css', false, null);
    }

    private function verifyRecaptcha()
    {
        $recaptcha = App::recaptcha();

        $token = null;
        if (isset($_POST['g-recaptcha-response'])) {
            $token = $_POST['g-recaptcha-response'];
        }

        if ($token === null) {
            echo json_encode([
                'success' => false,
                'message' => 'Recaptcha token not present',
                'data' => $_POST
            ]);
            die;
        }

        $client = new Client([
            'base_uri' => 'https://www.google.com/recaptcha/api/siteverify'
        ]);

        try {
            $response = $client->request('POST', '', ['form_params' => [
                'secret' => $recaptcha['secret_key'],
                'response' => $token,
                // 'remoteip' =>
            ]]);
        } catch (\Exception $e) {
            echo json_encode([
                'success' => false,
                'message' => 'Recaptcha Validation Failed',
                'exception' => $e->getMessage(),
            ]);
            die;
        }

        $data = json_decode($response->getBody()->getContents(), true);

        if (!$data['success']) {
            echo json_encode([
                'success' => false,
                'message' => 'Recaptcha Validation Failed',
                'response' => $data,
            ]);
        }

        return $data;
    }

    private function sendLead($data)
    {
//        $form = get_post($data['form_id']);
        $form_settings = get_field('settings', 'options');
        $form_fields = get_fields($data['form_id']);

        unset($data['action']);
        unset($data['form_id']);
        unset($data['submit']);

        // process default data
        $other_data = [];
        $other_data['oid'] = $form_settings['oid'] != false ? $form_settings['oid'] : '';
        $retURL = $form_fields['settings'] != false && $form_fields['settings']['form_return_url'] != false ? $form_fields['settings']['form_return_url'] : '';
        $other_data['retURL'] = $retURL;
        if ($form_settings['debug_mode'] === true) {
            $other_data['debug'] = 1;
            $other_data['debugEmail'] = $form_settings['debug_email'] != false ? $form_settings['debug_email'] : '';
        }
        if ($form_fields['settings']['hidden_fields'] != false && !empty($form_fields['settings']['hidden_fields'])) {
            foreach ($form_fields['settings']['hidden_fields'] as $field) {
                $other_data[$field['name']] = $field['value'];
            }
        }

        $data = array_merge($other_data, $data);

        // format data for email notification
        $details = '';
        foreach ($data as $key => $value) {
            // escape input data
            if( $key != 'g-recaptcha-response') {
                $value = htmlspecialchars($value);
            }
            $details .= $key . ": " . $value . "\r\n";
        }

        // process file uploads
        if (session_id() && isset($_SESSION['form_file_upload'])) {
            $session_data = $_SESSION['form_file_upload'];
            $details .= $session_data['name'] . ': ' . $session_data['id'] . "\r\n";

            $details .= 'Files: ';
            foreach ($session_data['files'] as $id => $files) {
                $details .= implode(', ', $files) . ', ';
            }
            $data[$session_data['name']] = $session_data['id'];
        }

        // Add ip addresses
        $data[App::getServerFieldID()] = App::getServerIp();
        $data['00N2v00000YLJ6f'] = App::getClientIpAddress();

        $endpoint_url = '';
        if (strlen($form_settings['endpoint_url']) > 0) {
            $endpoint_url = $form_settings['endpoint_url'];
        }
        if ($form_fields['settings']['endpoint_url'] != false && strlen($form_fields['settings']['endpoint_url']) > 0) {
            $endpoint_url = $form_fields['settings']['endpoint_url'];
        }

        // Get the current Unix timestamp in seconds
        $timestamp_seconds = time();

        // Get the current microseconds
        list($microseconds, $seconds) = explode(' ', microtime());
        $microseconds = str_pad(round($microseconds * 1000), 3, '0', STR_PAD_RIGHT); // Convert microseconds to milliseconds and ensure 3 digits

        // Concatenate the timestamp in seconds with microseconds
        $time = $timestamp_seconds . $microseconds;

        $data['captcha_settings'] = json_encode([
            'keyname' => $form_settings['keyname']? $form_settings['keyname']: 'recaptcha_v2',
            'fallback' => 'true',
            'orgId' => $other_data['oid'],
            'ts' => $time
        ]);


        $client = new Client([
            'base_uri' => $endpoint_url
        ]);

        // Convert form data to a query string
        //$form_data_string = http_build_query($data);

        try {
            $response = $client->request('POST', '', [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                //'body' => $form_data_string
                'form_params' => $data
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                'success' => false,
                'message' => 'SF Submission Failed',
                'data' => $data,
                'exception' => $e->getMessage(),
            ]);

            //NOTE extra logs, can be deleted later
            $logging = [
                'session_id' => session_id(),
                'session_data' => $_SESSION['form_file_upload'],
                '$details' => $details,
                '$data' => $data,
                'message' => $e->getMessage()
            ];
            error_log('OES - Form submission FAILED. ' . print_r($logging, true));
            die;
        }
        if ($response->getStatusCode() !== 200) {
            echo json_encode([
                'success' => false,
                'message' => 'SF Submission Failed',
                'data' => $data,
                'response' => $response->getBody(),
            ]);

            //NOTE extra logs, can be deleted later
            $logging = [
                'session_id' => session_id(),
                'session_data' => $_SESSION['form_file_upload'],
                '$details' => $details,
                '$data' => $data,
                'body' => $response->getBody(),
            ];
            error_log('OES - Form submission FAILED (2). ' . print_r($logging, true));

            echo $response->getBody();
            die;
        }

        //NOTE extra logs, can be deleted later
        $logging = [
            'session_id' => session_id(),
            'session_data' => $_SESSION['form_file_upload'],
            '$details' => $details,
            '$data' => $data
        ];
        error_log('OES - Form submission SUCCESS. ' . print_r($logging, true));

        // reset the session
        unset($_SESSION['form_file_upload']);
        echo json_encode([
            'success' => true,
            'message' => 'SUCCESS',
            'data' => $data,
            'response' => $response->getBody(),
            'redirect' => $retURL
        ]);
        die;
    }

    public function formBuilderProcess()
    {
//        $this->verifyRecaptcha();
        $data = $_POST;
        $this->sendLead($data);
    }

    public function fileUploadProcess()
    {
        check_ajax_referer('form_builder_file_upload', 'security');

        $s3 = new S3Client([
            'credentials' => array(
                'key' => env('AWS_S3_KEY'),
                'secret' => env('AWS_S3_SECRET')
            ),
            'version' => 'latest',
            'region' => env('AWS_S3_REGION')
        ]);

        // initialise session id
        if (!isset($_SESSION['form_file_upload'])) {
            $files_id = Carbon::now()->format('YmdHis') . rand(0, 100);
            $_SESSION['form_file_upload'] = [
                'id' => $files_id,
                'name' => $_POST['name'],
                'files' => []
            ];
        } else {
            $files_id = $_SESSION['form_file_upload']['id'];
        }

        // initialise field id
        $field_id = $_POST['id'];
        if (!isset($_SESSION['form_file_upload']['files'][$field_id])) {
            $_SESSION['form_file_upload']['files'][$field_id] = [];
        }

        foreach ($_FILES['file']['name'] as $index => $file_name) {
            $full_path = $files_id . '_' . $file_name;
            try {
                $s3->putObject([
                    'Bucket' => env('AWS_S3_BUCKET'),
                    'Key' => $full_path,
                    'SourceFile' => $_FILES['file']['tmp_name'][$index],
                ]);
                $_SESSION['form_file_upload']['files'][$field_id][] = $file_name;

                //NOTE extra logs, can be deleted later
                $upload_details = [
                    'function' => 'FormProcessor::fileUploadProcess()',
                    'Bucket' => env('AWS_S3_BUCKET'),
                    'Key' => $full_path,
                    'SourceFile' => $_FILES['file']['tmp_name'][$index],
                    'field_id' => $field_id,
                    'file_name' => $file_name,
                    'session_id' => session_id(),
                    'session_data' => $_SESSION['form_file_upload'],
                ];
                error_log('OES - File upload SUCCESS. ' . print_r($upload_details, true));
            } catch (S3Exception|\Exception $e) {
                //NOTE extra logs, can be deleted later
                $upload_details = [
                    'function' => 'FormProcessor::fileUploadProcess()',
                    'Bucket' => env('AWS_S3_BUCKET'),
                    'Key' => $full_path,
                    'SourceFile' => $_FILES['file']['tmp_name'][$index],
                    'field_id' => $field_id,
                    'file_name' => $file_name,
                    'session_id' => session_id(),
                    'session_data' => $_SESSION['form_file_upload'],
                    'message' => $e->getMessage(),
                ];
                error_log('OES - File upload FAILED. ' . print_r($upload_details, true));

                echo json_encode([
                    'success' => false,
                    'message' => 'FAIL',
                    'data' => $e->getMessage()
                ]);
                die;
            }
        }

        echo json_encode([
            'success' => true,
            'message' => 'SUCCESS',
            'data' => $_SESSION['form_file_upload']['files'][$field_id],
        ]);
        die;
    }
}
