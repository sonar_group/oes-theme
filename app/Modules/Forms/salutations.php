<?php

namespace App\Modules;

return [
    [
        "label" => "Prof.",
        "value" => "Prof."
    ],
    [
        "label" => "Mr",
        "value" => "Mr"
    ],
    [
        "label" => "Dr",
        "value" => "Dr"
    ],
    [
        "label" => "Ms",
        "value" => "Ms"
    ],
    [
        "label" => "Mrs",
        "value" => "Mrs"
    ],
    [
        "label" => "Miss",
        "value" => "Miss"
    ],
    [
        "label" => "Mx",
        "value" => "Mx"
    ],
    [
        "label" => "Reverend",
        "value" => "Reverend"
    ],
];
