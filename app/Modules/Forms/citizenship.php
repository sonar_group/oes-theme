<?php

namespace App\Modules;

return [
    [
        "value" => "Australian Citizen",
        "label" => "Australian Citizen"
    ],
    [
        "value" => "Australian Permanent Resident Visa - Humanitarian",
        "label" => "Australian Permanent Resident Visa - Humanitarian"
    ],
    [
        "value" => "Australian Permanent Resident Visa - Non-Humanitarian",
        "label" => "Australian Permanent Resident Visa - Non-Humanitarian"
    ],
    [
        "value" => "NZ citizen",
        "label" => "NZ citizen"
    ],
    [
        "value" => "International Student Residing Overseas",
        "label" => "International Student Residing Overseas"
    ],
    [
        "value" => "International Student Residing in Australia",
        "label" => "International Student Residing in Australia"
    ],
];
