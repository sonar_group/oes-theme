<?php

namespace App\Modules;

return [
    [
        "label" => "Postgraduate qualification (e.g. Postgraduate Diploma, Masters, PHD)",
        "value" => "Postgraduate qualification (e.g. Postgraduate Diploma, Masters, PHD)"
    ],
    [
        "label" => "Bachelor Degree",
        "value" => "Bachelor Degree"
    ],
    [
        "label" => "Other post school qualification (e.g. VET Certificate, Associate Degree or Diploma)",
        "value" => "Other post school qualification (e.g. VET Certificate, Associate Degree or Diploma)"
    ],
    [
        "label" => "Completed Year 12 schooling or equivalent",
        "value" => "Completed Year 12 schooling or equivalent"
    ],
    [
        "label" => "Did not complete Year 12 schooling or equivalent",
        "value" => "Did not complete Year 12 schooling or equivalent"
    ],
    [
        "label" => "Completed Year 10 schooling or equivalent",
        "value" => "Completed Year 10 schooling or equivalent"
    ],
    [
        "label" => "Did not complete Year 10 schooling or equivalent",
        "value" => "Did not complete Year 10 schooling or equivalent"
    ],
    [
        "label" => "Don't know",
        "value" => "Don't know"
    ]
];