<?php

namespace App\Modules;

return [
    [
        "label" => "Doctoral Degree",
        "value" => "Doctoral Degree"
    ],
    [
        "label" => "Master Degree",
        "value" => "Master Degree"
    ],
    [
        "label" => "Graduate Diploma or Graduate Certificate",
        "value" => "Graduate Diploma or Graduate Certificate"
    ],
    [
        "label" => "Bachelor Degree",
        "value" => "Bachelor Degree"
    ],
    [
        "label" => "Advanced Diploma and Associate Degree",
        "value" => "Advanced Diploma and Associate Degree"
    ],
    [
        "label" => "Diploma",
        "value" => "Diploma"
    ],
    [
        "label" => "Certificate IV",
        "value" => "Certificate IV"
    ],
    [
        "label" => "Certificate III",
        "value" => "Certificate III"
    ],
    [
        "label" => "Certificate II",
        "value" => "Certificate II"
    ],
    [
        "label" => "Certificate I",
        "value" => "Certificate I"
    ],
    [
        "label" => "None of the above",
        "value" => "None of the above"
    ]
];