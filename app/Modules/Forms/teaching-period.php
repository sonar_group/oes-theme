<?php

namespace App\Modules;

return [
    [
        "value" => "TP5",
        "label" => "TP5 Starting 29/8/2022"
    ],
    [
        "value" => "TP6",
        "label" => "TP6 Starting 24/10/2022"
    ],
    [
        "value" => "TP1",
        "label" => "TP1 Starting 10/01/2023"
    ],
    [
        "value" => "TP2",
        "label" => "TP2 Starting 7/03/2023"
    ],
    [
        "value" => "TP3",
        "label" => "TP3 Starting 9/5/2023"
    ],
    [
        "value" => "TP4",
        "label" => "TP4 Starting 4/7/2023"
    ]
];
