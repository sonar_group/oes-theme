<?php

namespace App\Modules;

return [
    [
        "label" => "Year 9",
        "value" => "Year 9"
    ],
    [
        "label" => "Year 10",
        "value" => "Year 10"
    ],
    [
        "label" => "Year 11",
        "value" => "Year 11"
    ],
    [
        "label" => "Year 12",
        "value" => "Year 12"
    ],
    [
        "label" => "Other year level, or Highest level of school is unknown",
        "value" => "Other year level, or Highest level of school is unknown"
    ],
];