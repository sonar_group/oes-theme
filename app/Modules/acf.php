<?php

namespace App\Modules;

use App;

// add parent
$parent = acf_add_options_page(array(
    'page_title' => 'Global Settings',
    'menu_title' => 'Global Settings',
    'position' => 70,
    'redirect' => true
));

acf_add_options_sub_page(array(
    'page_title' => 'Global Settings',
    'menu_title' => 'Global',
    'parent_slug' => $parent['menu_slug'],
));

/**
 * ACF Google Map Custom Field.
 */

add_filter('acf/fields/google_map/api', function ($api) {
    $google = App::google();

    if (is_array($google) && array_key_exists('api_key', $google)) {
        $api['key'] = $google['api_key'];
    }


    return $api;
});

/**
 * Properly load both child and parent blocks
 */
add_filter('sage-acf-gutenberg-blocks-templates', function ($directory) {
    add_filter('doing_it_wrong_trigger_error', function () { return false; });
    $directory[] = '../../oes-theme/resources/views/blocks';
    return $directory;
}, 99);
