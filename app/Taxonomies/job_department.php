<?php

namespace App\Taxonomies;

/*
 * Departments Taxonomy
 */
function register_taxonomy_job_departments()
{
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name' => _x('Departments', 'Taxonomy General Name', 'text_domain'),
        'singular_name' => _x('Department', 'Taxonomy Singular Name', 'text_domain'),
        'search_items' => __('Search Departments', 'text_domain'),
        'all_items' => __('All Departments', 'text_domain'),
        'parent_item' => __('Parent Department', 'text_domain'),
        'parent_item_colon' => __('Parent Department:', 'text_domain'),
        'edit_item' => __('Edit Department', 'text_domain'),
        'update_item' => __('Update Department', 'text_domain'),
        'add_new_item' => __('Add New Department', 'text_domain'),
        'new_item_name' => __('New Department Name', 'text_domain'),
        'menu_name' => __('Department', 'text_domain'),
    );

    $args = array(
        'hierarchical' => false,
        'public' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => array(
            'slug' => '/career',
            'with_front' => false
        ),
    );

    register_taxonomy('job_department', array('job'), $args);
}

add_action('init', 'App\Taxonomies\register_taxonomy_job_departments', 0);
