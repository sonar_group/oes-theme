<?php

namespace App\Taxonomies;

/*
 * Departments Taxonomy
 */
function register_taxonomy_faq_categories()
{
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name' => _x('Category', 'Taxonomy General Name', 'text_domain'),
        'singular_name' => _x('Category', 'Taxonomy Singular Name', 'text_domain'),
        'search_items' => __('Search Category', 'text_domain'),
        'all_items' => __('All Category', 'text_domain'),
        'parent_item' => __('Parent Category', 'text_domain'),
        'parent_item_colon' => __('Parent Category:', 'text_domain'),
        'edit_item' => __('Edit Category', 'text_domain'),
        'update_item' => __('Update Category', 'text_domain'),
        'add_new_item' => __('Add New Category', 'text_domain'),
        'new_item_name' => __('New Category Name', 'text_domain'),
        'menu_name' => __('Category', 'text_domain'),
    );

    $args = array(
        'hierarchical' => false,
        'public' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => array(
            'slug' => '/faqs',
            'with_front' => false
        ),
    );

    register_taxonomy('faq_category', array('faq'), $args);
}

add_action('init', 'App\Taxonomies\register_taxonomy_faq_categories', 0);
