<?php

namespace App\Taxonomies;

/*
 * Disciplines Taxonomy
 */
function register_taxonomy_disciplines()
{
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name' => _x('Disciplines', 'Taxonomy General Name', 'text_domain'),
        'singular_name' => _x('Discipline', 'Taxonomy Singular Name', 'text_domain'),
        'search_items' => __('Search Disciplines', 'text_domain'),
        'all_items' => __('All Disciplines', 'text_domain'),
        'parent_item' => __('Parent Discipline', 'text_domain'),
        'parent_item_colon' => __('Parent Discipline:', 'text_domain'),
        'edit_item' => __('Edit Discipline', 'text_domain'),
        'update_item' => __('Update Discipline', 'text_domain'),
        'add_new_item' => __('Add New Discipline', 'text_domain'),
        'new_item_name' => __('New Discipline Name', 'text_domain'),
        'menu_name' => __('Discipline', 'text_domain'),
    );

    $args = array(
        'hierarchical' => false,
        'public' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => array(
            'slug' => '/online-courses',
            'with_front' => false
        ),
    );

    register_taxonomy('discipline', array('course'), $args);
}

add_action('init', 'App\Taxonomies\register_taxonomy_disciplines', 0);
