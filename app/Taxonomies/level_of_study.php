<?php

namespace App\Taxonomies;

/*
 * Disciplines Taxonomy
 */
function register_taxonomy_level_of_study()
{
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name' => _x('Level of Study', 'Taxonomy General Name', 'text_domain'),
        'singular_name' => _x('Level of Study', 'Taxonomy Singular Name', 'text_domain'),
        'search_items' => __('Search Levels of Study', 'text_domain'),
        'all_items' => __('All Levels of Study', 'text_domain'),
        'parent_item' => __('Parent Level of Study', 'text_domain'),
        'parent_item_colon' => __('Parent Level of Study:', 'text_domain'),
        'edit_item' => __('Edit Level of Study', 'text_domain'),
        'update_item' => __('Update Level of Study', 'text_domain'),
        'add_new_item' => __('Add New Level of Study', 'text_domain'),
        'new_item_name' => __('New Level of Study Name', 'text_domain'),
        'menu_name' => __('Level of Study', 'text_domain'),
    );

    $args = array(
        'hierarchical' => false,
        'public' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => array(
            'slug' => '/online-courses',
            'with_front' => false
        ),
    );

    register_taxonomy('level_of_study', array('course', 'calendar'), $args);
}

add_action('init', 'App\Taxonomies\register_taxonomy_level_of_study', 0);
