<?php

namespace App\Taxonomies;

/*
 * Locations Taxonomy
 */
function register_taxonomy_job_locations()
{
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name' => _x('Locations', 'Taxonomy General Name', 'text_domain'),
        'singular_name' => _x('Location', 'Taxonomy Singular Name', 'text_domain'),
        'search_items' => __('Search Locations', 'text_domain'),
        'all_items' => __('All Locations', 'text_domain'),
        'parent_item' => __('Parent Location', 'text_domain'),
        'parent_item_colon' => __('Parent Location:', 'text_domain'),
        'edit_item' => __('Edit Location', 'text_domain'),
        'update_item' => __('Update Location', 'text_domain'),
        'add_new_item' => __('Add New Location', 'text_domain'),
        'new_item_name' => __('New Location Name', 'text_domain'),
        'menu_name' => __('Location', 'text_domain'),
    );

    $args = array(
        'hierarchical' => false,
        'public' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => array(
            'slug' => '/career',
            'with_front' => false
        ),
    );

    register_taxonomy('job_location', array('job'), $args);
}

add_action('init', 'App\Taxonomies\register_taxonomy_job_locations', 0);
