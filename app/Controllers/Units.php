<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class Units extends Controller
{

    protected $acf = true;
    public $units_category = null;

    public function __construct()
    {
        add_action('wp_ajax_nopriv_units_archive', [$this, 'unitsArchiveAjax']);
        add_action('wp_ajax_units_archive', [$this, 'unitsArchiveAjax']);
    }

    /**
     * All News
     *
     * @param null $id
     * @return array
     */
    public function getAllUnits()
    {
        $args = [
            'post_type' => 'unit',
            'post_status' => ['publish'],
            'posts_per_page' => -1,
        ];
        if (!empty($this->units_category)) {
            $args['tax_query'] = [
                [
                    'taxonomy' => 'unit_category',
                    'field' => 'term_id',
                    'terms' => intval($this->units_category),
                ]
            ];
        }

        $units = new WP_Query($args);
        $units_data = [];

        if ($units->have_posts()) {
            while ($units->have_posts()) {
                $units->the_post();
                $units_data[] = [
                    'id' => get_the_ID(),
                    'permalink' => get_the_permalink(get_the_ID()),
                    'title' => strval(get_the_title()),
                    'content' => get_post(get_the_ID()),
                ];

            }
        }
        return $units_data;
    }

    public static function unitsArchiveAjax()
    {
        $queried_units_keyword = (isset($_POST['units_keyword'])) ? $_POST['units_keyword'] : '';

        header("Content-Type: text/html");

        $args = array(
            'post_type' => 'unit',
            'post_status' => ['publish'],
            'posts_per_page' => -1,
        );

        if (!empty($queried_units_keyword)) {
            $args['s'] = $queried_units_keyword;
        }

        $loop = new WP_Query($args);

        $out = '';

        if ($loop->have_posts()) :
            while ($loop->have_posts()) :
                $loop->the_post();
                global $post;

                $blocks = App::parseBlock(get_post($post->ID), 'single-unit');
                if ($blocks) {
                    foreach ($blocks as $unit) {

                        $out .= '<a class="col-6 col-md-4" data-toggle="modal" data-target="#unitModal_' . $i . '_' . $unit['block_code'] . '">';
                        $out .= '<h3>' . $unit['block_name'] .'</h3><br />';
                        $out .= '<p>' . $unit['block_description'] .'</p><br />';
                        $out .= '<p><strong>' . $unit['block_code'] .'</strong></p><br />';
                        $out .= '</a>';
                    }
                }

            endwhile;

        endif;
        wp_reset_postdata();
        die($out);
    }
}
