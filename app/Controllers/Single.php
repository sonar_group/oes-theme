<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class Single extends Controller
{

    protected $acf = true;

    /**
     * Related News
     *
     * @param null $id
     * @return array
     */
    public function relatedNews($id = null)
    {
        $args = [
            'post_type' => 'post',
            'post_status' => ['publish'],
            'post__not_in' => array(get_the_ID()),
            'posts_per_page' => 3,
            'no_found_rows' => 'true',
            'orderby' => ['date'],
            'order' => 'DESC',
            'tax_query' => [
                [
                    'taxonomy' => 'category',
                    'field' => 'id',
                    'terms' => (get_the_terms($id, 'category')) ? get_the_terms($id, 'category')[0]->term_id : '',
                ]
            ]
        ];

        $news = new WP_Query($args);
        $news_data = [];

        if ($news->have_posts()) {
            while ($news->have_posts()) {
                $news->the_post();
                $news_data[] = [
                    'id' => get_the_ID(),
                    'permalink' => get_the_permalink(get_the_ID()),
                    'title' => strval(get_the_title()),
                    'image' => get_the_post_thumbnail_url(get_the_ID(), 'full'),
                    'term' => get_the_terms(get_the_ID(), 'category')[0]->name,
                    'read_time' => get_field('read_time', get_the_ID()),
                ];
            }
            wp_reset_postdata();
        }
        return $news_data;
    }

    /**
     * Fields
     *
     * @param null $id
     * @return array
     */
    public function fields($id = null)
    {
        return [
            'excerpt' => get_the_excerpt($id),
            'related_topics' => get_field('related_topics', $id),
            'read_time' => get_field('read_time', $id),
            'has_form' => get_field('has_form', $id),
            'forms_layout' => get_field('forms_layout', $id),
            'has_tiles' => get_field('has_tiles', $id),
            'tiles' => get_field('tiles', $id),
        ];
    }
}
