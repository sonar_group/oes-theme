<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class Form extends Controller
{
    protected $acf = true;


    /**
     * Get Data
     *
     * @param $id
     * @return mixed
     */
    public static function getData($id)
    {
        return get_field('settings', $id);
    }

    /**
     * Get Hidden Field
     *
     * @param $field
     * @return bool|false|mixed|string
     */
    public static function getHiddenField($field)
    {
        global $post;

        $return = false;

        switch ($field) {
            case 'title':
                $return = get_the_title($post->ID);
                break;
            case 'url':
                $return = get_the_permalink($post->ID);
                break;

            case 'course_code':
                $return = get_field('course_code', $post->ID);
                break;

            case 'course_discipline':
                $term = get_the_terms($post->ID, 'discipline');

                if ($term) {
                    $return = array_pop($term)->name;
                }
                break;

        }

        return $return;

    }

}
