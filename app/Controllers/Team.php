<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class Team extends Controller
{

    protected $acf = true;

    /**
     * All Team
     *
     * @param null $id
     * @return array
     */
    public function getAllTeams()
    {
        $query = new \WP_Query([
            'post_type' => 'team',
            'post_status' => ['publish'],
            'nopaging' => true,
        ]);

        $posts = [];

        foreach($query->posts as $post) {
            $posts[] = [
                'title' => get_the_title($post->ID),
                'name' => '',
                'role' => '',
                'shortBio' => '',
                'longBio' => '',
                'image' => ''
            ];
        }

        return $posts;
    }
}
