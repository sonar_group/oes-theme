<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleCourse extends Controller
{

    protected $acf = true;

    /**
     * Fields
     *
     * @param null $id
     * @return array
     */
    public function fields($id = null)
    {
        return [
            'course_code' => get_field('course_code', $id),
            'show_in_forms' => get_field('show_in_forms', $id),
            'fees_table' => get_field('fees_table', $id),
            'sticky_bar' => get_field('sticky_bar_course', $id),
            'inpage' => get_field('inpage', $id),
            'course_compare' => get_field('course_compare', $id),
            'course_units' => get_field('course_units', $id),
            'forms' => get_field('forms', 'options'),
            'course_options' => get_field('course', 'options'),
            'sticky_bar_global' => get_field('sticky_bar', 'options'),
        ];
    }
}
