<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class FAQs extends Controller
{

    protected $acf = true;
    public $faqs_category = null;

    public function __construct()
    {
        add_action('wp_ajax_nopriv_faqs_archive', [$this, 'faqsArchiveAjax']);
        add_action('wp_ajax_faqs_archive', [$this, 'faqsArchiveAjax']);
    }

    /**
     * All News
     *
     * @param null $id
     * @return array
     */
    public function getAllFAQs()
    {
        $args = [
            'post_type' => 'faq',
            'post_status' => ['publish'],
            'posts_per_page' => -1,
        ];
        if (!empty($this->faqs_category)) {
            $args['tax_query'] = [
                [
                    'taxonomy' => 'faq_category',
                    'field' => 'term_id',
                    'terms' => intval($this->faqs_category),
                ]
            ];
        }

        $faqs = new WP_Query($args);
        $faqs_data = [];

        if ($faqs->have_posts()) {
            while ($faqs->have_posts()) {
                $faqs->the_post();
                $faqs_data[] = [
                    'id' => get_the_ID(),
                    'permalink' => get_the_permalink(get_the_ID()),
                    'title' => strval(get_the_title()),
                    'term' => !empty(get_the_terms(get_the_ID(), 'faq_category')[0]->name) ? get_the_terms(get_the_ID(), 'faq_category')[0]->name : '',
                    'content' => get_post(get_the_ID()),
                ];

            }
        }
        return $faqs_data;
    }

    /**
     * @return array
     */
    public static function getAllFAQsCategories()
    {
        $faqs_categories = get_terms('faq_category');
        return $faqs_categories;
    }

    public function setFAQsCategory($faqs_category)
    {
        $this->faqs_category = $faqs_category;
    }

    public static function faqsArchiveAjax()
    {
        $queried_faqs_category = (isset($_POST['faqs_category'])) ? $_POST['faqs_category'] : '';

        header("Content-Type: text/html");

        $args = array(
            'post_type' => 'faq',
            'post_status' => ['publish'],
            'posts_per_page' => -1,
        );

        if (!empty($queried_faqs_category)) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'faq_category',
                    'field' => 'term_id',
                    'terms' => intval($queried_faqs_category),
                )
            );
        }

        $loop = new WP_Query($args);

        $out = '';
        $i = 0;

        if ($loop->have_posts()) :

            $out .= '<div class="col-12">';
            $out .= '<div class="accordion faqs-group" id="faqsAccordion_archive">';

            while ($loop->have_posts()) :
                $loop->the_post();
                global $post;

                $blocks = App::parseBlock(get_post($post->ID), 'single-faq');
                if ($blocks) {
                    foreach ($blocks as $faq) {

                        $out .= '<div class="card">';
                        $out .= '<div class="card-header" id="heading_archive_' . $i . '" data-toggle="collapse" data-target="#collapse_archive_' . $i . '" aria-expanded="true" aria-controls="collapse_archive_' . $i . '">';
                        $out .= '<h5>' . $faq['block_question'] . '</h5>';
                        $out .= '</div>';

                        $out .= '<div id="collapse_archive_' . $i . '" class="collapse" aria-labelledby="heading_archive_' . $i . '" data-parent="#faqsAccordion_archive">';
                        $out .= '<div class="card-body">' . $faq['block_answer'] . '</div>';
                        $out .= '</div>';
                        $out .= '</div>';

                        $i++;
                    }
                }

            endwhile;

            $out .= '</div>';
            $out .= '</div>';

        endif;
        wp_reset_postdata();
        die($out);
    }
}
