<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class Job extends Controller
{

    protected $acf = true;

    public function __construct()
    {
        add_action('wp_ajax_nopriv_job_archive', [$this, 'jobArchiveAjax']);
        add_action('wp_ajax_job_archive', [$this, 'jobArchiveAjax']);
    }

    /**
     * All Team
     *
     * @param null $id
     * @return array
     */
    public function getAllJobs()
    {
        $query = new \WP_Query([
            'post_type' => 'job',
            'post_status' => ['publish'],
            'nopaging' => true,
            'orderby' => 'publish_date',
            'order' => 'DESC',
        ]);

        $posts = [];

        foreach($query->posts as $post) {
            $posts[] = [
                'id' => $post->ID,
                'content' => get_post($post->ID),
            ];
        }

        return $posts;
    }

    /**
     * All Departments
     *
     * @param null $id
     * @return array
     */
    public function getAllDepartments() {
        return get_terms('job_department');
    }

    /**
     * All Locations
     *
     * @param null $id
     * @return array
     */
    public function getAllLocations() {
        return get_terms('job_location');
    }

    /**
     * Ajax callback for Job Archive block filters
     *
     * @param null
     * @return string
     */
    public static function jobArchiveAjax()
    {
        $queried_location = (isset($_POST['location'])) ? $_POST['location'] : '';
        $queried_department = (isset($_POST['department'])) ? $_POST['department'] : '';
        $queried_date = (isset($_POST['date'])) ? $_POST['date'] : '';

        header("Content-Type: text/html");

        if (!empty($queried_date) && $queried_date == 'sort-closing-date') {
            $args = array(
                'post_type' => 'job',
                'post_status' => ['publish'],
                'posts_per_page' => -1,
                'meta_key' => 'closing_date',
                'orderby' => 'meta_value',
                'order' => 'ASC',
            );
        } else {
            $args = array(
                'post_type' => 'job',
                'post_status' => ['publish'],
                'posts_per_page' => -1,
                'orderby' => 'publish_date',
                'order' => 'DESC',
            );
        }

        if (!empty($queried_location) && $queried_location !== 'all') {
            $args['tax_query'] = array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'job_location',
                    'field' => 'term_id',
                    'terms' => intval($queried_location),
                )
            );
        }
        if (!empty($queried_department) && $queried_department !== 'all') {
            $args['tax_query'][] = array(
                'taxonomy' => 'job_department',
                'field' => 'term_id',
                'terms' => intval($queried_department),
            );
        }

        $jobs = new WP_Query($args);

        $out = '';

        if ($jobs->have_posts()) :
            while ($jobs->have_posts()) :
                $jobs->the_post();
                global $post;

                $blocks = App::parseBlock(get_post($post->ID), 'single-job');

                $out .= '<div class="col-12">';
                $out .= '<div class="job-archive__article">';
                if ($blocks) {
                    foreach ($blocks as $job) {
                        $out .= '<h4 class="job-archive__article__title">' . $job['block_title'] . '</h4>';
                        $out .= '<div class="job-archive__article__description">' . $job['block_short_description'] . '</div>';
                        $out .= '<div class="row">';
                        $out .= '<div class="col-12 col-md-6">';
                        $out .= '<div class="job-archive__article__link">';
                        if ($job['block_link']) {
                            $out .= '<a href="' . $job['block_link']['url'] . '" target="'. $job['block_link']['target'] . '">' . $job['block_link']['title'] . '</a>';
                        }
                        $out .= '</div>';
                        $out .= '</div>';
                        $out .= '<div class="col-12 col-md-6">';
                        $out .= '<div class="job-archive__article__date">';
                        if ($job['block_closing_date']) {
                            $out .= '<p>Application closes <span>' . preg_replace('/(\d{4})(\d{2})(\d{2})/i', '$3/$2/$1', $job['block_closing_date']) . '</span></p>';
                        }
                        $out .= '</div>';
                        $out .= '</div>';
                        $out .= '</div>';
                    }
                }
                $out .= '</div>';
                $out .= '</div>';

            endwhile;
        endif;
        wp_reset_postdata();
        die($out);
    }
}
