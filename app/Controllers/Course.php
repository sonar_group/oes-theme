<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class Course extends Controller
{

    protected $acf = true;
    public $discipline = null;
    public $level_of_study = null;

    public function __construct()
    {
        add_action('wp_ajax_nopriv_interactive_search', [$this, 'interactiveSearchAjax']);
        add_action('wp_ajax_interactive_search', [$this, 'interactiveSearchAjax']);
        add_action('wp_ajax_nopriv_course_archive', [$this, 'courseArchiveAjax']);
        add_action('wp_ajax_course_archive', [$this, 'courseArchiveAjax']);
        add_action('wp_ajax_nopriv_course_archive_multiple', [$this, 'courseArchiveMultipleAjax']);
        add_action('wp_ajax_course_archive_multiple', [$this, 'courseArchiveMultipleAjax']);
        add_action('wp_ajax_nopriv_discipline_courses', [$this, 'disciplineCoursesAjax']);
        add_action('wp_ajax_discipline_courses', [$this, 'disciplineCoursesAjax']);
        add_action('wp_ajax_nopriv_levels_of_study_courses', [$this, 'levelsOfStudyCoursesAjax']);
        add_action('wp_ajax_levels_of_study_courses', [$this, 'levelsOfStudyCoursesAjax']);
    }

    /**
     * All Courses
     *
     * @param null $id
     * @return array
     */
    public function getAllCourses()
    {
        $args = [
            'post_type' => 'course',
            'posts_per_page' => -1,
            'post_status' => ['publish'],
        ];
        if (!empty($this->discipline)) {
            $args['tax_query'] = [
                [
                    'taxonomy' => 'discipline',
                    'field' => 'term_id',
                    'terms' => intval($this->discipline),
                ]
            ];
        }
        if (!empty($this->level_of_study)) {
            $args['tax_query'] = [
                [
                    'taxonomy' => 'level_of_study',
                    'field' => 'term_id',
                    'terms' => intval($this->level_of_study),
                ]
            ];
        }

        $courses = new WP_Query($args);
        $courses_data = [];

        if ($courses->have_posts()) {
            while ($courses->have_posts()) {
                $courses->the_post();
                global $post;
                if ($post->post_type == 'course' && $post->post_parent == true) {

                    if(taxonomy_exists('level_of_study')) {
                        $level_of_study = !empty(get_the_terms($post->ID, 'level_of_study')[0]->name) ? get_the_terms($post->ID, 'level_of_study')[0]->name : '';
                    } else {
                        $level_of_study = null;
                    }

                    if(taxonomy_exists('discipline')) {
                        $term = !empty(get_the_terms($post->ID, 'discipline')[0]->name) ? get_the_terms($post->ID, 'discipline')[0]->name : '';
                    } else {
                        $term = null;
                    }

                    $courses_data[] = [
                        'id' => get_the_ID(),
                        'permalink' => get_the_permalink(get_the_ID()),
                        'title' => strval(get_the_title()),
                        'image' => get_the_post_thumbnail_url(get_the_ID(), 'full'),
                        'term' => $term,
                        'level_of_study' => $level_of_study,
                    ];
                }
            }
            wp_reset_postdata();
        }
        return $courses_data;
    }

    /**
     * @return array
     */
    public static function getAllDisciplines()
    {
        $disciplines = get_terms('discipline');
        return $disciplines;
    }

    /**
     * @return array
     */
    public function getAllDisciplinesOfLevelsOfStudy()
    {
        $args = [
            'post_type' => 'course',
            'posts_per_page' => -1,
            'post_status' => ['publish'],
        ];
        if (!empty($this->level_of_study)) {
            $args['tax_query'] = [
                [
                    'taxonomy' => 'level_of_study',
                    'field' => 'term_id',
                    'terms' => intval($this->level_of_study),
                ]
            ];
        }

        $courses = new WP_Query($args);
        $disciplines = [];

        if ($courses->have_posts()) {
            while ($courses->have_posts()) {
                $courses->the_post();
                global $post;
                if ($post->post_type == 'course' && $post->post_parent == true) {
                    if (!empty(get_the_terms($post->ID, 'discipline'))) {
                        $post_disciplines = get_the_terms($post->ID, 'discipline');
                        foreach ($post_disciplines as $discipline) {
                            if (!in_array($discipline, $disciplines)) {
                                $disciplines[] = $discipline;
                            }
                        }
                    }
                }
            }
            wp_reset_postdata();
        }
        return $disciplines;
    }

    /**
     * All Levels of Study
     *
     * @param null $id
     * @return array
     */
    public static function getAllLevelsOfStudy()
    {
        if(taxonomy_exists('level_of_study')) {
            return get_terms('level_of_study');
        } else {
            return null;
        }
    }

    /**
     * All Levels of Study
     *
     * @param null $id
     * @return array
     */
    public function getAllLevelsOfStudyInDiscipline()
    {
        $args = [
            'post_type' => 'course',
            'posts_per_page' => -1,
            'post_status' => ['publish'],
        ];
        if (!empty($this->discipline)) {
            $args['tax_query'] = [
                [
                    'taxonomy' => 'discipline',
                    'field' => 'term_id',
                    'terms' => intval($this->discipline),
                ]
            ];
        }

        $courses = new WP_Query($args);
        $level_of_studies = [];

        if ($courses->have_posts()) {
            while ($courses->have_posts()) {
                $courses->the_post();
                global $post;
                if ($post->post_type == 'course' && $post->post_parent == true) {
                    if(taxonomy_exists('level_of_study')) {
                        if (!empty(get_the_terms($post->ID, 'level_of_study'))) {
                            $post_level_of_studies = get_the_terms($post->ID, 'level_of_study');
                            foreach ($post_level_of_studies as $level_of_study) {
                                if (!in_array($level_of_study, $level_of_studies)) {
                                    $level_of_studies[] = $level_of_study;
                                }
                            }
                        }
                    }
                }
            }
            wp_reset_postdata();
        }
        return $level_of_studies;
    }

    public function setDiscipline($discipline)
    {
        $this->discipline = $discipline;
    }

    public function setLevelOfStudy($level_of_study)
    {
        $this->level_of_study = $level_of_study;
    }

    public static function interactiveSearchAjax()
    {
        $queried_discipline = (isset($_POST['discipline'])) ? $_POST['discipline'] : '';
        $queried_level_of_study = (isset($_POST['level_of_study'])) ? $_POST['level_of_study'] : '';

        header("Content-Type: text/html");

        $args = array(
            'post_type' => 'course',
            'posts_per_page' => -1,
            'post_status' => ['publish'],
        );

        if (!empty($queried_discipline)) {
            $args['tax_query'][] = array(
                array(
                    'taxonomy' => 'discipline',
                    'field' => 'term_id',
                    'terms' => intval($queried_discipline),
                )
            );
        }

        if (!empty($queried_level_of_study)) {
            $args['tax_query'][] = array(
                array(
                    'taxonomy' => 'level_of_study',
                    'field' => 'term_id',
                    'terms' => intval($queried_level_of_study),
                )
            );
        }

        $courses = new WP_Query($args);

        $out = '';

        if ($courses->have_posts()) :
            while ($courses->have_posts()) :
                $courses->the_post();
                global $post;

                if ($post->post_type == 'course' && $post->post_parent == true) {

                    if(taxonomy_exists('level_of_study')) {
                        $level_of_study = !empty(get_the_terms($post->ID, 'level_of_study')[0]->name) ? get_the_terms($post->ID, 'level_of_study')[0]->name : '';
                    } else {
                        $level_of_study = null;
                    }

                    $discipline = !empty(get_the_terms($post->ID, 'discipline')[0]->name) ? get_the_terms($post->ID, 'discipline')[0]->name : '';

                    $out .= '<a class="col-12 col-md-4 course" href="' . get_the_permalink() . '"><div class="content">';
                    if (!empty(get_the_post_thumbnail_url($post->ID, 'full'))) {
                        $out .= '<img src="' . get_the_post_thumbnail_url($post->ID, 'full') . '" loading="lazy" alt="">';
                    }
                    $out .= '<div>' . get_the_title() . '</div>';
                    $out .= '<div>' . $discipline . '</div>';
                    if (!empty($level_of_study)) {
                        $out .= '<div>' . $level_of_study . '</div>';
                    }
                    $out .= '</div></a>';

                }

            endwhile;
        endif;
        wp_reset_postdata();
        die($out);
    }

    public static function levelsOfStudyCoursesAjax()
    {
        $queried_discipline = (isset($_POST['discipline'])) ? $_POST['discipline'] : '';
        $queried_level_of_study = (isset($_POST['level_of_study'])) ? $_POST['level_of_study'] : '';

        header("Content-Type: text/html");

        $args = array(
            'post_type' => 'course',
            'posts_per_page' => -1,
            'post_status' => ['publish'],
        );

        if (!empty($queried_discipline)) {
            $args['tax_query'][] = array(
                array(
                    'taxonomy' => 'discipline',
                    'field' => 'term_id',
                    'terms' => intval($queried_discipline),
                )
            );
        }

        if (!empty($queried_level_of_study)) {
            $args['tax_query'][] = array(
                array(
                    'taxonomy' => 'level_of_study',
                    'field' => 'term_id',
                    'terms' => intval($queried_level_of_study),
                )
            );
        }

        $courses = new WP_Query($args);

        $out = '';

        if ($courses->have_posts()) :
            while ($courses->have_posts()) :
                $courses->the_post();
                global $post;

                if ($post->post_type == 'course' && $post->post_parent == true) {

                    $discipline = !empty(get_the_terms($post->ID, 'discipline')[0]->name) ? get_the_terms($post->ID, 'discipline')[0]->name : '';

                    $out .= '<a class="col-12 col-md-4 course" href="' . get_the_permalink() . '"><div class="content">';
                    if (!empty(get_the_post_thumbnail_url($post->ID, 'full'))) {
                        $out .= '<img src="' . get_the_post_thumbnail_url($post->ID, 'full') . '" loading="lazy" alt="">';
                    }
                    $out .= '<div>' . get_the_title() . '</div>';
                    $out .= '<div>' . $discipline . '</div>';
                    $out .= '</div></a>';

                }

            endwhile;
        endif;
        wp_reset_postdata();
        die($out);
    }

    public static function disciplineCoursesAjax()
    {
        $queried_discipline = (isset($_POST['discipline'])) ? $_POST['discipline'] : '';
        $queried_level_of_study = (isset($_POST['level_of_study'])) ? $_POST['level_of_study'] : '';

        header("Content-Type: text/html");

        $args = array(
            'post_type' => 'course',
            'posts_per_page' => -1,
            'post_status' => ['publish'],
        );

        if (!empty($queried_discipline)) {
            $args['tax_query'][] = array(
                array(
                    'taxonomy' => 'discipline',
                    'field' => 'term_id',
                    'terms' => intval($queried_discipline),
                )
            );
        }

        if (!empty($queried_level_of_study)) {
            $args['tax_query'][] = array(
                array(
                    'taxonomy' => 'level_of_study',
                    'field' => 'term_id',
                    'terms' => intval($queried_level_of_study),
                )
            );
        }

        $courses = new WP_Query($args);

        $out = '';

        if ($courses->have_posts()) :
            while ($courses->have_posts()) :
                $courses->the_post();
                global $post;

                if ($post->post_type == 'course' && $post->post_parent == true) {

                    $discipline = !empty(get_the_terms($post->ID, 'discipline')[0]->name) ? get_the_terms($post->ID, 'discipline')[0]->name : '';

                    $out .= '<a class="col-12 col-md-4 course" href="' . get_the_permalink() . '"><div class="content">';
                    if (!empty(get_the_post_thumbnail_url($post->ID, 'full'))) {
                        $out .= '<img src="' . get_the_post_thumbnail_url($post->ID, 'full') . '" loading="lazy" alt="">';
                    }
                    $out .= '<div>' . get_the_title() . '</div>';
                    $out .= '<div>' . $discipline . '</div>';
                    $out .= '</div></a>';

                }

            endwhile;
        endif;
        wp_reset_postdata();
        die($out);
    }

    public static function courseArchiveAjax()
    {
        $queried_discipline = (isset($_POST['discipline'])) ? $_POST['discipline'] : '';

        header("Content-Type: text/html");

        $args = array(
            'post_type' => 'course',
            'posts_per_page' => -1,
            'post_status' => ['publish'],
        );

        if (!empty($queried_discipline)) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'discipline',
                    'field' => 'term_id',
                    'terms' => intval($queried_discipline),
                )
            );
        }

        $courses = new WP_Query($args);

        $out = '';

        if ($courses->have_posts()) :
            while ($courses->have_posts()) :
                $courses->the_post();
                global $post;

                if ($post->post_type == 'course' && $post->post_parent == true) {

                    $discipline = !empty(get_the_terms($post->ID, 'discipline')[0]->name) ? get_the_terms($post->ID, 'discipline')[0]->name : '';

                    $out .= '<a class="col-12 col-md-4 course" href="' . get_the_permalink() . '"><div class="content">';
                    if (!empty(get_the_post_thumbnail_url($post->ID, 'full'))) {
                        $out .= '<img src="' . get_the_post_thumbnail_url($post->ID, 'full') . '" loading="lazy" alt="">';
                    }
                    $out .= '<div>' . get_the_title() . '</div>';
                    $out .= '<div>' . $discipline . '</div>';
                    $out .= '</div></a>';

                }

            endwhile;
        endif;
        wp_reset_postdata();
        die($out);
    }

    public static function courseArchiveMultipleAjax()
    {
        $queried_discipline = (!empty($_POST['discipline'])) ? explode(',', $_POST['discipline']) : '';
        $queried_level_of_study = (!empty($_POST['level_of_study'])) ? explode(',', $_POST['level_of_study']) : '';
        $queried_keyword = (!empty($_POST['keyword'])) ? $_POST['keyword'] : '';

        header("Content-Type: text/html");

        $args = array(
            'post_type' => 'course',
            'posts_per_page' => -1,
            'post_status' => ['publish'],
        );

        if (!empty($queried_keyword)) {
            $args['s'] = $queried_keyword;
        }

        if (!empty($queried_discipline) && $queried_discipline !== []) {
            $args['tax_query'][] = array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'discipline',
                    'field' => 'term_id',
                    'terms' => $queried_discipline,
                )
            );
        }
        if (!empty($queried_level_of_study) && $queried_level_of_study !== []) {
            $args['tax_query'][] = array(
                'taxonomy' => 'level_of_study',
                'field' => 'term_id',
                'terms' => $queried_level_of_study,
            );
        }

        $courses = new WP_Query($args);

        $out['courses_number'] = 0;
        $out['discipline_name'] = get_term(intval($queried_discipline))->name;
        $out['level_of_study_name'] = get_term(intval($queried_level_of_study))->name;
        $out['html'] = '';

        if ($courses->have_posts()) :
            while ($courses->have_posts()) :
                $courses->the_post();
                global $post;

                if ($post->post_type == 'course' && $post->post_parent == true) {

                    $discipline = !empty(get_the_terms($post->ID, 'discipline')[0]->name) ? get_the_terms($post->ID, 'discipline')[0]->name : '';

                    $out['html'] .= '<a class="col-12 col-md-4 course" href="' . get_the_permalink() . '"><div class="content">';
                    if (!empty(get_the_post_thumbnail_url($post->ID, 'full'))) {
                        $out['html'] .= '<img src="' . get_the_post_thumbnail_url($post->ID, 'full') . '" loading="lazy" alt="">';
                    }
                    $out['html'] .= '<div>' . get_the_title() . '</div>';
                    $out['html'] .= '<div>' . $discipline . '</div>';
                    $out['html'] .= '</div></a>';
                    $out['courses_number']++;

                }

            endwhile;
        endif;
        wp_reset_postdata();
        $out = json_encode($out);
        die($out);
    }

    public static function unitId()
    {
        global $post;

        $units = get_query_var( 'units' );

        if(self::isUnitPage() || $units){
            $slug = basename($_SERVER['REQUEST_URI']);
            $new_slug = str_replace('/'.$slug, '', $_SERVER['REQUEST_URI']);

            return url_to_postid($new_slug);
        }

        return $post->ID;
    }


    public static function isUnitPage(){

        $units = get_query_var( 'units' );

        if(is_singular('course') && $units == 'units'){
            return true;
        }

        return false;
    }

    public static function unitsHeader(){
        $post = get_post(self::unitId());

        if ( has_blocks( $post->post_content ) ) {
            $blocks = parse_blocks( $post->post_content );
            foreach( $blocks as $block ) {

                if($block['blockName'] = 'acf/header') {
                    echo render_block( $block );
                    break;
                }
            }
        }

    }


    public static function unitSettings()
    {
        $unit_groups = get_field('course_units', self::unitId());

        return $unit_groups;
    }

    public static function units()
    {
        $settings = self::unitSettings();
        $units = $settings['units'];

        return $units;
    }

    public static function unitsOutcomes()
    {
        $settings = self::unitSettings();
        $units = $settings['learning_outcomes'];

        return $units;
    }

    public static function unitsOverview()
    {
        $settings = self::unitSettings();
        $units = $settings['overview'];

        return $units;
    }


    public static function redirect(){
        $units = get_query_var( 'units' );

        if(is_singular('course') && $units && $units !== 'units'){
            $page = get_the_permalink(self::unitId());
            header("Refresh: 0; url=$page");
        }
    }


}
