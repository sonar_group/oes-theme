<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class News extends Controller
{

    protected $acf = true;
    public $featured_news = [];
    public $category = null;

    public function __construct()
    {
        add_action('wp_ajax_nopriv_more_post_ajax', [$this, 'morePostAjax']);
        add_action('wp_ajax_more_post_ajax', [$this, 'morePostAjax']);
        add_action('wp_ajax_nopriv_category_ajax', [$this, 'categoryAjax']);
        add_action('wp_ajax_category_ajax', [$this, 'categoryAjax']);
    }

    /**
     * All News
     *
     * @param null $id
     * @return array
     */
    public function getAllNews()
    {
        $args = [
            'post_type' => 'post',
            'post_status' => ['publish'],
            'posts_per_page' => 6,
            'paged' => get_query_var('paged') ? get_query_var('paged') : 1,
        ];
        if (!empty($this->category)) {
            $args['tax_query'] = [
                [
                    'taxonomy' => 'category',
                    'field' => 'term_id',
                    'terms' => intval($this->category),
                ]
            ];
        }

        if (!empty($this->featured_news)) {
            $args['post__not_in'] = [$this->featured_news->ID];
        }

        $news = new WP_Query($args);
        $news_data = [];

        if ($news->have_posts()) {
            while ($news->have_posts()) {
                $news->the_post();
                $news_data[] = [
                    'id' => get_the_ID(),
                    'permalink' => get_the_permalink(get_the_ID()),
                    'title' => strval(get_the_title()),
                    'image' => get_the_post_thumbnail_url(get_the_ID(), 'full'),
                    'excerpt' => get_the_excerpt(get_the_ID()),
                    'term' => !empty(get_the_terms(get_the_ID(), 'category')[0]->name) ? get_the_terms(get_the_ID(), 'category')[0]->name : '',
                    'read_time' => get_field('read_time', get_the_ID()),
                ];

            }
        }
        return $news_data;
    }

    /**
     * All News
     *
     * @param null $id
     * @return array
     */
    public function getFeaturedNews($id)
    {
        return
            [
                'id' => $id,
                'permalink' => get_the_permalink($id),
                'title' => strval(get_the_title($id)),
                'image' => get_the_post_thumbnail_url($id, 'full'),
                'excerpt' => get_the_excerpt($id),
                'term' => !empty(get_the_terms($id, 'category')[0]->name) ? get_the_terms($id, 'category')[0]->name : '',
                'read_time' => get_field('read_time', $id),
            ];
    }

    /**
     * @return array
     */
    public static function getAllNewsCategories()
    {
        $categories = get_categories();
        return $categories;
    }

    public function setFeaturedNews($news)
    {
        $this->featured_news = $news;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public static function categoryAjax()
    {
        $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 6;
        $queried_category = (isset($_POST['cat'])) ? $_POST['cat'] : '';
        $featured_news = (isset($_POST['featured_news'])) ? $_POST['featured_news'] : '';

        header("Content-Type: text/html");

        $args = array(
            'post_type' => 'post',
            'post_status' => ['publish'],
            'posts_per_page' => $ppp,
            'paged' => 1,
        );

        if (!empty($queried_category)) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'term_id',
                    'terms' => intval($queried_category),
                )
            );
        }

        if (!empty($featured_news)) {
            $args['post__not_in'] = [$featured_news];
        }

        $loop = new WP_Query($args);

        $out['posts_number'] = $loop->found_posts;
        $out['html'] = '';

        if ($loop->have_posts()) :
            while ($loop->have_posts()) :
                $loop->the_post();
                global $post;

                $out['html'] .= '<div class="col-12 col-md-4">';
                $out['html'] .= '<div class="news-archive__article">';
                $out['html'] .= '<a href="' . get_the_permalink() . '">';
                if (!empty(get_the_post_thumbnail_url($post->ID, 'full'))) {
                    $out['html'] .= '<img src="' . get_the_post_thumbnail_url($post->ID, 'full') . '" loading="lazy" alt="">';
                }
                $out['html'] .= '<h4>' . get_the_title() . '</h4>';
                $out['html'] .= '<p>' . get_the_excerpt() . '</p>';
                $out['html'] .= '<p>' . !empty(get_the_terms($post->ID, 'category')[0]->name) ? get_the_terms($post->ID, 'category')[0]->name : '' . '</p>';
                $out['html'] .= '<p>' . get_field('read_time') . '</p>';
                $out['html'] .= '</a>';
                $out['html'] .= '</div>';
                $out['html'] .= '</div>';
            endwhile;
        endif;
        wp_reset_postdata();
        $out = json_encode($out);
        die($out);
    }

    public static function morePostAjax()
    {
        $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 6;
        $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
        $queried_category = (isset($_POST['cat'])) ? $_POST['cat'] : '';
        $featured_news = (isset($_POST['featured_news'])) ? $_POST['featured_news'] : '';

        header("Content-Type: text/html");

        $args = array(
            'post_type' => 'post',
            'post_status' => ['publish'],
            'posts_per_page' => $ppp,
            'paged' => $page,
        );

        if (!empty($queried_category)) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'term_id',
                    'terms' => intval($queried_category),
                )
            );
        }

        if (!empty($featured_news)) {
            $args['post__not_in'] = [$featured_news];
        }

        $loop = new WP_Query($args);

        $out['posts_number'] = $loop->found_posts;
        $out['html'] = '';

        if ($loop->have_posts()) :
            while ($loop->have_posts()) :
                $loop->the_post();
                global $post;

                $out['html'] .= '<div class="col-12 col-md-4">';
                $out['html'] .= '<div class="news-archive__article">';
                $out['html'] .= '<a href="' . get_the_permalink() . '">';
                if (!empty(get_the_post_thumbnail_url($post->ID, 'full'))) {
                    $out['html'] .= '<img src="' . get_the_post_thumbnail_url($post->ID, 'full') . '" loading="lazy" alt="">';
                }
                $out['html'] .= '<h4>' . get_the_title() . '</h4>';
                $out['html'] .= '<p>' . get_the_excerpt() . '</p>';
                $out['html'] .= '<p>' . !empty(get_the_terms($post->ID, 'category')[0]->name) ? get_the_terms($post->ID, 'category')[0]->name : '' . '</p>';
                $out['html'] .= '<p>' . get_field('read_time') . '</p>';
                $out['html'] .= '</a>';
                $out['html'] .= '</div>';
                $out['html'] .= '</div>';
            endwhile;
        endif;
        wp_reset_postdata();
        $out = json_encode($out);
        die($out);
    }
}
