<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class App extends Controller
{
    /**
     * @return string|void
     */
    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    /**
     * @return mixed
     */
    public static function google()
    {
        return get_field('google', 'options');
    }

    /**
     * @return mixed
     */
    public function header()
    {
        return get_field('header', 'options');
    }

    /**
     * @return mixed
     */
    public function footer()
    {
        return get_field('footer', 'options');
    }

    /**
     * @return mixed
     */
    public static function courseOptions()
    {
        return get_field('course', 'options');
    }

    /**
     * @return string|void
     */
    public function siteName()
    {
        return get_bloginfo('name');
    }

    /**
     * @return mixed
     */
    public static function apply()
    {
        return get_field('apply_banner', 'options');
    }

    /**
     * @return mixed
     */
    public static function globalFormFields()
    {
        return get_field('forms', 'options');
    }

    /**
     * @return mixed
     */
    public static function recaptcha()
    {
        return get_field('recaptcha', 'options');
    }

    /**
     * @return mixed
     */
    public static function salesforce()
    {
        return get_field('salesforce', 'options');
    }

    /**
     * @return mixed
     */
    public static function headerScripts()
    {
        return get_field('header_scripts', 'options');
    }

    /**
     * @return mixed
     */
    public static function bodyScripts()
    {
        return get_field('body_scripts', 'options');
    }

    /**
     * @return mixed
     */
    public static function notFound()
    {
        return get_field('404', 'options');
    }

    /**
     * @return mixed
     */
    public static function stickyBar()
    {
        $sticky_bar = get_field('sticky_bar', 'options');

        if (!$sticky_bar) {
            return [
                'sticky_bar' => '',
                'status' => ''
            ];
        }

        return $sticky_bar;
    }

    public static function stickyBarCourse($id = null)
    {
        $sticky_bar = get_field('sticky_bar_course', $id);

        if (!$sticky_bar) {
            return [
                'override_sticky_bar' => '',
                'status' => ''
            ];

        }

        return $sticky_bar;
    }

    public static function courseButtons($id = null)
    {
        return get_field('inpage', $id);
    }

    public static function lazyLoading($id = null)
    {
        return get_field('lazy_loading', $id);
    }

    /**
     * @return mixed
     */
    public static function formSetup()
    {
        $args = [
            'post_type' => 'course',
            'posts_per_page' => -1,
        ];

        $courses = new WP_Query($args);
        $courses_data = [];

        if ($courses->have_posts()) {
            while ($courses->have_posts()) {
                $courses->the_post();
                global $post;
                if ($post->post_type == 'course' && $post->post_parent == true) {
                    $courses_data[] = [
                        'title' => strval(get_the_title()),
                        'course_code' => get_field('course_code', $post->ID),
                    ];
                }
            }
        }
        return $courses_data;
    }

    /**
     * @return mixed
     */
    public static function pagination()
    {
        $pagination_links = paginate_links(
            [
                'type' => 'list',
            ]
        );

        if (!empty($pagination_links)) {
            $pagination_links = str_replace("<ul class='page-numbers'>", '<ul class="pagination">', $pagination_links);
            $pagination_links = str_replace('<li>', '<li class="page-item">', $pagination_links);
            $pagination_links = str_replace('page-numbers', 'page-link', $pagination_links);
            $pagination_links = str_replace('current', 'active', $pagination_links);
        }

        return $pagination_links;
    }

    /**
     * @return mixed
     */
    public static function searchPageResults()
    {
        global $wp_query;
        // Get number of posts for each type
        $all_posts = new WP_Query(
            [
                's' => get_search_query(),
                'posts_per_page' => -1,
            ]
        );
        $course_count = 0;
        $article_count = 0;
        if ($all_posts->have_posts()) {
            while ($all_posts->have_posts()) {
                $all_posts->the_post();
                if (get_post_parent() && get_post_type_object(get_post_type())->labels->singular_name == 'Course') {
                    $course_count++;
                }
                if (get_post_type_object(get_post_type())->labels->singular_name == 'Post') {
                    $article_count++;
                }
            }
        }
        $result['total_results'] = $all_posts->found_posts;
        $result['total_courses'] = $course_count;
        $result['total_articles'] = $article_count;
        $result['actual_link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (strpos($result['actual_link'], 'course')) {
            $result['current_tab'] = 'course';
        } elseif (strpos($result['actual_link'], 'post')) {
            $result['current_tab'] = 'article';
        } else {
            $result['current_tab'] = 'all';
        }
        if (strpos($result['actual_link'], '&order=ASC&orderby=title')) {
            $result['current_sort'] = 'Title: A-Z';
        } elseif (strpos($result['actual_link'], '&order=DESC&orderby=title')) {
            $result['current_sort'] = 'Title: Z-A';
        } else {
            $result['current_sort'] = 'Relevance';
        }
        $result['actual_link'] = explode('&', $result['actual_link'])[0];
        $link_list = explode('/', $result['actual_link']);
        if (count($link_list) > 4) {
            $link = '';
            for ($i = 0; $i < count($link_list); $i++) {
                if ($i != 3 && $i != 4) {
                    $link = $link . $link_list[$i] . '/';
                }
            }
            $result['actual_link'] = rtrim($link, '/');
        }
        // Get Link for the filter
        if ($result['current_tab'] == 'course') {
            $result['link'] = $result['actual_link'] . '&post_type=course';
        } elseif ($result['current_tab'] == 'article') {
            $result['link'] = $result['actual_link'] . '&post_type=post';
        } else {
            $result['link'] = $result['actual_link'];
        }
        return $result;
    }

    /**
     * @param null $object
     * @param null $block_name
     * @return array|bool
     */
    public static function parseBlock($object = null, $block_name = null)
    {
        if ($object || $block_name) {
            $blocks = parse_blocks($object->post_content);
            $content = [];

            foreach ($blocks as $block) {
                if ($block['blockName'] === 'acf/' . $block_name) {
                    $content[] = $block['attrs']['data'];
                }
            }

            return $content;
        }

        return false;
    }

    public static function getCountdowns()
    {
        $response = array();
        $posts = get_posts(array(
            'post_type' => 'countdown',
            'fields' => 'ID',
            'posts_per_page' => -1
        ));
        foreach ($posts as $post) {
            $CountObj = array(
                'posts' => get_field('posts', $post->ID),
                'start_date' => get_field('start_date', $post->ID),
                'cut_off_date' => get_field('cut_off_date', $post->ID),
                'category' => get_field('display_category', $post->ID),
                'show_countdown' => get_field('show_countdown', $post->ID),
                'caption' => get_field('caption', $post->ID),
                'apply_button_only' => get_field('set_only_on_apply_button', $post->ID),
                'background_colour' => get_field('background_color', $post->ID),
                'link' => get_field('link', $post->ID),
                'countdown_id' => $post->ID
            );
            array_push($response, $CountObj);
        }

        return $response;
        wp_reset_postdata();
    }

    /**
     * @return mixed|string
     */
    public static function getClientIpAddress()
    {
        // CloudFront sets the real client IP in the X-Forwarded-For header
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_list = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            return trim($ip_list[0]); // The first IP is the real client IP
        }
        // Fallback to REMOTE_ADDR if no X-Forwarded-For is available
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * @return mixed
     */
    public static function getServerIp()
    {
        // Check if Cloudflare is used (by inspecting the header)
        if (!empty($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            // If using Cloudflare, get the server's real IP from the known internal Cloudflare header
            return $_SERVER['HTTP_CF_CONNECTING_IP'];
        } else {
            // Otherwise, fall back to normal server IP lookup
            return $_SERVER['SERVER_ADDR'];
        }
    }

    /**
     * @return string
     */
    public static function getServerFieldID()
    {
        $salesforce = App::salesforce();

        if (strpos($salesforce['form_url'], 'test') !== false) {
            return '00N9r0000050W6f';
        } else {
            return '00NRF000001E7aX';
        }
    }
}
