<?php

namespace App;

require_once 'Modules/blocks.php';

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__ . '\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory() . '/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory() . '/index.php';
    }

    return $comments_template;
}, 100);

/**
 * Remove the 'feed' endpoint
 */
add_filter('rewrite_rules_array', function ($rules) {
    foreach ($rules as $rule => $rewrite) {
        // remove feed rewrites
        if (preg_match('/.*(feed).*/', $rule)) {
            unset($rules[$rule]);
        }

        // remove comment rewrites
        if (preg_match('/.*(comment).*/', $rule)) {
            unset($rules[$rule]);
        }

        // remove author rewrites
        if (preg_match('/.*(author).*/', $rule)) {
            unset($rules[$rule]);
        }

        // remove year rewrites
        if (preg_match('/\(\[0-9\]\{4\}.*/', $rule)) {
            unset($rules[$rule]);
        }

        // remove attachment rewrites
        if (preg_match('/.*(attachment).*/', $rule)) {
            unset($rules[$rule]);
        }
        if (preg_match('/.*(attachment).*/', $rewrite)) {
            unset($rules[$rule]);
        }

        // remove category rewrites
        if (preg_match('/.*(category).*/', $rule)) {
            unset($rules[$rule]);
        }

        // remove tag rewrites
        if (preg_match('/.*(tag).*/', $rule)) {
            unset($rules[$rule]);
        }

        // remove post type rewrites
        if (preg_match('/.*(type).*/', $rule)) {
            unset($rules[$rule]);
        }
    }
    return $rules;
});

/**
 * Remove WP version from RSS.
 */
add_filter('the_generator', '__return_false');

/**
 * Remove WP version from css.
 */
add_filter('style_loader_src', function ($src) {
    if (strpos($src, 'ver=')) {
        $src = remove_query_arg('ver', $src);
    }
    return $src;
}, 9999);

/**
 * Remove WP version from scripts.
 */
add_filter('script_loader_src', function ($src) {
    if (strpos($src, 'ver=')) {
        $src = remove_query_arg('ver', $src);
    }
    return $src;
}, 9999);

/**
 * MCE Font Support
 */
add_filter('mce_css', function ($sheets) {
    return "$sheets," . \App\google_fonts_url();
});

/**
 * Async load CSS
 */
add_filter('style_loader_tag', function ($html, $handle, $href) {
    if (is_admin()) {
        return $html;
    }

    $dom = new \DOMDocument();
    $dom->loadHTML($html);
    $tag = $dom->getElementById($handle . '-css');
    $tag->setAttribute('media', 'print');
    $tag->setAttribute('onload', "this.media='all'");
    $tag->removeAttribute('type');
    $tag->removeAttribute('id');
    $html = $dom->saveHTML($tag);

    return $html;
}, 9999, 3);

/**
 * Load ACF JSON
 */
add_filter('acf/settings/load_json', function ($paths) {

    $paths = array(get_template_directory() . '/assets/acf-json');

    if (is_child_theme()) {
        $paths[] = get_stylesheet_directory() . '/assets/acf-json';
    }

    return $paths;
});

/**
 * Save ACF JSON
 */
add_filter('acf/settings/save_json', function ($paths) {

    if (is_child_theme()) {
        $paths = get_stylesheet_directory() . '/assets/acf-json';
    } else {
        $paths = get_template_directory() . '/assets/acf-json';
    }

    return $paths;
});

/**
 * Add SVG mime types
 */
add_filter('upload_mimes', function ($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    return $mimes;
});

/**
 * Create page for Course Units
 */
add_filter('template_include', function($template) {
    $url  = $_SERVER["PHP_SELF"];
    $path = explode("/", $url);
    $last = end($path);

    if ($last === 'course-unit') {
        $new_template = locate_template('course-unit.blade.php');
        //$new_template = template_path(locate_template('views/course-unit.blade.php'));
        if ( '' != $new_template ) {
            return $new_template ;
        }
    }
    return $template;
});


/**
 *  Manually override the function’s return value to tell WP we don’t have a theme.json file
 */
add_filter('theme_file_path', function($path, $file) {
    if($file === 'theme.json') {
        return false;
    }
    return $path;
}, 0, 2);
