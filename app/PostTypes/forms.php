<?php

namespace App\PostTypes;

/*
 * Forms post type and functions
 */

use Carbon\Carbon;

function register_forms()
{
    $labels = array(
        'name' => _x('Forms', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Form', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Forms', 'text_domain'),
        'name_admin_bar' => __('Forms', 'text_domain'),
        'archives' => __('Forms Archives', 'text_domain'),
        'attributes' => __('Forms Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent Form:', 'text_domain'),
        'all_items' => __('All Forms', 'text_domain'),
        'add_new_item' => __('Add New Form', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'new_item' => __('New Form', 'text_domain'),
        'edit_item' => __('Edit Form', 'text_domain'),
        'update_item' => __('Update Form', 'text_domain'),
        'view_item' => __('View Form', 'text_domain'),
        'view_items' => __('View Forms', 'text_domain'),
        'search_items' => __('Search Forms', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image' => __('Use as featured image', 'text_domain'),
        'insert_into_item' => __('Insert into Form', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this Form', 'text_domain'),
        'items_list' => __('Forms list', 'text_domain'),
        'items_list_navigation' => __('Forms list navigation', 'text_domain'),
        'filter_items_list' => __('Filter Forms list', 'text_domain'),
    );
    $args = array(
        'label' => __('Forms', 'text_domain'),
        'description' => __('Forms Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'author', 'editor'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-list-view',
        'menu_position' => 30,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'rewrite' => [
            'slug' => 'forms',
            'with_front' => false
        ]
    );
    register_post_type('form', $args);
}

add_action('init', 'App\PostTypes\register_forms', 0);

if (function_exists('acf_add_options_page')) {
    acf_add_options_sub_page(array(
        'page_title' => 'Forms Settings',
        'menu_title' => 'Forms Settings',
        'parent_slug' => 'edit.php?post_type=form',
    ));
    acf_add_options_sub_page(array(
        'page_title' => 'Form Select Settings',
        'menu_title' => 'Form Select Settings',
        'parent_slug' => 'edit.php?post_type=form',
    ));
}

/**
 *  Hide Forms Type
 */
function hide_forms_type()
{
    if (is_singular('form')) {
        wp_redirect(home_url(), 301);
        exit;
    }
}

//add_action('template_redirect', 'App\PostTypes\hide_forms_type');

add_action('admin_menu', 'App\PostTypes\forms_register_admin_page');

function forms_register_admin_page()
{
    add_submenu_page('edit.php?post_type=form', 'Reset Form Select Options', 'Reset Form Select Options', 'edit_posts', 'reset-form-select-options', 'App\PostTypes\reset_form_select_options', 99);
}

function reset_form_select_options()
{
    if (have_rows('salutation', 'options')) {
        delete_field('salutation', 'options');
    }
    $options = include __DIR__.'/../Modules/Forms/salutations.php';
    usort($options, function ($a, $b) {
        return strcasecmp($a['value'], $b['value']);
    });
    update_field('salutation', $options, 'options');

    if (have_rows('commencing_year', 'options')) {
        delete_field('commencing_year', 'options');
    }
    $options = [
        [
            'label' => Carbon::create()->format('Y'),
            'value' => Carbon::create()->format('Y')
        ],
        [
            'label' => Carbon::create()->addYear(1)->format('Y'),
            'value' => Carbon::create()->addYear(1)->format('Y')
        ]
    ];
    update_field('commencing_year', $options, 'options');

    if (have_rows('commencing_teaching_period', 'options')) {
        delete_field('commencing_teaching_period', 'options');
    }
    $options = include __DIR__.'/../Modules/Forms/teaching-period.php';
    usort($options, function ($a, $b) {
        return strcasecmp($a['value'], $b['value']);
    });
    update_field('commencing_teaching_period', $options, 'options');

    if (have_rows('citizenship', 'options')) {
        delete_field('citizenship', 'options');
    }
    $options = include __DIR__.'/../Modules/Forms/citizenship.php';
    usort($options, function ($a, $b) {
        return strcasecmp($a['value'], $b['value']);
    });
    update_field('citizenship', $options, 'options');

    if (have_rows('educational_achievement', 'options')) {
        delete_field('educational_achievement', 'options');
    }
    $options = include __DIR__.'/../Modules/Forms/educational-achievement.php';
    usort($options, function ($a, $b) {
        return strcasecmp($a['value'], $b['value']);
    });
    update_field('educational_achievement', $options, 'options');

    if (have_rows('school_study', 'options')) {
        delete_field('school_study', 'options');
    }
    $options = include __DIR__.'/../Modules/Forms/school-study.php';
    usort($options, function ($a, $b) {
        return strcasecmp($a['value'], $b['value']);
    });
    update_field('school_study', $options, 'options');

    if (have_rows('parent_guardian_education', 'options')) {
        delete_field('parent_guardian_education', 'options');
    }
    $options = include __DIR__.'/../Modules/Forms/parent-guardian-education.php';
    usort($options, function ($a, $b) {
        return strcasecmp($a['value'], $b['value']);
    });
    update_field('parent_guardian_education', $options, 'options');

    echo 'Reset Form Select Options Successful.';
}

add_action('init', function () {
    if (session_status() === PHP_SESSION_NONE) {
        session_set_cookie_params(86400, '/'); // 24 hours
        session_start();
    }
});