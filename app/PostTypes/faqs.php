<?php

namespace App\PostTypes;

/*
 * FAQs post type and functions
 */
function register_faqs()
{
    $labels = array(
        'name' => _x('FAQs', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('FAQ', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('FAQs', 'text_domain'),
        'name_admin_bar' => __('FAQs', 'text_domain'),
        'archives' => __('FAQs Archives', 'text_domain'),
        'attributes' => __('FAQs Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent FAQ:', 'text_domain'),
        'all_items' => __('All FAQs', 'text_domain'),
        'add_new_item' => __('Add New FAQ', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'new_item' => __('New FAQ', 'text_domain'),
        'edit_item' => __('Edit FAQ', 'text_domain'),
        'update_item' => __('Update FAQ', 'text_domain'),
        'view_item' => __('View FAQ', 'text_domain'),
        'view_items' => __('View FAQs', 'text_domain'),
        'search_items' => __('Search FAQs', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image' => __('Use as featured image', 'text_domain'),
        'insert_into_item' => __('Insert into FAQ', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this FAQ', 'text_domain'),
        'items_list' => __('FAQs list', 'text_domain'),
        'items_list_navigation' => __('FAQs list navigation', 'text_domain'),
        'filter_items_list' => __('Filter FAQs list', 'text_domain'),
    );
    $args = array(
        'label' => __('FAQs', 'text_domain'),
        'description' => __('FAQs Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'author', 'editor'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-editor-help',
        'menu_position' => 30,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'rewrite' => [
            'slug' => 'faqs',
            'with_front' => false
        ]
    );
    register_post_type('faq', $args);
}

add_action('init', 'App\PostTypes\register_faqs', 0);
