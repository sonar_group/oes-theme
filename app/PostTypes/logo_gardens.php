<?php

namespace App\PostTypes;

/*
 * Logo Gardens post type and functions
 */
function register_logo_gardens()
{
    $labels = array(
        'name' => _x('Logo Gardens', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Logo Garden', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Logo Gardens', 'text_domain'),
        'name_admin_bar' => __('Logo Gardens', 'text_domain'),
        'archives' => __('Logo Gardens Archives', 'text_domain'),
        'attributes' => __('Logo Gardens Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent Logo Garden:', 'text_domain'),
        'all_items' => __('All Logo Gardens', 'text_domain'),
        'add_new_item' => __('Add New Logo Garden', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'new_item' => __('New Logo Garden', 'text_domain'),
        'edit_item' => __('Edit Logo Garden', 'text_domain'),
        'update_item' => __('Update Logo Garden', 'text_domain'),
        'view_item' => __('View Logo Garden', 'text_domain'),
        'view_items' => __('View Logo Gardens', 'text_domain'),
        'search_items' => __('Search Logo Gardens', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image' => __('Use as featured image', 'text_domain'),
        'insert_into_item' => __('Insert into Logo Garden', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this Logo Garden', 'text_domain'),
        'items_list' => __('Logo Gardens list', 'text_domain'),
        'items_list_navigation' => __('Logo Gardens list navigation', 'text_domain'),
        'filter_items_list' => __('Filter Logo Gardens list', 'text_domain'),
    );
    $args = array(
        'label' => __('Logo Gardens', 'text_domain'),
        'description' => __('Logo Gardens Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'author', 'editor'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-book-alt',
        'menu_position' => 30,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'rewrite' => [
            'slug' => 'logo_gardens',
            'with_front' => false
        ]
    );
    register_post_type('logo_garden', $args);
}

add_action('init', 'App\PostTypes\register_logo_gardens', 0);
