<?php

namespace App\PostTypes;

/*
 * Teams post type and functions
 */
function register_teams()
{
    $labels = array(
        'name' => _x('Teams', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Team', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Teams', 'text_domain'),
        'name_admin_bar' => __('Teams', 'text_domain'),
        'archives' => __('Teams Archives', 'text_domain'),
        'attributes' => __('Teams Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent Team:', 'text_domain'),
        'all_items' => __('All Teams', 'text_domain'),
        'add_new_item' => __('Add New Team', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'new_item' => __('New Team', 'text_domain'),
        'edit_item' => __('Edit Team', 'text_domain'),
        'update_item' => __('Update Team', 'text_domain'),
        'view_item' => __('View Team', 'text_domain'),
        'view_items' => __('View Teams', 'text_domain'),
        'search_items' => __('Search Teams', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image' => __('Use as featured image', 'text_domain'),
        'insert_into_item' => __('Insert into Team', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this Team', 'text_domain'),
        'items_list' => __('Teams list', 'text_domain'),
        'items_list_navigation' => __('Teams list navigation', 'text_domain'),
        'filter_items_list' => __('Filter Teams list', 'text_domain'),
    );
    $args = array(
        'label' => __('Teams', 'text_domain'),
        'description' => __('Teams Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'author', 'editor'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-buddicons-buddypress-logo',
        'menu_position' => 30,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'rewrite' => [
            'slug' => 'teams',
            'with_front' => false
        ]
    );
    register_post_type('team', $args);
}

add_action('init', 'App\PostTypes\register_teams', 0);
