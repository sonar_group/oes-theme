<?php

namespace App\PostTypes;

/*
 * Countdown post type and functions
 */
function register_countdown()
{
    $labels = array(
        'name' => _x('Countdown', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Countdown', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Countdowns', 'text_domain'),
        'name_admin_bar' => __('Countdowns', 'text_domain'),
        'archives' => __('Countdowns Archives', 'text_domain'),
        'attributes' => __('Countdowns Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent Countdown:', 'text_domain'),
        'all_items' => __('All Countdowns', 'text_domain'),
        'add_new_item' => __('Add New Countdown', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'new_item' => __('New Countdown', 'text_domain'),
        'edit_item' => __('Edit Countdown', 'text_domain'),
        'update_item' => __('Update Countdown', 'text_domain'),
        'view_item' => __('View Countdown', 'text_domain'),
        'view_items' => __('View Countdowns', 'text_domain'),
        'search_items' => __('Search Countdowns', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'insert_into_item' => __('Insert into Countdown', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this Countdown', 'text_domain'),
        'items_list' => __('Countdowns list', 'text_domain'),
        'items_list_navigation' => __('Countdowns list navigation', 'text_domain'),
        'filter_items_list' => __('Filter Countdowns list', 'text_domain'),
    );
    $args = array(
        'label' => __('Countdowns', 'text_domain'),
        'description' => __('Countdowns Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'author', 'editor'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-backup',
        'menu_position' => 30,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => false,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'rewrite' => [
            'with_front' => false
        ]
    );
    register_post_type('countdown', $args);
}

add_action('init', 'App\PostTypes\register_countdown', 0);
