<?php

namespace App\PostTypes;

/*
 * Units post type and functions
 */
function register_units()
{
    $labels = array(
        'name' => _x('Units', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Unit', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Units', 'text_domain'),
        'name_admin_bar' => __('Units', 'text_domain'),
        'archives' => __('Units Archives', 'text_domain'),
        'attributes' => __('Units Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent Unit:', 'text_domain'),
        'all_items' => __('All Units', 'text_domain'),
        'add_new_item' => __('Add New Unit', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'new_item' => __('New Unit', 'text_domain'),
        'edit_item' => __('Edit Unit', 'text_domain'),
        'update_item' => __('Update Unit', 'text_domain'),
        'view_item' => __('View Unit', 'text_domain'),
        'view_items' => __('View Units', 'text_domain'),
        'search_items' => __('Search Units', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image' => __('Use as featured image', 'text_domain'),
        'insert_into_item' => __('Insert into Unit', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this Unit', 'text_domain'),
        'items_list' => __('Units list', 'text_domain'),
        'items_list_navigation' => __('Units list navigation', 'text_domain'),
        'filter_items_list' => __('Filter Units list', 'text_domain'),
    );
    $args = array(
        'label' => __('Units', 'text_domain'),
        'description' => __('Units Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'author', 'editor'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-open-folder',
        'menu_position' => 30,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'rewrite' => [
            'slug' => 'online-courses/units',
            'with_front' => false
        ],
    );
    register_post_type('unit', $args);

    add_rewrite_rule('^online-courses/units/([^/]+)/?$', 'index.php?post_type=unit&unit=$matches[1]', 'top');
}

add_action('init', 'App\PostTypes\register_units', 0);
