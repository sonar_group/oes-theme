<?php

namespace App\PostTypes;

/*
 * Courses post type and functions
 */
function register_courses()
{
    $labels = array(
        'name' => _x('Courses', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Course', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Courses', 'text_domain'),
        'name_admin_bar' => __('Courses', 'text_domain'),
        'archives' => __('Courses Archives', 'text_domain'),
        'attributes' => __('Courses Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent Course:', 'text_domain'),
        'all_items' => __('All Courses', 'text_domain'),
        'add_new_item' => __('Add New Course', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'new_item' => __('New Course', 'text_domain'),
        'edit_item' => __('Edit Course', 'text_domain'),
        'update_item' => __('Update Course', 'text_domain'),
        'view_item' => __('View Course', 'text_domain'),
        'view_items' => __('View Courses', 'text_domain'),
        'search_items' => __('Search Courses', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image' => __('Use as featured image', 'text_domain'),
        'insert_into_item' => __('Insert into Course', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this Course', 'text_domain'),
        'items_list' => __('Courses list', 'text_domain'),
        'items_list_navigation' => __('Courses list navigation', 'text_domain'),
        'filter_items_list' => __('Filter Courses list', 'text_domain'),
    );
    $args = array(
        'label' => __('Courses', 'text_domain'),
        'description' => __('Courses Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'author', 'editor', 'page-attributes', 'thumbnail'),
        'taxonomies' => array('discipline'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'menu_position' => 30,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'rewrite' => [
            'slug' => 'online-courses',
            'with_front' => false
        ]
    );
    register_post_type('course', $args);

    add_rewrite_tag( '%units%', '([^/]*)' );
    add_rewrite_rule(
        '^online-courses/([^/]*)/([^/]*)/([^/]*)/?$',
        'index.php?course=$matches[1]&units=$matches[3]',
        'top'
    );
}

add_action('init', 'App\PostTypes\register_courses', 0);
