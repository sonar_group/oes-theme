<?php

namespace App\PostTypes;

/*
 * Histories post type and functions
 */
function register_histories()
{
    $labels = array(
        'name' => _x('Histories', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('History', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Histories', 'text_domain'),
        'name_admin_bar' => __('Histories', 'text_domain'),
        'archives' => __('Histories Archives', 'text_domain'),
        'attributes' => __('Histories Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent History:', 'text_domain'),
        'all_items' => __('All Histories', 'text_domain'),
        'add_new_item' => __('Add New History', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'new_item' => __('New History', 'text_domain'),
        'edit_item' => __('Edit History', 'text_domain'),
        'update_item' => __('Update History', 'text_domain'),
        'view_item' => __('View History', 'text_domain'),
        'view_items' => __('View Histories', 'text_domain'),
        'search_items' => __('Search Histories', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image' => __('Use as featured image', 'text_domain'),
        'insert_into_item' => __('Insert into History', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this History', 'text_domain'),
        'items_list' => __('Histories list', 'text_domain'),
        'items_list_navigation' => __('Histories list navigation', 'text_domain'),
        'filter_items_list' => __('Filter Histories list', 'text_domain'),
    );
    $args = array(
        'label' => __('Histories', 'text_domain'),
        'description' => __('Histories Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'author', 'editor'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-calendar-alt',
        'menu_position' => 30,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'rewrite' => [
            'slug' => 'histories',
            'with_front' => false
        ]
    );
    register_post_type('history', $args);
}

add_action('init', 'App\PostTypes\register_histories', 0);
