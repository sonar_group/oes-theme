<?php

/**
 * This file SHOULD NOT be included in the parent theme
 * Make sure to copy this file to the child theme and include it at functions.php
 *
 * For reference:
 * TEMPLATEPATH refers to the parent template
 * STYLESHEETPATH refers to the child template
 * ie, at the child theme, it would be:
 * require_once TEMPLATEPATH . '/../app/PostTypes/courses.php';
 */

require_once 'PostTypes/courses.php';
require_once 'PostTypes/units.php';
require_once 'PostTypes/faqs.php';
require_once 'PostTypes/teams.php';
require_once 'PostTypes/jobs.php';
require_once 'PostTypes/histories.php';
require_once 'PostTypes/logo_gardens.php';
require_once 'PostTypes/forms.php';
require_once 'PostTypes/countdown.php';
