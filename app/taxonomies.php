<?php

/**
 * This file SHOULD NOT be included in the parent theme
 * Make sure to copy this file to the child theme and include it at functions.php
 *
 * For reference:
 * TEMPLATEPATH refers to the parent template
 * STYLESHEETPATH refers to the child template
 * ie, at the child theme, it would be:
 * require_once TEMPLATEPATH . '/../app/Taxonomies/disciplines.php';
 */

require_once 'Taxonomies/disciplines.php';
require_once 'Taxonomies/faq_category.php';
require_once 'Taxonomies/level_of_study.php';
require_once 'Taxonomies/job_department.php';
require_once 'Taxonomies/job_location.php';
