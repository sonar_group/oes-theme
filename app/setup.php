<?php

namespace App;

use App\Modules\FormProcessor;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Container;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

require_once 'Modules/acf.php';

FormProcessor::getInstance();

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_style('google/fonts', \App\google_fonts_url(), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
    wp_enqueue_script('jquery-ui-autocomplete');

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    if (!is_admin()) {
        wp_dequeue_style('wp-block-library');
    }

    // Include WPML case.
    if (in_array('sitepress-multilingual-cms/sitepress.php', get_option('active_plugins'))) {
        $ajaxurl = admin_url('admin-ajax.php?lang=' . ICL_LANGUAGE_CODE);
    } else {
        $ajaxurl = admin_url('admin-ajax.php');
    }

    wp_localize_script('sage/main.js', 'ajax_posts', array(
        'ajaxurl' => $ajaxurl,
        'action' => 'more_post_ajax',
        'noPosts' => esc_html__('No older posts found', 'oes-theme-ajax'),
        'fileUploadNonce' => wp_create_nonce('form_builder_file_upload')
    ));
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'top_nav' => __('Top Navigation', 'sage'),
        'main_nav' => __('Main Navigation', 'sage'),
        'footer_nav' => __('Footer Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * Remove jQuery Migrate
 */
add_action('wp_default_scripts', function ($scripts) {
    if (isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];

        if ($script->deps) { // Check whether the script has any dependencies
            $script->deps = array_diff($script->deps, array(
                'jquery-migrate'
            ));
        }
    }
});

/**
 * Inject critical assets in head as early as possible
 */
add_action('wp_head', function (): void {
    echo '<link rel="preconnect" href="https://fonts.gstatic.com">';
}, 1);

/**
 * Clean up head
 */
add_action('init', function () {

    // EditURI link.
    remove_action('wp_head', 'rsd_link');
    // Category feed links.
    remove_action('wp_head', 'feed_links_extra', 3);
    // Post and comment feed links.
    remove_action('wp_head', 'feed_links', 2);
    // Windows Live Writer.
    remove_action('wp_head', 'wlwmanifest_link');
    // Index link.
    remove_action('wp_head', 'index_rel_link');
    // Previous link.
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    // Start link.
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    // Canonical.
    remove_action('wp_head', 'rel_canonical', 10, 0);
    // Shortlink.
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    // Links for adjacent posts.
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    // WP version.
    remove_action('wp_head', 'wp_generator');
    // Remove emojis from the frontend and backend
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
});

//add_action('init', function() {
//    $url  = $_SERVER["PHP_SELF"];
//    $path = explode("/", $url);
//    $last = end($path);
//
//    if ( $last === 'course-unit' ) {
//        $load = locate_template('course-unit.blade.php', true);
//
//        if ($load) {
//            exit(); // just exit if template was found and loaded
//        }
//    }
//});
