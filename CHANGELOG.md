# Change Log
## [2.2.0.11] - 05/02/2025
Add server ids for different environments

## [2.2.0.10] - 03/02/2025
Add ip addresses to Form Builder hidden input fields

## [2.2.0.9] - 02/12/2024
HTML escape on form builder inputs

## [2.2.0.8] - 18/09/2024
Apply Experian API to form builder

## [2.2.0.7] - 25/07/2024
Fix Form Builder recaptcha PHP warning

## [2.2.0.6] - 11/07/2024
Fix Form Builder recaptcha
Add Recaptcha form field

## [2.2.0.5] - 02/07/2024
Update to sage-acf-gutenberg-blocks-php-8 lib

## [2.2.0.4] - 02/07/2024
Update package.lock file

## [2.2.0.3] - 02/07/2024
Fix Form Builder custom dropdown field mandatory validation functionality
Add Recaptcha Keyname from Form Settings
Update Form Processor timestamp
Disable Verify Recaptcha from Controller

## [2.2.0.2] - 18/03/2024
Update composer installers version 2

## [2.2.0.1] - 18/03/2024
Update Sage Lib support PHP 8.0+

## [2.2.0] - 12/03/2024
Support PHP version 8.0+

## [2.1.26.1] - 01/03/2024
Added Change Log for OES Theme

## [2.1.26] - 30/05/2023
Minor update on Form Builder
### Added
* Added Google Recaptcha on Form Builder

## [2.1.25] - 29/05/2023
Minor update on Form Builder
### Changed
* Update captcha_setting function on Form Builder

## [2.1.24] - 02/02/2023
Added a new feature on Form Builder and fix countdown response issue
### Added
* Add Salesforce detail
### Fixed
* Fix warning countdown response

## [2.1.23] - 17/01/2023
Added a new block
### Added
* Units Archive block added

## [2.1.22] - 1/01/2023
Minor updated on Form Builder
### Changed
  * Removed debug code

## [2.1.21] - 17/11/2022
Added a new feature
### Added
* Countdown Timer

## [2.1.20] - 04/10/2022
Added a new feature on Form Builder
### Added
* Extra logging to forms

#### List of blocks
* Apply Banner
* Banner
* Brochure Form
* Call Out Banner
* Columns
* Complex Copy
* Course Archive Multiple
* Course Archive
* Course Glance
* Course Glance v2
* Course Overview
* ourse Tab Content
* Course Tab Content v2
* Course Testimonial
* Disciplines
* Discipline Courses
* Faqs
* Faqs Archive
* Footnote
* Form Builder
* Forms Layout
* Header
* History Archive
* Icons
* Image Gallery
* Image Gallery Text
* Industry Insights
* Inline Brochure Form
* Interactive Search
* Job Archive
* Left Right
* Levels Of Study
* Levels Of Study Courses
* Levels Of Study Infographic
* Logo Garden Archive
* News Archive
* Parallax Page
* Portrait Carousel
* Profiles Columns
* Quick Links
* Related Courses
* Related News
* Search Courses
* Search Courses v2
* Single Faq
* Single History
* Single Job
* Single Logo Garden
* Single Team
* Single Unit
* Step Content
* Sticky Sub Nav
* Tabbed Content
* Team
* Testimonial
* Testimonial Carousel
* Tiles
* Vertical Accordion
* Video
