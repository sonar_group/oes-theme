export default function (config) {
  return {
    ignore: ':hidden',
    ignoreTitle: true,
    rules: config.rules,
    messages: {
      name: 'Please enter your name.',
      first_name: 'Please enter your first name.',
      last_name: 'Please enter your last name.',
      email: 'Please enter a valid email address.',
      mobile: 'Please enter a valid mobile number.',
      agree_terms: 'Please accept our policy.',
      ...config.messages,
    },
    errorElement: 'div',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
        const $formOptionsWrap = element.closest('.form-options-wrap')
        if ($formOptionsWrap.length) {
          error.insertAfter($formOptionsWrap)
        } else {
          error.insertAfter(element.parent('label'))
        }
      } else if (element.hasClass('form-phone')) {
        const $formGroupWrap = element.closest('.form-phone-wrap')
        error.insertAfter($formGroupWrap)
      } else {
        error.insertAfter(element)
      }
    },
    highlight: function (element) {
      const $element = $(element)
      if ($element.prop('type') === 'checkbox' || $element.prop('type') === 'radio') {
        return
      }
      $element.addClass('is-invalid').removeClass('is-valid')
    },
    unhighlight: function (element) {
      const $element = $(element)
      if ($element.prop('type') === 'checkbox' || $element.prop('type') === 'radio') {
        return
      }
      $element.addClass('is-valid').removeClass('is-invalid')
    },
    submitHandler: function(form) {
      console.log('This form needs a submit handler!', form)
      alert('This form needs a submit handler!')
    },
  }
}
