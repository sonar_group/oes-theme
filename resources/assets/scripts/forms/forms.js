import 'jquery-validation'
import Axios from 'axios'
import validatorConfig from './validator-config'
import $ from 'jquery'
import moment from 'moment'
import intlTelInput from 'intl-tel-input';

export default function () {
  const windowConfig = window.ajax_posts
  const $forms = $('form.form-builder-form')

  if (!$forms.length) {
    return
  }

  /* Custom validations */

  const phoneApply = document.querySelectorAll('.form-builder-phone input');
  if (phoneApply !== null) {
    phoneApply.forEach(function (item) {
      intlTelInput(item, {
        initialCountry: 'AU',
        showSelectedDialCode: true,
        autoPlaceholder: 'off',
        utilsScript: 'https://cdn.jsdelivr.net/npm/intl-tel-input@19.5.6/build/js/utils.js',
      });
    });
  }

  jQuery.validator.addMethod('customPhone', function (value, element) {
    const phone = window.intlTelInputGlobals.getInstance(element)
    return phone.isValidNumber()
  }, 'Please enter a valid mobile number')


  /* Set custom config */
  let defaultConfig = {rules: {}, messages: {}}
  const $phones = $forms.find('input.form-phone')
  $phones.each(function () {
    const $this = $(this)
    defaultConfig.rules[$this.prop('name')] = {
      customPhone: true,
    }
  })
  const $inputEmails = $forms.find('input[type="email"]')
  $inputEmails.each(function () {
    const $this = $(this)
    if ($this.prop('name').length && $this.prop('title').length) {
      defaultConfig.messages[$this.prop('name')] = {
        required: 'Please enter your ' + $this.prop('title').toLowerCase() + '.',
        email: 'Please enter a valid email address.',
      }
    }
  })
  const $inputTexts = $forms.find('input[type="text"], input[type="date"]')
  $inputTexts.each(function () {
    const $this = $(this)
    if ($this.prop('name').length && $this.prop('title').length) {
      defaultConfig.messages[$this.prop('name')] = {
        required: 'Please enter your ' + $this.prop('title').toLowerCase() + '.',
      }
    }
  })
  const $inputSelects = $forms.find('select')
  $inputSelects.each(function () {
    const $this = $(this)
    if ($this.prop('name').length && $this.prop('title').length) {
      defaultConfig.messages[$this.prop('name')] = {
        required: 'Please select your ' + $this.prop('title').toLowerCase() + '.',
      }
    }
  })


  /* Handle form submit */
  let config = validatorConfig(defaultConfig)
  config.submitHandler = function (form) {
    // store google tag events
    // if (window.dataLayer) {
    //   window.dataLayer.push({
    //     'type': 'Mailing List',
    //     'category': 'Stay up To Date',
    //     'name': 'Stay up To Date',
    //     'event': 'form-submit-stayuptodate'
    //   })
    // }

    const $form = $(form)

    // Check any recaptcha
    const $recaptcha = $form.find('.g-recaptcha')
    if ($recaptcha.length > 0 && $form.find('[name="g-recaptcha-response"]').val() === '') {
      $recaptcha.prev('.recaptcha-error').removeClass('d-none')
      return false
    }

    // get mobile number with country code
    const phoneFields = $form.find('input.form-phone')
    phoneFields.each(function () {
      const $phone = $(this)
      const phoneNumber = window.intlTelInputGlobals.getInstance($phone[0])
      $phone.val(phoneNumber.getNumber())
    })

    const $submit = $form.find('button[type="submit"]')
    $submit.attr('disabled', true)
    $form.find('.message').hide().html('Please wait.').fadeIn()

    const formData = new FormData($form[0])
    const qs = new URLSearchParams(formData)

    // remove all file field inputs
    const $fileFields = $form.find('input[type="file"]')
    $fileFields.each(function () {
      const $this = $(this)
      const name = $this.attr('name')
      qs.delete(name)
    })

    // reformat date field
    const $dateFields = $form.find('input[type="date"]')
    $dateFields.each(function () {
      const $this = $(this)
      const name = $this.attr('name')
      const dateValue = qs.get(name)
      const dateFormatted = moment(dateValue, 'YYYY-MM-DD').format('DD/MM/YYYY')
      qs.set(name, dateFormatted)
    })

    Axios.post(windowConfig.ajaxurl, qs, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(response => {
      if (response.data.success) {
        $form.find('.message').hide().html('Thank you.').fadeIn()
        $form[0].reset()

        if (response.data.redirect && response.data.redirect.length > 0) {
          window.location.href = response.data.redirect
          return
        }
      } else {
        $form.find('.message').hide().html('Something went wrong, please try again').fadeIn()
      }
      $submit.attr('disabled', false)
    }).catch(err => {
      console.error(err)
      $form.find('.message').hide().html('Something went wrong, please try again').fadeIn()
      $submit.attr('disabled', false)
    })
  }

  $forms.each(function () {
    const $form = $(this)
    $form.validate(config)
  })


  /* Handle file upload */
  $forms.each(function () {
    const $form = $(this)
    const $fileFields = $form.find('input[type="file"]')

    $fileFields.on('change', function () {
      const $this = $(this)
      const file_obj = $this.prop('files')
      let form_data = new FormData()
      form_data.append('action', 'form_file_upload')
      for (let i = 0; i < file_obj.length; i++) {
        form_data.append('file[]', file_obj[i])
      }
      form_data.append('action', 'form_builder_file_upload')
      form_data.append('security', windowConfig.fileUploadNonce)
      form_data.append('name', $this.attr('name'))
      form_data.append('id', $this.attr('id'))

      Axios.post(windowConfig.ajaxurl, form_data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(response => {
        /* Handle file upload field UI */
        if (response.data && response.data.data) {
          const $description = $this.closest('.form-file').find('.form-file-description')
          let format = response.data.data.join(', ')
          $description.html(format)
        }
      }).catch(err => {
        console.error(err)
      })
    })
  })
}
