import Axios from 'axios'
import 'jquery-validation';
import intlTelInput from 'intl-tel-input';
import $ from 'jquery'

export default function () {
  //Retrieve forms with experian enabled
  const $forms = $('form[data-experian="true"]');

  const errors = {
    name: 'Please enter your name',
    first_name: 'Please enter your first name',
    last_name: 'Please enter your last name',
    email: 'Please enter a valid email address',
    mobile: 'Please enter a valid phone number',
    validate: 'Validating your details',
    failure: 'Please check your details and try again.',
  }

  //Add strict email validation to ensure valid email is entered
  $.validator.addMethod('strictEmail',
    function (value, element) {
      return this.optional(element) || /[a-z0-9]+@[a-z]+\.[a-z]+/.test(value);
    }, 'Please enter a valid email address'
  );

  //jquery validate config
  const config = {
    ignore: ':hidden',
    ignoreTitle: true,
    rules: {
      email: {
        required: true,
        strictEmail: true,
      },
    },
    messages: {
      name: errors.name,
      first_name: errors.first_name,
      last_name: errors.last_name,
      email: errors.email,
      mobile: errors.mobile,
    },
    errorElement: 'label',
    errorClass: 'validate-error',
  }

  //Submit button handler
  config.submitHandler = function (form) {

    console.log('Hello...');

    //Retrieve inputs to validate
    const $form = $(form);
    const $phone = $form.find('[name="mobile"]');
    const $email = $form.find('[type="email"]');
    const $loader = $form.find('.experian-loader');

    let rawPhone = $phone.val();
    let response = '';
    let data = $form.serializeArray();

    $.each(data, function(i, field){
      if(field.name =='g-recaptcha-response' ) {
        response = field.value;
      }
    });

    // Validation Recaptcha Response
    if(response == ''){
      return false;
    }


    // Phone Validation
    if (!rawPhone.length || window[$form.attr('intl')].isValidNumber() == false) {
      $form.validate().showErrors({
        'mobile': errors.mobile,
      });

      return false;
    }

    // Error Validation
    if ($email.attr('invalid').length || $phone.attr('invalid').length) {

      if ($email.attr('invalid').length) {
        $form.validate().showErrors({
          'email': errors.email,
        });
      }

      if ($phone.attr('invalid').length) {
        $form.validate().showErrors({
          'mobile': errors.mobile,
        });
      }

      $loader.html('<span>' + errors.failure + '</span>');

      return false;
    }

    // Update phone number with country code
    // rawPhone = window[$form.attr('intl')].getNumber();
    $phone.val(window[$form.attr('intl')].getNumber());

    return true;
  }


  /*
 Form Experian API Validator : Moved from within the submitHandler
 */
  function ExperianValidator(form) {
    // Experian API vars
    const windowConfig = window.validator_api;

    //Retrieve inputs to validate
    const $form = $(form);
    const $email = $form.find('[type="email"]');
    const $phone = $form.find('[name="mobile"]');
    const $submit = $form.find('[type="submit"]');
    const $loader = $form.find('.experian-loader');

    // Input Type vars
    let typingPhoneTimer;
    let typingEmailTimer;
    let doneTypingInterval = 500;

    /* Experian API: Mobile Phone Input Validation
    // phone input:
    // on keyup, start the countdown
    // on keydown, clear the countdown
     */
    $phone.on('keyup', function () {
      clearTimeout(typingPhoneTimer);
      typingPhoneTimer = setTimeout(validatePhone, doneTypingInterval);
    });
    $phone.on('keydown', function () {
      $loader.html('');
      clearTimeout(typingPhoneTimer);
    });

    // Experian API Mobile Phone Validation
    function validatePhone() {
      //Retrieve values to validate
      let rawPhone = $phone.val();

      // Phone Validation
      if (!rawPhone.length || window[$form.attr('intl')].isValidNumber() == false) {
        $form.validate().showErrors({
          'mobile': errors.mobile,
        });

        return false;
      }

      // Update phone number with country code and props
      rawPhone = window[$form.attr('intl')].getNumber();
      $phone.prop('disabled', true);
      $phone.attr('invalid', true);
      $submit.prop('disabled', true);

      // Loading Validation Message
      $loader.html('<span class="loading">' + errors.validate + '</span>');

      // Experian API Request
      Axios.post(windowConfig.url + 'phone/validate/v2', {
        number: rawPhone,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'accept': 'application/json',
          'Auth-Token': windowConfig.key,
          'Add-Metadata': true,
        },
      }).then((phoneResponse) => {
          let phoneConfidence = phoneResponse.data.result.confidence;
          let validPhone = false;

          /**
           * Note: Acceptance Criteria
           * Phone accept case: Verified, Absent, Unknown, No coverage.
           */

          //Check acceptance criteria and set proceed status
          if (phoneConfidence === 'Verified' || phoneConfidence === 'Absent' || phoneConfidence === 'Unknown' || phoneConfidence === 'No coverage') {
            // console.log('Phone Number accepted');

            validPhone = true;
          } else {
            // console.log('Bad Phone number!');

            // 'mobile': `Mobile has been flagged for spam. Confidence: ${phoneConfidence}`,
            $form.validate().showErrors({
              'mobile': errors.mobile,
            });
            validPhone = false;
          }

          // Check proceed status
          if (validPhone) {
            $loader.html('');
            $phone.attr('invalid', '');
          } else {
            $loader.html('<span>' + errors.failure + '</span>');
          }

          $phone.prop('disabled', false);
          $submit.prop('disabled', false);
        }
      ).catch((error) => {
        console.log(error);
        $loader.html('<span>' + errors.failure + '</span>');
        $phone.prop('disabled', false);
        $phone.attr('invalid', true);
      });

    }

    /* Experian API: Email Input Validation
    // email input:
    // on keyup, start the countdown
    // on keydown, clear the countdown
     */
    $email.on('keyup', function () {
      clearTimeout(typingEmailTimer);
      typingEmailTimer = setTimeout(validateEmail, doneTypingInterval);
    });
    $email.on('keydown', function () {
      $loader.html('');
      clearTimeout(typingEmailTimer);
    });

    // Experian API Email Validation
    function validateEmail() {
      //Retrieve values to validate
      let rawEmail = $email.val();

      // Email Validation
      if (!rawEmail.length || !$form.validate().element($email)) {
        $form.validate().showErrors({
          'email': errors.email,
        });

        return false;
      }

      // Update props
      $email.prop('disabled', true);
      $email.attr('invalid', true);
      $submit.prop('disabled', true);

      // Loading Validation
      $loader.html('<span class="loading">' + errors.validate + '</span>');

      // Experian API Request
      Axios.post(windowConfig.url + 'email/validate/v2', {
        email: rawEmail,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'accept': 'application/json',
          'Auth-Token': windowConfig.key,
          'Add-Metadata': true,
        },
      }).then((emailResponse) => {
          let emailConfidence = emailResponse.data.result.verbose_output;
          let validEmail = false;

          /**
           * Note: Acceptance Criteria
           * Email accept case: verified, mailboxFull, roleAccount.
           */

          //Check acceptance criteria and set proceed status
          if (emailConfidence === 'verified' || emailConfidence === 'mailboxFull' || emailConfidence === 'roleAccount' || emailConfidence === 'unknown' || emailConfidence === 'timeout' || emailConfidence === 'acceptAll' || emailConfidence === 'relayDenied' || emailConfidence === 'BLANK') {
            // console.log('Email Accepted');

            validEmail = true;
          } else {
            // console.log('Bad Email');

            // 'email': `Email has been flagged for spam. Confidence: ${emailConfidence}`,
            $form.validate().showErrors({
              'email': errors.email,
            });
            validEmail = false;
          }

          // Check proceed status
          if (validEmail) {
            $loader.html('');
            $email.attr('invalid', '');
          } else {
            $loader.html('<span>' + errors.failure + '</span>');
          }

          $email.prop('disabled', false);
          $submit.prop('disabled', false);
        }
      ).catch((error) => {
        console.log(error);
        $loader.html('<span>' + errors.failure + '</span>');
        $email.prop('disabled', false);
        $email.attr('invalid', true);
      });

    }
  }

  /*
   Enable form validation for each form
   */
  $forms.each(function () {
    const $form = $(this);
    const mobile = $form.find('#mobile');

    // Initialise country code per phone input
    if (mobile.length) {
      window[$form.attr('intl')] = intlTelInput(mobile[0], {
        initialCountry: 'AU',
        separateDialCode: true,
        autoPlaceholder: 'off',
        utilsScript:
          'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.16/js/utils.js',
      });
    }

    // Action input validation
    $form.validate(config);
    // Action Experian Validations
    ExperianValidator(this);
  })
}

