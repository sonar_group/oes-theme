export default function () {
  let $infographic = $('.infographic__item')

  $infographic.click(function () {
      var $this = $(this),
      $level = $('#level-of-study-' + $this.data('id'));

    if ($this.hasClass('active')) {
      $this.removeClass('active');
      $level.addClass('show');
      return false;
    }

    $infographic.removeClass('active');
    $this.addClass('active');
    $('.results').removeClass('show');
    $level.addClass('show');
  });
}
