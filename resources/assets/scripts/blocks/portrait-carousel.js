import 'slick-carousel'

export default () => {
  let $slick = $('.portrait-carousel__slider');
  let $previous = $('.portrait-carousel__controls .icon.slick-prev');
  let $next = $('.portrait-carousel__controls .icon.slick-next');

  $slick.slick ({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    autoplay: false,
    prevArrow: $previous,
    nextArrow: $next,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
}
