import 'slick-carousel'

export default () => {
  let $slick = $('.testimonial-carousel__slider');
  let $previous = $('.testimonial-carousel__controls .icon.slick-prev');
  let $next = $('.testimonial-carousel__controls .icon.slick-next');

  $slick.slick ({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    autoplay: false,
    prevArrow: $previous,
    nextArrow: $next,
  });
}
