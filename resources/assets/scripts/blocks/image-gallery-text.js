import $ from 'jquery'

export default function() {
    let $mobileCarousel = $('.image-gallery-text-slider')
    /* Slick needs no get Reinitialized on window Resize after it was destroyed */
    $(window).on('load resize orientationchange', function() {
      /* Initializes a slick carousel only on mobile screens */
      // slick on mobile
      if ($(window).width() > 768) {
        if ($mobileCarousel.hasClass('slick-initialized')) {
          $mobileCarousel.slick('unslick');
        }
      }
      else{
        if (!$mobileCarousel.hasClass('slick-initialized')) {
          if ($mobileCarousel.length > 0) {
            $mobileCarousel.not('.slick-initialized').slick({
              autoplay: false,
              arrows: false,
              infinite: false,
              dots: true,
              speed: 500,
              slidesToShow: 1,
              centerMode: true,
              slidesToScroll: 1,
              mobileFirst: true,
              adaptiveHeight: true,
            })
          }
        }
      }
    });
}