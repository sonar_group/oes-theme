export default function () {

  $(window).on('load', function() {
    const accordion = $('#verticalAccordion');
    const collapse = accordion.find('.collapse');

    if ($(window).width() < 960) {
      accordion.removeClass('width');
      collapse.removeClass('width');
    } else {
      accordion.addClass('width');
      collapse.addClass('width');
    }

    collapse.not('.show').each((index, element) => {
      $(element).parent().find('[data-toggle=\'collapse\']').addClass('collapsed');
    });

    let $cardHeader = $('#verticalAccordion .card-header')

    $cardHeader.click(function () {
      var $this = $(this)

      if ($this.parent().hasClass('active')) {
        $this.parent().removeClass('active');
        return false;
      }

      $cardHeader.parent().removeClass('active');
      $this.parent().addClass('active');
    });
  });
}
