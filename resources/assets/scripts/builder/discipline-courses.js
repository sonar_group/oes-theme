import $ from 'jquery'

export default function() {

    // get query string from single news page
    function getURL() {
      var vars = [], hash;
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
      }
      return vars;
    }

    var $levelOfStudyID = getURL()['level_of_study'];
    if ($levelOfStudyID) {
        const $disciplineID = $( 'form#discipline-courses__form input[name="discipline"]' ).val()
        $( 'form#discipline-courses__form input[name="level_of_study"]' ).val( $levelOfStudyID )
        $( '.discipline-courses__selector' ).removeClass( 'active' )
        $( '.discipline-courses__selector[href="?level_of_study=' + $levelOfStudyID + '"' ).addClass( 'active' )
        loadCourses( $levelOfStudyID, $disciplineID )
    }

    // Level of Study Filter
    $( '.discipline-courses__form a' ).on( 'click', function(e) {
        e.preventDefault()
        const $idVal = ( $( this ).data('id') === 'All' ) ? '' : $( this ).data('id')
        const $disciplineID = $( 'form#discipline-courses__form input[name="discipline"]' ).val()
        $( 'form#discipline-courses__form input[name="level_of_study"]' ).val( $idVal )
        $( '.discipline-courses__selector' ).removeClass( 'active' )
        $( this ).addClass( 'active' )
        loadCourses( $idVal, $disciplineID )
    } )

    function loadCourses( $id, $disciplineID ){
        var str = '&discipline=' + $disciplineID + '&level_of_study=' + $id + '&action=discipline_courses';
        $.ajax({
            type: 'POST',
            dataType: 'html',
            url: window.ajax_posts.ajaxurl,
            data: str,
            success: function(data){
                var $data = $(data);
                if($data.length){
                    $('.discipline-courses__courses').html($data);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                window.console.error(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
            },

        });
        return false;
    }
}
