import $ from 'jquery'

export default function() {

    // get query string from single news page
    function getURL() {
      var vars = [], hash;
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
      }
      return vars;
    }

    var $disciplineID = getURL()['discipline'];
    if ($disciplineID) {
        const $levelOfStudyID = $( 'form#levels-of-study-courses__form input[name="level_of_study"]' ).val()
        $( 'form#levels-of-study-courses__form input[name="discipline"]' ).val( $disciplineID )
        $( '.levels-of-study-courses__selector' ).removeClass( 'active' )
        $( '.levels-of-study-courses__selector[href="?discipline=' + $disciplineID + '"' ).addClass( 'active' )
        loadCourses( $levelOfStudyID, $disciplineID )
    }

    // Level of Study Filter
    $( '.levels-of-study-courses__form a' ).on( 'click', function(e) {
        e.preventDefault()
        const $disciplineID = ( $( this ).data('id') === 'All' ) ? '' : $( this ).data('id')
        const $levelOfStudyID = $( 'form#levels-of-study-courses__form input[name="level_of_study"]' ).val()
        $( 'form#levels-of-study-courses__form input[name="discipline"]' ).val( $disciplineID )
        $( '.levels-of-study-courses__selector' ).removeClass( 'active' )
        $( this ).addClass( 'active' )
        loadCourses( $levelOfStudyID, $disciplineID )
    } )

    function loadCourses( $levelOfStudyID, $disciplineID ){
        var str = '&discipline=' + $disciplineID + '&level_of_study=' + $levelOfStudyID + '&action=levels_of_study_courses';
        $.ajax({
            type: 'POST',
            dataType: 'html',
            url: window.ajax_posts.ajaxurl,
            data: str,
            success: function(data){
                var $data = $(data);
                if($data.length){
                    $('.levels-of-study-courses__courses').html($data);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                window.console.error(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
            },

        });
        return false;
    }
}
