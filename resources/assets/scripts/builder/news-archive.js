import $ from 'jquery'

export default function () {

  // Category Filter
  $('.news-archive__categories__form a').on('click', function (e) {
    e.preventDefault()
    const $idVal = ($(this).data('id') === 'All') ? '' : $(this).data('id')
    $('form#news-archive__categories__form input[name="category"]').val($idVal)
    $('.news-archive__category').removeClass('active')
    $(this).children('.news-archive__category').addClass('active')
    load_categories($idVal)
  })

  // Load More Button

  $('#more_posts').on('click', function () { // When btn is pressed.
    $('#more_posts').attr('disabled', true); // Disable the button, temp.
    load_posts();
  });

  var ppp = 6; // Post per page
  var cat = $('#more_posts').data('category');
  var featured_news = $('#more_posts').data('featured');
  var pageNumber = 1;

  function load_posts() {
    pageNumber++
    var str = '&featured_news=' + featured_news + '&cat=' + cat + '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=more_post_ajax';
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: window.ajax_posts.ajaxurl,
      data: str,
      success: function (data) {
        if (data.html) {
          $('#ajax-posts').append(data.html);
          if (data.posts_number > $('#ajax-posts').find('.news-archive__article').length) {
            $('#more_posts').attr('disabled', false);
          } else {
            $('#more_posts').attr('disabled', true);
          }
        } else {
          $('#more_posts').attr('disabled', true);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        window.console.error(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
      },

    });
    return false;
  }

  function load_categories(cat_id) {
    pageNumber = 1
    $('#more_posts').data('category', cat_id)
    cat = $('#more_posts').data('category');
    var str = '&featured_news=' + featured_news + '&cat=' + cat + '&ppp=' + ppp + '&action=category_ajax';
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: window.ajax_posts.ajaxurl,
      data: str,
      success: function (data) {
        if (data.html) {
          //console.log(data)
          $('#ajax-posts').html(data.html);
          if (data.posts_number > $('#ajax-posts').find('.news-archive__article').length) {
            $('#more_posts').attr('disabled', false);
          } else {
            $('#more_posts').attr('disabled', true);
          }
        } else {
          $('#ajax-posts').html('Sorry! No data available.');
          $('#more_posts').attr('disabled', true);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        window.console.error(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
      },

    });
    return false;
  }
}
