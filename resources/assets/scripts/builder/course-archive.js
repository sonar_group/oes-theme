import $ from 'jquery'

export default function() {

  // Discipline Filter
  $( '.course-archive__selector' ).on( 'click', function(e) {
    e.preventDefault()
    const $idVal = ( $( this ).data('id') === 'All' ) ? '' : $( this ).data('id')
    $( 'form#course-archive__form input[name="discipline"]' ).val( $idVal )
    $( '.course-archive__selector' ).removeClass( 'active' )
    $( this ).addClass( 'active' )
    loadCourses( $idVal )
  } )

  function loadCourses( $id ){
    var str = '&discipline=' + $id + '&action=course_archive';
    $.ajax({
      type: 'POST',
      dataType: 'html',
      url: window.ajax_posts.ajaxurl,
      data: str,
      success: function(data){
        var $data = $(data);
        if($data.length){
          $('.course-archive__courses').html($data);
        }
      },
      error : function(jqXHR, textStatus, errorThrown) {
        window.console.error(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
      },

    });
    return false;
  }
}
