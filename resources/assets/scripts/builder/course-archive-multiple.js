import $ from 'jquery'

export default function () {

    let discipline = []
    let level_of_study = []
    let keyword = ''

    // get query string from single news page
    function getURL() {
      var vars = [], hash;
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
      }
      return vars;
    }

    var $disciplineID = getURL()['discipline']
    if ($disciplineID) {
        discipline.push($disciplineID)
        $('form#course-archive-multiple__form input[name="discipline"]').val($disciplineID)
        $( '.course-archive-multiple__selector a[href="?discipline=' + $disciplineID + '"' ).addClass( 'active' )
    }

    var $levelOfStudyID = getURL()['level_of_study']
    if ($levelOfStudyID) {
        level_of_study.push($levelOfStudyID)
        $('form#course-archive-multiple__form input[name="level_of_study"]').val($levelOfStudyID)
        $( '.course-archive-multiple__selector a[href="?level_of_study=' + $levelOfStudyID + '"' ).addClass( 'active' )
    }

    var $keywordInput = getURL()['keyword']
    if ($keywordInput) {
      keyword = $keywordInput
      $('.course-archive-multiple__keyword').val(keyword)
    }

    if ($disciplineID || $levelOfStudyID || $keywordInput) {
      loadCourses(discipline, level_of_study, keyword)
    }

  // Discipline Filter
  $('.course-archive-multiple__selector a').on('click', function (e) {
    e.preventDefault()

    switch ($(this).data('type')) {
      case 'discipline':
        if($(this).hasClass('active')) {
          $('.course-archive-multiple__selector a[href="?discipline=' + $(this).data('id') + '"').removeClass('active')
          discipline.splice($.inArray($(this).data('id'), discipline), 1);
        } else {
          discipline.push($(this).data('id'))
          $('.course-archive-multiple__selector a[href="?discipline=' + $(this).data('id') + '"').addClass( 'active' )
        }
        break
      case 'level_of_study':
        if($(this).hasClass('active')) {
          $('.course-archive-multiple__selector a[href="?level_of_study=' + $(this).data('id') + '"').removeClass('active')
          level_of_study.splice($.inArray($(this).data('id'), level_of_study), 1);
        } else {
          level_of_study.push($(this).data('id'))
          $('.course-archive-multiple__selector a[href="?level_of_study=' + $(this).data('id') + '"').addClass( 'active' )
        }
    }

    loadCourses(discipline, level_of_study, keyword)
  })

  // Keyword Filter
  $('.course-archive-multiple__keyword-button').on('click', function(e) {
    e.preventDefault()

    keyword = $('.course-archive-multiple__keyword').val()
    loadCourses(discipline, level_of_study, keyword)
  })

  $('.course-archive-multiple__keyword').on('keypress', function(event) {
    if ( event.which == 13 ) {
      event.preventDefault();

      keyword = $('.course-archive-multiple__keyword').val()
      loadCourses(discipline, level_of_study, keyword)
    }
  })

  $('.course-archive-multiple__reset-button').on('click', function() {
    $('.course-archive-multiple__selector a').removeClass('active')
    $('.course-archive-multiple__keyword').val('')
    discipline = []
    level_of_study = []
    keyword = ''
    loadCourses(discipline, level_of_study, keyword)
  })

  function loadCourses(discipline, level_of_study, keyword) {
    var str = '&keyword=' + keyword + '&discipline=' + discipline + '&level_of_study=' + level_of_study + '&action=course_archive_multiple';
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: window.ajax_posts.ajaxurl,
      data: str,
      success: function (data) {
        if (data.html) {
          $('.course-archive-multiple__courses').html(data.html)
        } else {
          $('.course-archive-multiple__courses').text('Sorry! There are no results that match the current filters.')
        }
        if (data.courses_number > 0) {
          $('.course-archive-multiple__info').show()
          $('.course-archive-multiple__courses-count').html(data.courses_number)
          // if (data.level_of_study_name) {
          //   $('.course-archive-multiple__level-of-study-name').html(data.level_of_study_name)
          // } else {
          //   $('.course-archive-multiple__level-of-study-name').html('')
          // }
          // if (data.discipline_name) {
          //   $('.course-archive-multiple__discipline-name').html(data.discipline_name)
          // } else {
          //   $('.course-archive-multiple__discipline-name').html('')
          // }
        } else {
          $('.course-archive-multiple__info').hide()
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        window.console.error(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
      },

    });
    return false;
  }
}
