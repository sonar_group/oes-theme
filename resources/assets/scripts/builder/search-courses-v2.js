import $ from 'jquery'

export default function() {
    var $disciplineList = $('.search-courses-v2__disciplines #discipline').data('disciplines')
    if ($disciplineList) {
        $disciplineList = $disciplineList.split('|')
    }
    $('.search-courses-v2 .search-courses-v2__results').hide()
    $('.search-courses-v2 .no-results').hide()

    $('.search-courses-v2__course').each( function() {
        $(this).parent().hide()
    })
    $('.search-courses-v2__disciplines #discipline').autocomplete({
        appendTo: $('.search-courses-v2__disciplines'),
        source: $disciplineList,
        select: function(event, ui) {
          // yes
            courseNavigate(ui.item.value)
            // console.log('select action: ' + ui.item.value)
        },
    })

    $('.search-courses-v2__disciplines #discipline').on('keypress', function(event) {
      if ( event.which == 13 ) {
        event.preventDefault();
        // console.log('enter action: ' + $(this).val())
        courseNavigate($(this).val())
      }
    });

    function courseNavigate(selected_discipline) {
      let hasNav = false
      $('.search-courses-v2__course').each( function() {
        console.log(selected_discipline + '  ' + $(this).data('course') + '  ' + $(this).data('link'))
          if (selected_discipline.trim().toLowerCase() == $(this).data('course').trim().toLowerCase()) {
              window.location.href = $(this).data('link')
              hasNav = true
          }
      })
      if (hasNav == false) {
        window.location.href = '/online-courses/?keyword=' + selected_discipline
      }
    }
}
