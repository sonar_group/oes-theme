import $ from 'jquery'

export default function() {

    // Job Filter
    $('.job-archive__filter_values .filter-label').on('click', function() {
        $(this).parent().children('.filter-check').toggleClass('open')
    })

    $( '.job-archive__filter_values .filter-check li' ).on( 'click', function(e) {
        e.preventDefault()
        // Default filter search
        let location = $('.job-archive__filter_values.locations .current-sort').data('sorting')
        let department = $('.job-archive__filter_values.departments .current-sort').data('sorting')
        let date = $('.job-archive__filter_values.dates .current-sort').data('sorting')
        // Get the new selected filters
        switch ($( this ).data('type')) {
            case 'location':
                location = $( this ).data('id')
                $('.job-archive__filter_values.locations .current-sort').data('sorting', $( this ).data('id'))
                $('.job-archive__filter_values.locations .current-sort__details').text($(this).text())
                break
            case 'department':
                department = $( this ).data('id')
                $('.job-archive__filter_values.departments .current-sort').data('sorting', $( this ).data('id'))
                $('.job-archive__filter_values.departments .current-sort__details').text($(this).text())
                break
            case 'date':
                date = $( this ).data('id')
                $('.job-archive__filter_values.dates .current-sort').data('sorting', $( this ).data('id'))
                $('.job-archive__filter_values.dates .current-sort__details').text($(this).text())
        }
        $(this).parent().children().removeClass('active')
        $(this).addClass('active')
        $(this).parent().parent().children('.filter-label .current-sort__details').text($(this).text())
        loadJobs( location, department, date )
    } )

    function loadJobs( location, department, date ){
        var str = '&location=' + location + '&department=' + department + '&date=' + date + '&action=job_archive';
        $.ajax({
            type: 'POST',
            dataType: 'html',
            url: window.ajax_posts.ajaxurl,
            data: str,
            success: function(data){
                var $data = $(data)
                if($data.length){
                    $('.job-archive__articles').html($data)
                } else {
                    $('.job-archive__articles').text('Sorry! There are no results that match the current filters.')
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                window.console.error(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
            },

        });
        return false;
    }
}