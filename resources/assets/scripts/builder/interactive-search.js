import $ from 'jquery'

export default function() {

    $('.interactive-search__results').hide()

    $('.interactive-search__select__current').on('click', function() {
        $('.interactive-search__select__list').toggleClass('open')
    })

    $('.interactive-search__select__list li').on('click', function() {
        $('.interactive-search__select__current').html($(this).data('name'))
        $('.interactive-search__select__list li').removeClass('selected')
        $(this).addClass('selected')
        $('.interactive-search__select__list').removeClass('open')
        $('.interactive-search__levels-of-study-option').removeClass('selected')
        loadCourses($(this).data('id'), null)

        if (!$('.interactive-search__results').is(':visible')) {
            $('.interactive-search__results').fadeIn()
          $([document.documentElement, document.body]).animate({
            scrollTop: $('#scrollToButton').offset().top -140,
          }, 500);
        }
    })

    $('.interactive-search__levels-of-study-option').on('click', function() {
      if (!$(this).hasClass('selected')) {
        $('.interactive-search__levels-of-study-option').removeClass('selected')
        $(this).addClass('selected')
        var $disciplineID = $('.interactive-search__select__list li.selected').data('id')

        loadCourses($disciplineID, $(this).data('id'))
      } else {
        $('.interactive-search__levels-of-study-option').removeClass('selected')
        loadCourses($disciplineID, null)
      }
    })

    function loadCourses($disciplineID, $levelOfStudyID){
        var str = '&discipline=' + $disciplineID + '&action=interactive_search';
        if ($levelOfStudyID !== null) {
          str = str + '&level_of_study=' + $levelOfStudyID;
        }
        $.ajax({
            type: 'POST',
            dataType: 'html',
            url: window.ajax_posts.ajaxurl,
            data: str,
            success: function(data){
                var $data = $(data);
                if($data.length){
                    $('.interactive-search__courses').html($data);
                } else {
                    $('.interactive-search__courses').html('No results found.');
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                window.console.error(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
            },

        });
        return false;
    }
}
