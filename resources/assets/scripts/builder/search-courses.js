import $ from 'jquery'

export default function() {
    var $disciplineList = $('.search-courses__disciplines #discipline').data('disciplines')
    if ($disciplineList) {
        $disciplineList = $disciplineList.split('|')
    }
    $('.search-courses .search-courses__results').hide()
    $('.search-courses .no-results').hide()

    $('.search-courses__course').each( function() {
        $(this).parent().hide()
    })
    $('.search-courses__disciplines #discipline').autocomplete({
        appendTo: $('.search-courses__disciplines'),
        source: $disciplineList,
        search: function() {
            coursesUpdate($(this).val())
        },
        change: function() {
            coursesUpdate($(this).val())
        },
        close: function() {
            coursesUpdate($(this).val())
        },
    })

    function coursesUpdate(selected_discipline) {
        let hasShown = false
        $('.search-courses__course').each( function() {
            if (!selected_discipline) {
                $(this).parent().fadeOut()
                return
            }
            if (selected_discipline && !$(this).data('discipline').toString().toLowerCase().includes(selected_discipline.toLowerCase()) && !$(this).data('course').toString().toLowerCase().includes(selected_discipline.toLowerCase())) {
                $(this).parent().fadeOut()
                return
            }
            if (!$('.search-courses .search-courses__results').is(':visible')) {
                $('.search-courses .search-courses__results').fadeIn()
            }
            if ($('.search-courses .no-results').is(':visible')) {
                $('.search-courses .no-results').fadeOut()
            }
            hasShown = true
            $(this).parent().fadeIn()
        })
        if (hasShown == false && selected_discipline) {
            if (!$('.search-courses .search-courses__results').is(':visible')) {
                $('.search-courses .search-courses__results').fadeIn()
            }
            if (!$('.search-courses .no-results').is(':visible')) {
                $('.search-courses .no-results').fadeIn()
            }
        } else if (hasShown == false  && $('.search-courses .search-courses__results').is(':visible')) {
            $('.search-courses .search-courses__results').fadeOut()
        }
    }
}
