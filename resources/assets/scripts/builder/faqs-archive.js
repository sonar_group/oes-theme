import $ from 'jquery'

export default function () {

  // FAQs Filter
  $('.faqs-archive__selector').on('click', function (e) {
    e.preventDefault()
    const $idVal = ($(this).data('id') === 'All') ? '' : $(this).data('id')
    $('form#faqs-archive__form input[name="faqs_category"]').val($idVal)
    $('.faqs-archive__selector').removeClass('active')
    $(this).addClass('active')
    loadCourses($idVal)
  })

  function loadCourses($id) {
    var str = '&faqs_category=' + $id + '&action=faqs_archive';
    $.ajax({
      type: 'POST',
      dataType: 'html',
      url: window.ajax_posts.ajaxurl,
      data: str,
      success: function (data) {
        var $data = $(data);
        if ($data.length) {
          $('.faqs-archive__faqs').html($data);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        window.console.error(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
      },

    });
    return false;
  }
}
