import $ from 'jquery'

export default function () {

  // search keyword
  $('.units-archive form').on('submit', function (e) {
    e.preventDefault()
    var $keyword = $('.units-archive form input[name="s"]').val()
    loadUnits($keyword)
  })

  // reset search
  $('.units-archive .reset-search').on('click', function (e) {
    e.preventDefault()
    $('.units-archive form input[name="s"]').val('')
    loadUnits('')
  })

  function loadUnits($keyword) {
    var str = '&units_keyword=' + $keyword + '&action=units_archive';
    $.ajax({
      type: 'POST',
      dataType: 'html',
      url: window.ajax_posts.ajaxurl,
      data: str,
      success: function (data) {
        console.log($data)
        var $data = $(data);
        if ($data.length) {
          $('.units-archive__units').html($data);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        window.console.error(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
      },

    });
    return false;
  }
}
