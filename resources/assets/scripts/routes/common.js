// import newArchive from '../builder/news-archive'
// import courseArchive from '../builder/course-archive';
// import courseArchiveMultiple from '../builder/course-archive-multiple';
// import interactiveSearch from '../builder/interactive-search';
// import portraitCarousel from '../blocks/portrait-carousel';
// import testimonialCarousel from '../blocks/testimonial-carousel';
// import searchCourses from '../builder/search-courses';
// import searchCoursesV2 from '../builder/search-courses-v2';
// import imageGalleryText from '../blocks/image-gallery-text';
// import stickyBar from '../components/sticky-bar';
// import jobArchive from '../builder/job-archive';
// import levelsOfStudyInfographic from '../blocks/levels-of-study-infographic';
// import stickySubNav from '../components/sticky-sub-nav';
// import verticalAccordion from '../blocks/vertical-accordion';
// import AOS from 'aos';
// import disciplineCourses from '../builder/discipline-courses';
// import levelsOfStudyCourses from '../builder/levels-of-study-courses';
// import faqsArchive from '../builder/faqs-archive'
// import unitsArchive from '../builder/units-archive'
// import forms from '../forms/forms'
// import countdown from '../components/countdown'

export default {
  init() {
    // JavaScript to be fired on all pages
    // newArchive()
    // courseArchive()
    // courseArchiveMultiple()
    // interactiveSearch()
    // portraitCarousel()
    // testimonialCarousel()
    // searchCourses()
    // searchCoursesV2()
    // imageGalleryText()
    // stickyBar()
    // jobArchive()
    // levelsOfStudyInfographic()
    // stickySubNav()
    // verticalAccordion()
    // this.aos()
    // this.popover()
    // disciplineCourses()
    // levelsOfStudyCourses()
    // faqsArchive()
    // unitsArchive()
    // countdown()
  },
  // aos() {
  //   let scrollRef = 0;
  //   window.addEventListener('scroll', function() {
  //     // increase value up to 10, then refresh AOS
  //     scrollRef <= 1 ? scrollRef++ : AOS.init({once: true});
  //   });
  // },
  // popover() {
  //   $(function () {
  //     $('[data-toggle="popover"]').popover()
  //   })
  // },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    //forms()
  },
};
