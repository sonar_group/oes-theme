export default function () {
  let $subMenu = $('.sticky-sub-nav__container')
  const subMenuItem = $('.sticky-sub-nav__item')

  if ($subMenu.length) {
    subMenuItem.click(function() {
      subMenuItem.removeClass('active');
      $(this).addClass('active');
    })
    $(window).on('scroll', () => {
      subMenuItem.each(function() {
        const href = $(this).attr('href')
        if (href[0] === '#' && href.length > 1 && $(href).offset() !== undefined) {
          if (($(window).scrollTop() + ($(window).height() / 2) > $(href).offset().top) & ($(window).scrollTop() + ($(window).height() / 2) < ($(href).offset().top + $(href).height()))) {
            $('.sticky-sub-nav__item').removeClass('active')
            $('a[href="' + href + '"]').closest('a').addClass('active')
            return
          }
        }
      })
    })
  }
}
