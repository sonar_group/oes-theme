import Cookies from 'js-cookie';

export default function () {
  const close = $('#stickyBarClose')
  const stickyBar = $('#stickyBar')

  close.click(function (e) {
    e.preventDefault()
    stickyBar.css('display', 'none')
    Cookies.set('ticker', 'exists', {expires: 1})
  })
  if (Cookies.get('ticker', 'exists')) {
    stickyBar.css('display', 'none')
  } else {
    stickyBar.css('display', 'flex')
  }
}
