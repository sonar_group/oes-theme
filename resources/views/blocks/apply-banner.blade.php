{{--
  Title: Apply Banner
  Description: Apply Banner Block - Content Blocks
  Icon: cover-image
  Keywords: Apply Banner Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $apply = App::apply();
@endphp

<section class="block apply-banner">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        @if($apply['heading'])
          <h2>{{$apply['heading']}}</h2>
        @endif
      </div>
      <div class="col-sm-6">
        @if($apply['content'])
          {!! $apply['content'] !!}
        @endif
      </div>
    </div>
  </div>
</section>
