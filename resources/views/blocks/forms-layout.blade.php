{{--
  Title: Forms Layout
  Description: Forms Layout - Forms and Content Blocks
  Icon: text-page
  Keywords: Forms Layout Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $form = App::globalFormFields();
  $block = get_field('block');
@endphp

<section class="block forms-layout">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        @if($block['form'] === 'apply')
          @include('forms.apply')
        @endif
        @if($block['form'] === 'contact')
          @include('forms.contact')
        @endif
      </div>
      <div class="col-md-4">

        @if(!empty($block['tiles']))
          @foreach($block['tiles'] as $tile)
            <div class="column col-12">
              <div class="content">
                @if(isset($tile['heading']))
                  <h4>{{ $tile['heading'] }}</h4>
                @endif
                @if(isset($tile['copy']))
                  {!! $tile['copy'] !!}
                @endif
                @if($tile['link_repeater'])
                  <div class="link mt-auto">
                    @foreach($tile['link_repeater'] as $link)
                      <p><a class="text-link" href="{{ $link['link']['url'] }}" target="{{ $link['link']['target'] }}">
                          {{ $link['link']['title'] }}
                        </a></p>
                    @endforeach
                  </div>
                @endif
              </div>
            </div>
          @endforeach
        @endif

      </div>
    </div>
  </div>
</section>
