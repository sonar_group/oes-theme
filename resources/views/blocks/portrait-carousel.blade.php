{{--
  Title: Portrait Carousel
  Description: Portrait Carousel - Content Blocks
  Icon: embed-photo
  Keywords: Portrait Carousel Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

@php
  $block = get_field('block');
@endphp

<section class="block portrait-carousel">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($block['heading'])
          <h2>{{$block['heading']}}</h2>
        @endif
      </div>
      <div class="col-md-5 heading">
        @if($block['copy'])
          {!! $block['copy'] !!}
        @endif
      </div>
      <div class="col-md-7">
        @if(!empty($block['carousel_item']) && $block['carousel_item'])
          <div class="portrait-carousel__controls">
              <span class="icon slick-prev">PREV</span>
              <span class="icon slick-next">NEXT</span>
          </div>
          <div class="portrait-carousel__slider">
            @foreach($block['carousel_item'] as $r => $item)
              <div class="slide" data-toggle="modal" data-target="#videoModal_portrait_{{$r}}">
                <div class="bg-video" style="background-image:url({!! ($item['image'])? $item['image']['url']: '' !!});">
                  <span class="icon">&times;</span>
                  <div class="content">
                    <p>{{$item['name']}}</p>
                    <p>{{$item['course']}}</p>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          @foreach($block['carousel_item'] as $r => $item)
            @include('components.video-modal', ['video' => $item['video_url'], 'r' => $r, 'modalName' => '_portrait'])
          @endforeach
        </div>
      @endif
    </div>
  </div>
</section>




