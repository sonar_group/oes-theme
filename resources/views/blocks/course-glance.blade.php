{{--
  Title: Course at a Glance
  Description: Course at a Glance Block - Content Blocks
  Icon: analytics
  Keywords: Course Glance Block
  Category: course-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $fields = $block['fields'];
  $block = get_field('block');
@endphp

<section class="block course-glance">
  <div class="container-fluid bg-image"
       style="background-image:url({!! ($block['image'])? $block['image']['url']: '' !!});">
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="details">
          @if($block['study_load'])
            <div class="study-load">
              <p>Study load</p>
              <p>{{ $block['study_load'] }}</p>
            </div>
          @endif

          @if($block['duration'])
            <div class="duration">
              <p>Duration</p>
              <p>{{ $block['duration'] }}</p>
            </div>
          @endif

          @if($block['intakes'])
            <div class="intakes">
              <p>Intakes</p>
              <p>{{ $block['intakes'] }}</p>
            </div>
          @endif
        </div>
        <div class="intro">
          @if($block['intro'])
            <h4>{{ $block['intro'] }}</h4>
          @endif
        </div>
      </div>
      <div class="col-md-3">
        @include('components.course-buttons')
      </div>
    </div>
  </div>
</section>

@if($fields['inpage']['apply_button'] && $fields['inpage']['apply_button'] === 'on')
  @include('forms.modal', ['title' => 'applyModal', 'form' => 'apply'])
@endif
@if($fields['inpage']['brochure_button'] && $fields['inpage']['brochure_button'] === 'on')
  @include('forms.modal', ['title' => 'brochureModal', 'form' => 'brochure'])
@endif
