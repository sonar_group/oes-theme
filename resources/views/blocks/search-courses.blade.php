{{--
  Title: Search Courses
  Description: Search courses by discipline or title
  Icon: search
  Keywords: Search Course Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block search-courses">
  @if (!empty($block['disciplines']))
    <div class="container">
      <div class="row">
        @if($block['fields']['heading'])
          <div class="col-12">
            <h1>{{$block['fields']['heading']}}</h1>
          </div>
        @endif
        <div class="col-12">
          <div class="search-courses__disciplines">
            <input type="text" id="discipline" name="discipline" placeholder="Search"
                   data-disciplines="{{$block['disciplines_name'] . '|' . $block['courses_name']}}">
          </div>
        </div>
      </div>
    </div>
  @endif
  <div class="search-courses__results">
    <div class="container">
      <div class="row search-courses__courses">
        @if (!empty($block['courses']))
          @foreach ($block['courses'] as $item)
            <div class="col-md-4">
              <div class="content search-courses__course" data-course="{{ $item['title'] }}"
                   data-discipline="{{ $item['term'] }}">
                <a href="{{ $item['permalink'] }}">
                  @if (!empty($item['image']))
                    <img src="{{ $item['image'] }}" loading="lazy" alt="">
                  @endif
                  <div class="h5">{{ $item['title'] }}</div>
                  <div class="small-label">{{ $item['term'] }}</div>
                </a>
              </div>
            </div>
          @endforeach
          @php wp_reset_postdata(); @endphp
        @endif
        <div class="col-12 no-results">
          No Results
        </div>
      </div>
    </div>
  </div>
</section>
