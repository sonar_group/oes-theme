{{--
  Title: Course Tab Content
  Description: Course Tab Content Block - Content Blocks
  Icon: portfolio
  Keywords: Course Tab Content  Block
  Category: course-blocks
  Mode: edit
  SupportsMode: false
--}}
@php $block = get_field('block'); @endphp

<section class="block course-tab-content">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ul id="tabs" class="nav nav-fill" role="tablist">
          @if($block['structure_content'])
            <li class="nav-item">
              <h5 id="tab-structure" href="#structure" class="nav-link active" data-toggle="tab" role="tab">
                Structure
              </h5>
            </li>
          @endif
          @if($block['entry_content'])
            <li class="nav-item">
              <h5 id="tab-entry" href="#entry" class="nav-link" data-toggle="tab" role="tab">
                Entry Requirements
              </h5>
            </li>
          @endif
          @if($block['fees_content'])
            <li class="nav-item">
              <h5 id="tab-fees" href="#fees" class="nav-link" data-toggle="tab" role="tab">
                Fees
              </h5>
            </li>
          @endif
          @if($block['accreditation_content'])
            <li class="nav-item">
              <h5 id="tab-accreditation" href="#accreditation" class="nav-link" data-toggle="tab" role="tab">
                Accreditation
              </h5>
            </li>
          @endif
        </ul>

        <div id="content" class="tab-content" role="tablist">
          @if($block['structure_content'])
            <div id="structure" class="card parent tab-pane fade show active" role="tabpanel"
                 aria-labelledby="tab-structure">
              <div class="card-header" role="tab" id="heading-structure" data-toggle="collapse"
                   href="#collapse-structure" aria-expanded="true"
                   aria-controls="collapse-structure">
                <h5>Structure</h5>
              </div>
              <div class="collapse show" id="collapse-structure" data-parent="#content"
                   role="tabpanel" aria-labelledby="heading-structure">
                <div class="card-body">
                  {!! $block['structure_content'] !!}
                  @if($block['units_group'])
                    @include('components.units-group', ['units_group' => $block['units_group']])
                  @endif
                </div>
              </div>
            </div>
          @endif

          @if($block['entry_content'])
            <div id="entry" class="card parent tab-pane fade" role="tabpanel"
                 aria-labelledby="tab-entry">
              <div class="card-header collapsed" role="tab" id="heading-entry" data-toggle="collapse"
                   href="#collapse-entry"
                   aria-expanded="false" aria-controls="collapse-entry">
                <h5>Entry Requirements</h5>
              </div>
              <div class="collapse" id="collapse-entry" data-parent="#content" role="tabpanel"
                   aria-labelledby="heading-entry">
                <div class="card-body">
                  {!! $block['entry_content'] !!}
                </div>
              </div>
            </div>
          @endif

          @if($block['fees_content'])
            <div id="fees" class="card parent tab-pane fade" role="tabpanel" aria-labelledby="tab-fees">
              <div class="card-header collapsed" role="tab" id="heading-fees" data-toggle="collapse"
                   href="#collapse-fees"
                   aria-expanded="false" aria-controls="collapse-fees">
                <h5>Fees</h5>
              </div>
              <div class="collapse" id="collapse-fees" role="tabpanel" data-parent="#content"
                   aria-labelledby="heading-fees">
                <div class="card-body">
                  {!! $block['fees_content'] !!}
                </div>
              </div>
            </div>
          @endif

          @if($block['accreditation_content'])
            <div id="accreditation" class="card parent tab-pane fade" role="tabpanel"
                 aria-labelledby="tab-accreditation">
              <div class="card-header collapsed" role="tab" id="heading-accreditation" data-toggle="collapse"
                   href="#collapse-accreditation"
                   aria-expanded="false" aria-controls="collapse-accreditation">
                <h5>Accreditation</h5>
              </div>
              <div class="collapse" id="collapse-accreditation" role="tabpanel" data-parent="#content"
                   aria-labelledby="heading-accreditation">
                <div class="card-body">
                  {!! $block['accreditation_content'] !!}
                </div>
              </div>
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
