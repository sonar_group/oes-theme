{{--
  Title: Brochure Form
  Description: Brochure Form Block - Content Blocks
  Icon: cover-image
  Keywords: Brochure Form Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $fields = $block['fields'];
@endphp

@if($fields['inpage']['brochure_button'] && $fields['inpage']['brochure_button'] === 'on')
  <section class="block brochure-form">
    <div class="container">
      <div class="row">
        <div class="col-md-6">

          @if($fields['inpage']['register_interest'] && $fields['inpage']['register_interest'] === 'on')
            @if(isset($fields['forms']['register_form_content']))
              {!! $fields['forms']['register_form_content'] !!}
            @endif
          @else
            @if(isset($fields['forms']['brochure_form_content']))
              {!! $fields['forms']['brochure_form_content'] !!}
            @endif
          @endif

        </div>
        <div class="col-md-6">
          @include('forms.brochure')
        </div>
      </div>
    </div>
    </div>
  </section>
@endif
