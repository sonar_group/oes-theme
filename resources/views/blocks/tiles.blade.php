{{--
  Title: Tiles
  Description: Tiles Block - Content Blocks
  Icon: excerpt-view
  Keywords: Tiles Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block tiles">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($block['heading'])
          <h2>{{$block['heading']}}</h2>
        @endif
      </div>

      @if($block['columns'])
        @foreach($block['columns'] as $tile)
          @include('components.tiles', [
              'blockClass' => 'tile',
              'backgroundImage' => $tile['image'],
              'backgroundColour' => $tile['background_colour'],
              'tile' => $block['columns'],
              'heading' => $tile['heading'],
              'copy' => $tile['copy'],
              'link' => $tile['link'],
              'btnClass' => 'text-link',
          ])
        @endforeach
      @endif

    </div>
  </div>
</section>
