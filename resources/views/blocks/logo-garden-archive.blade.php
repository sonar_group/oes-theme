{{--
  Title: Logo Garden Archive
  Description: Logo Garden Archive Block - Content Blocks
  Icon: excerpt-view
  Keywords: Logo Garden Archive Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
  $r = rand(1,50);
@endphp


<section class="block logo-garden-archive">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if(isset($block['heading']))
          <h2>{{ $block['heading'] }}</h2>
        @endif

        @if(isset($block['content']))
          {!! $block['content'] !!}
        @endif

      </div>
    </div>
    <div class="row">

        @if($block['logo_garden'])
          @foreach($block['logo_garden'] as $block_content)
            @php $blocks = App::parseBlock($block_content, 'single-logo-garden'); @endphp

            @if($blocks)
              @foreach($blocks as $logo_garden)
                @php $r++ @endphp

                @php($title = str_replace(' ', '', $logo_garden['block_name']))

                <div class="column col-md-4">
                  <div class="content">
                    @if(isset($logo_garden['block_image']))
                      <div class="bg-image" style="background-image: url({{ wp_get_attachment_url($logo_garden['block_image']) }});"></div>
                    @endif
                    @if(isset($logo_garden['block_name']))
                      <h4>{{ $logo_garden['block_name'] }}</h4>
                    @endif
                    @if(isset($logo_garden['block_short_bio']))
                      {!! $logo_garden['block_short_bio'] !!}
                    @endif
                      <button data-toggle="modal"
                              data-target="#logoGardenModal_{{$title}}">Read Bio</button>
                  </div>
                </div>

                @include('components.logo-garden-modal', ['logo_garden' => $logo_garden])

              @endforeach
            @endif

          @endforeach
        @endif

      </div>
    </div>
  </div>
</section>

