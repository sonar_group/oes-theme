{{--
  Title: Header
  Description: Header Block - Content Blocks
  Icon: money
  Keywords: Header Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section
  class="block header" {!! (isset($block['background_colour']))? 'style="background-color:' . $block['background_colour'] .'"': '' !!}>
  @if($block['image'])
    <div class="bg-image"
         style="background-image:url({!! ($block['image'])? $block['image']['url']: '' !!});"></div>
  @endif
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($block['heading'])
          <h2>{{ $block['heading'] }}</h2>
        @endif
        @if($block['sub_heading'])
          <p>{{ $block['sub_heading'] }}</p>
        @endif
        @if($block['copy'])
          <p>{{ $block['copy'] }}</p>
        @endif
        @if(isset($block['link']['url']))
          <a class="btn" href="{{ $block['link']['url'] }}" target="{{ $block['link']['target'] }}">
            {{ $block['link']['title'] }}
          </a>
        @endif
      </div>
    </div>
  </div>
</section>
