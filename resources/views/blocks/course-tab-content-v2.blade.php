{{--
  Title: Course Tab Content V2
  Description: Course Tab Content V2 Block - Content Blocks
  Icon: portfolio
  Keywords: Course Tab Content V2  Block
  Category: course-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
  if ($block['layout'] === 'left') {
      $tabLayout = 'left';
      $open = 'col-md-2';
      $close = '</div><div class="col-md-10">';
  }
  if ($block['layout'] === 'top') {
      $tabLayout = 'top';
      $open = 'col-md-12';
      $close = '';
  }
@endphp

<section class="block course-tab-content">
  <div class="container">
    <div class="row">
      <div class="{{$open}}">
        <ul id="tabs" class="nav nav-fill" role="tablist">
          @foreach($block['tab_group'] as $i => $tab)
            @php($title = str_replace(' ', '', $tab['title']))
            <li class="nav-item">
              <h5 id="tab-structure_{{$title}}" href="#{{$title}}" class="nav-link {{ $i == 0 ? 'active' : '' }}" data-toggle="tab" role="tab">
                {{$tab['title']}}
              </h5>
            </li>
          @endforeach
        </ul>

        {!! $close !!}

        @php(reset_rows())

        <div id="content" class="tab-content" role="tablist">
          @foreach($block['tab_group'] as $i => $tab)
            @php($title = str_replace(' ', '', $tab['title']))
            <div id="{{$title}}" class="card parent tab-pane fade {{ $i === 0 ? 'show active' : '' }}" role="tabpanel"
                 aria-labelledby="tab-{{$title}}">
              <div class="card-header" role="tab" id="heading-{{$title}}" data-toggle="collapse"
                   href="#collapse-{{$title}}" aria-expanded="true"
                   aria-controls="collapse-{{$title}}">
                <h5>{{$tab['title']}}</h5>
              </div>
              <div class="collapse show" id="collapse-{{$title}}" data-parent="#content"
                   role="tabpanel" aria-labelledby="heading-{{$title}}">
                <div class="card-body">
                  {!! $tab['content'] !!}
                  @if($tab['content_type'] === 'accordion')
                    @include('components.accordion', ['parent' => $title, 'accordion' => $tab['accordion']])
                  @endif
                  @if($tab['content_type'] === 'units')
                    @include('components.units-group-v2', ['content' => $tab['units']])
                  @endif
                  @if($tab['content_type'] === 'academic_team')
                    @foreach($tab['academic_team'] as $content)
                      @include('components.academic-team', ['content' => $content])
                    @endforeach
                  @endif
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
