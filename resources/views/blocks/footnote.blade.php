{{--
  Title: Footnote
  Description: Footnote Block - Content Blocks
  Icon: cover-image
  Keywords: Footnote Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block footnote">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($block['content'])
          {!! $block['content'] !!}
        @endif
      </div>
    </div>
  </div>
</section>
