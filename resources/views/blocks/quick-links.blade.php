{{--
  Title: Quick Links
  Description: Quick Links - Content Blocks
  Icon: excerpt-view
  Keywords: Quick Links Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block quick-links">
  <div class="container">
    <div class="row">

      @if($block['quick_link'])
        @foreach($block['quick_link'] as $content)

          @if(isset($content['link']['url']))
            <a class="column {!! ($block['layout'])? $block['layout']: 'col-12' !!}"
               href="{!! $content['link']['url'] !!}"
               target="{!! $content['link']['target'] !!}">
              @else
                <div class="column {!! ($block['layout'])? $block['layout']: 'col-12' !!}">
                  @endif

                  @if(isset($content['tag']))
                    <div class="column__tag">{{$content['tag']}}</div>
                  @endif

                  <div class="column__content">
                    @if(isset($content['image']['url']))
                      <div class="bg-image" style="background-image: url({!! $content['image']['url'] !!});"></div>
                    @endif
                    @if(isset($content['content']))
                      {!! $content['content'] !!}
                    @endif

                  </div>

                  @if(isset($content['footnote']))
                    <div class="column__footnote">{{$content['footnote']}}</div>
              @endif

              @if(isset($content['link']['url']))
            </a>
          @else
    </div>
    @endif

    @endforeach
    @endif

  </div>
  </div>
</section>
