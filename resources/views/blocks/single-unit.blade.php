{{--
  Title: Single Unit
  Description: Single Unit Block - Content Blocks
  Icon: excerpt-view
  Keywords: Single Unit Block
  Category: unit-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block single-unit">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        @if($block['name'])
          <h2>{{ $block['name'] }}</h2>
        @endif
          @if($block['headline'])
            <h3>{{ $block['headline'] }}</h3>
          @endif
      </div>
      <div class="col-sm-6">
        @if($block['code'])
          <p>Unit Code:
            <span>{{ $block['code'] }}</span>
          </p>
        @endif
          @if($block['price'])
            <p>Price:
              <span>{{ $block['price'] }}</span>
            </p>
          @endif
          @if($block['duration'])
            <p>Duration:
              <span>{{ $block['duration'] }}</span>
            </p>
          @endif
        @if($block['contact_hours'])
          <p>Contact Hours:
            <span>{{ $block['contact_hours'] }}</span>
          </p>
        @endif
          @if($block['credit_points'])
            <p>Credit Points:
              <span>{{ $block['credit_points'] }}</span>
            </p>
          @endif
        @if($block['description'])
          <p>Description:</p>
          {!! $block['description'] !!}
        @endif
      </div>
    </div>
  </div>
</section>
