{{--
  Title: Interactive Search Course
  Description: Show courses by selecting a discipline, results can be filtered by level of study
  Icon: code-standards
  Keywords: Interactive Search Course Block, Course Wizard
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block interactive-search">
  <div class="interactive-search__heading">
    <div class="container">
      <div class="row">
        <div class="col-12">
          @if($block['fields']['heading'])
            <h3>{{$block['fields']['heading']}}</h3>
          @endif
          <div class="interactive-search__select" id="scrollToButton">
            <h3 class="interactive-search__select__current">Please select a course</h3>
            @if (!empty($block['disciplines']))
              <ul class="interactive-search__select__list">
                @foreach ($block['disciplines'] as $item)
                  <li class="option bullet" data-id="{{ $item->term_id}}" data-name="{{ $item->name }}">
                    {{ $item->name }}
                  </li>
                @endforeach
              </ul>
            @endif
          </div>
          @if(!empty($block['fields']['link']['url']))
            <a class="button-large" href="{{ $block['fields']['link']['url'] }}"
               target="{{ $block['fields']['link']['target'] }}">
              {{ $block['fields']['link']['title'] }}
            </a>
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="container interactive-search__results">
    <div class="row">
      <div class="col-12">
        <h5>Available courses</h5>
      </div>
      <div class="col-12">
        @if(!empty($block['levels_of_study']))
          @foreach ($block['levels_of_study'] as $item)
            <div class="interactive-search__levels-of-study-option" data-id="{{ $item->term_id}}"
                 data-name="{{ $item->name }}">
              {{ $item->name }}
            </div>
          @endforeach
        @endif
      </div>
    </div>
    <div class="row interactive-search__courses"></div>
  </div>
</section>
