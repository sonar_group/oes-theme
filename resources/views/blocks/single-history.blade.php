{{--
  Title: Single History
  Description: Single History Block - Content Blocks
  Icon: excerpt-view
  Keywords: Single History Block
  Category: history-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block single-history">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($block['date'])
          <h2>{{ $block['date'] }}</h2>
        @endif
      </div>
      <div class="col-12">
        @if($block['intro_copy'])
          <h4>{!! $block['intro_copy'] !!}</h4>
        @endif
        @if($block['image'])
          <img src="{!! ($block['image'])? $block['image']['url']: '' !!}" class="img-fluid icon">
        @endif
        @if($block['copy'])
          {!! $block['copy'] !!}
        @endif
      </div>
    </div>
  </div>
</section>
