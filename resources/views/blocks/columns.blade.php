{{--
  Title: Columns
  Description: Columns Block - Content Blocks
  Icon: excerpt-view
  Keywords: Columns Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');

  if (count($block['columns']) === 1) {
      $columnClass = 'col-12';
  } elseif (count($block['columns']) === 2) {
      $columnClass = 'col-md-6';
  } else {
      switch (count($block['columns']) % 4) {
          case 1:
          case 2:
          case 3:
              $columnClass = 'col-md-4';
              break;
          case 0:
              $columnClass = 'col-md-4';
              break;
          default:
              $columnClass = 'col-12';
              break;
      }
  }
@endphp

<section class="block columns">
  <div class="container">
    <div class="row">
      <div class="col-12 heading {{$block['heading_align']}}">
        @if($block['heading'])
          <h2>{{$block['heading']}}</h2>
        @endif
          @if($block['copy'])
            <p>{{$block['copy']}}</p>
          @endif
      </div>

      @if($block['columns'])
        @foreach($block['columns'] as $column)
          <div class="column {{$columnClass}} {{$block['text_align']}}">
            <div class="content">
              @if(!empty($column['image']['url']))
                <div class="bg-image" style="background-image: url({!! $column['image']['url'] !!});"></div>
              @endif
                @if(!empty($column['icon']['url']))
                  <div class="icon"><img src="{!! $column['icon']['url'] !!}" class="img-fluid"></div>
                @endif
              @if(isset($column['heading']))
                <h4>{{ $column['heading'] }}</h4>
              @endif
              @if(isset($column['copy']))
                {!! $column['copy'] !!}
              @endif
              @if(isset($column['link']['url']))
                <div class="link mt-auto {{$column['link_align']}}">
                  <a class="{{$column['link_type']}}" href="{{ $column['link']['url'] }}" target="{{ $column['link']['target'] }}">
                    {{ $column['link']['title'] }}
                  </a>
                </div>
              @endif
            </div>
          </div>
        @endforeach
      @endif

    </div>
  </div>
</section>
