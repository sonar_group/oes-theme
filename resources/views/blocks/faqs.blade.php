{{--
  Title: FAQs
  Description: FAQs Block - Content Blocks
  Icon: excerpt-view
  Keywords: FAQs Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
  $r = rand(1,50);
@endphp


<section class="block faqs">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6">
        @if(isset($block['heading']))
          <h2>{{ $block['heading'] }}</h2>
        @endif
      </div>
      <div class="col-12 col-md-6">
        @if($block['link'])
          <a class="btn" href="{{ $block['link']['url'] }}" target="{{ $block['link']['target'] }}">
            {{ $block['link']['title'] }}
          </a>
        @endif
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        @if($block['faqs'])
          @include('components.faq', ['faqs' => $block['faqs'], 'parent' => $r])
        @endif
      </div>
    </div>
  </div>
</section>

