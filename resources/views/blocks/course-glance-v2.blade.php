{{--
  Title: Course at a Glance V2
  Description: Course at a Glance V2 Block - Content Blocks
  Icon: analytics
  Keywords: Course Glance V2 Block
  Category: course-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $fields = $block['fields'];
  $block = get_field('block');
@endphp

<section class="block course-glance-v2"
         style="background-image: url({!! (!empty($fields['course_options']['course_page_header']['url']))? $fields['course_options']['course_page_header']['url']: '' !!});">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if(!empty($fields['course_options']['subtitle']))
          <h6>{{ $fields['course_options']['subtitle'] }}</h6>
        @endif
      </div>
      <div class="col-md-6">
        @if($block['course_title'])
          <h1>{{ $block['course_title'] }}</h1>
        @endif
        @if($block['course_major'])
          <h2>{{ $block['course_major'] }}</h2>
        @endif
      </div>
      <div class="col-md-6">
        @if($block['intro'])
          <p>{{ $block['intro'] }}</p>
        @endif
        @include('components.course-buttons')
      </div>
    </div>
    <div class="row">
      <div class="col-12 details">
        @if($block['details'])
          @foreach($block['details'] as $detail)
            <div class="details__detail">
              @if(!empty($detail['icon']['url']))
                <div class="icon"><img src="{!! $detail['icon']['url'] !!}" class="img-fluid"></div>
              @endif
              @if($detail['copy'])
                <p>{{ $detail['copy'] }}</p>
              @endif
              @if($detail['tooltip'] && $detail['tooltip'] === 'yes')
                <p data-toggle="popover" data-html="true" data-placement="right"
                   data-content="{{ $detail['tooltip_copy'] }}">
                  @if($detail['title'])
                    <span>{{ $detail['title'] }}</span>
                  @endif
                  <span class="icon">?</span>
                </p>
              @else
                @if($detail['title'])
                  <p>{{ $detail['title'] }}</p>
                @endif
              @endif
            </div>
          @endforeach
        @endif
      </div>
    </div>
  </div>
</section>

{{--@if($fields['inpage']['apply_button'] && $fields['inpage']['apply_button'] === 'on')--}}
{{--  @include('forms.modal', ['title' => 'applyModal', 'form' => 'apply'])--}}
{{--@endif--}}
{{--@if($fields['inpage']['brochure_button'] && $fields['inpage']['brochure_button'] === 'on')--}}
{{--  @include('forms.modal', ['title' => 'brochureModal', 'form' => 'brochure'])--}}
{{--@endif--}}
