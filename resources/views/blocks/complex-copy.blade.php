{{--
  Title: Complex Copy
  Description: Complex Copy - Content Blocks
  Icon: text-page
  Keywords: Complex Copy Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');

  if ($block['layout'] === 'left') {
      $columnClass = 'col-md-7';
  } elseif ($block['layout'] === 'full') {
      $columnClass = 'col-12';
  } elseif ($block['layout'] === 'right') {
      $columnClass = 'col-md-6 offset-md-6';
  } elseif ($block['layout'] === 'centred') {
      $columnClass = 'col-md-10 mx-auto';
  }
@endphp

<section class="block complex-copy">
  <div class="container">
    <div class="row">

      @if($block['layout'] === 'left' || $block['layout'] === 'full' || $block['layout'] === 'right' || $block['layout'] === 'centred')
        <div class="{{$columnClass}}">
          <h2>{{ $block['heading'] }}</h2>
          {!! $block['content'] !!}
        </div>
      @endif

      @if($block['layout'] === 'mixed')
        <div class="col-md-5 mixed">
          <h2>{{ $block['heading'] }}</h2>
        </div>
        <div class="col-md-7">
          {!! $block['content'] !!}
        </div>
      @endif

    </div>
  </div>
</section>
