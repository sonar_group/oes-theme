{{--
  Title: Video
  Description: Video Block - Content Blocks
  Icon: format-video
  Keywords: Video Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
  $r = rand(1,50);
@endphp

<section class="block video">
  <div class="container {{$block['text_alignment']}}">
    <div class="row">
        <div class="{!! ($block['text_alignment'] === 'copy-left' || $block['text_alignment'] === 'copy-right')? 'col' : 'col-12' !!} {!! ($block['text_alignment'] === 'copy-right')? 'order-2' : '' !!}">
          @if($block['heading'])
            <h2>{{$block['heading']}}</h2>
          @endif
          @if($block['copy'])
            {!! $block['copy'] !!}
          @endif
        </div>
        <div class="{{$block['width']}}">
          <div class="bg-video"
               style="background-image:url({!! ($block['thumbnail'])? $block['thumbnail']['url']: '' !!});"
               data-toggle="modal" data-target="#videoModal_{{$r}}">
            <span class="icon">&times;</span>
          </div>
        </div>
    </div>
  </div>
  @include('components.video-modal', ['video' => $block['video_url'], 'r' => $r, 'modalName' => ''])
</section>
