{{--
  Title: Banner
  Description: Banner Block - Content Blocks
  Icon: cover-image
  Keywords: Banner Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php $block = get_field('block'); @endphp

<section class="block banner">
  <div class="bg-image" style="background-image:url({!! ($block['image'])? $block['image']['url']: '' !!});"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <h1>{{ $block['heading'] }}</h1>
        <h3>{{ $block['sub_heading'] }}</h3>
        @if(isset($block['link']['url']))
          <a class="btn" href="{{ $block['link']['url'] }}" target="{{ $block['link']['target'] }}">
            {{ $block['link']['title'] }}
          </a>
        @endif
      </div>
      @if($block['more_content'] === 'on')
        <div class="col-sm-8">
          @if($block['copy'])
            {!! $block['copy'] !!}
          @endif
          @if(isset($block['2nd_link']['url']))
            <a class="btn" href="{{ $block['2nd_link']['url'] }}" target="{{ $block['2nd_link']['target'] }}">
              {{ $block['2nd_link']['title'] }}
            </a>
          @endif
        </div>
      @endif
    </div>
  </div>
</section>
