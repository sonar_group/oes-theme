{{--
  Title: Step Content
  Description: Step Content - Content Blocks
  Icon: text-page
  Keywords: Step Content Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
  $r = rand(51,99);
@endphp

<section class="block step-content">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        @if($block['heading'])
          <h2>{{$block['heading']}}</h2>
        @endif
      </div>
      <div class="col-md-9">
        @if($block['content'])
          {!! $block['content'] !!}
        @endif

        @if($block['faqs'])
          @include('components.faq', ['faqs' => $block['faqs'], 'parent' => $r])
        @endif

      </div>
    </div>
  </div>
</section>
