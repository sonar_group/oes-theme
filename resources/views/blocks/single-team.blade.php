{{--
  Title: Single Team
  Description: Single Team Block - Content Blocks
  Icon: excerpt-view
  Keywords: Single Team Block
  Category: team-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block single-team">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($block['name'])
          <h2>{{ $block['name'] }}</h2>
        @endif
          @if($block['role'])
            <h3>{{ $block['role'] }}</h3>
          @endif
      </div>
      <div class="col-12">
        @if($block['long_bio'])
          {!! $block['long_bio'] !!}
        @endif
        @if($block['link'])
          <a class="btn" href="{{ $block['link']['url'] }}" target="{{ $block['link']['target'] }}">
            {{ $block['link']['title'] }}
          </a>
        @endif
      </div>
    </div>
  </div>
</section>
