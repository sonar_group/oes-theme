{{--
  Title: Single Job
  Description: Single Job Block - Content Blocks
  Icon: excerpt-view
  Keywords: Single Job Block
  Category: job-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block single-job">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($block['title'])
          <h2>{{ $block['title'] }}</h2>
        @endif
      </div>
      <div class="col-12">
        @if($block['long_description'])
          {!! $block['long_description'] !!}
        @endif
      </div>
      <div class="col-12">
        <div class="row">
          <div class="col-12 col-md-6">
            @if($block['link'])
              <a href="{{ $block['link']['url'] }}" target="{{ $block['link']['target'] }}">{!! $block['link']['title'] !!}</a>
            @endif
          </div>
          <div class="col-12 col-md-6">
            @if($block['closing_date'])
              {!! $block['closing_date'] !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
