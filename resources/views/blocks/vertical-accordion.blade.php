{{--
  Title: Vertical Accordion
  Description: Vertical Accordion Block - Content Blocks
  Icon: index-card
  Keywords: Vertical Accordion Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
  $r = rand(1,50);
@endphp

<section class="block vertical-accordion">
  <div class="accordion width v-accordion" id="verticalAccordion">
    <div class="card active v-card">
      <div class="card-header v-card-header" data-toggle="collapse" data-target="#collapseCourses">
        More Courses
      </div>

      <div id="collapseCourses" class="collapse show width" data-parent="#verticalAccordion">
        <div class="card-body v-card-body">
          @if($block['courses_heading'])
          <p>{{$block['courses_heading']}}</p>
          @endif
          @foreach($block['courses'] as $item)
            <a href="{!! get_permalink($item) !!}" class="col-12">
              <div>{{get_the_title($item)}}</div>
            </a>
          @endforeach
          @php(wp_reset_postdata())
        </div>
      </div>
    </div>
    <div class="card v-card">
      <div class="card-header v-card-header" data-toggle="collapse" data-target="#collapseFAQs">
        FAQs
      </div>
      <div id="collapseFAQs" class="collapse width" data-parent="#verticalAccordion">
        <div class="card-body v-card-body">
          @if($block['faqs_heading'])
          <p>{{$block['faqs_heading']}}</p>
          @endif
          @if($block['faqs'])
            @include('components.faq', ['faqs' => $block['faqs'], 'parent' => $r])
          @endif
        </div>
      </div>
    </div>
    <div class="card v-card">
      <div class="card-header v-card-header" data-toggle="collapse" data-target="#collapseArticles">
        Articles
      </div>
      <div id="collapseArticles" class="collapse width" data-parent="#verticalAccordion">
        <div class="card-body v-card-body">
          @if($block['articles_heading'])
          <p>{{$block['articles_heading']}}</p>
          @endif
          @foreach($block['articles'] as $item)
            <a href="{!! get_permalink($item) !!}" class="col-12">
              <div>{{get_the_title($item)}}</div>
            </a>
          @endforeach
          @php(wp_reset_postdata())
        </div>
      </div>
    </div>
  </div>
</section>
