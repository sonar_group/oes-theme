{{--
  Title: History Archive
  Description: History Archive Block - Content Blocks
  Icon: excerpt-view
  Keywords: History Archive Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp


<section class="block history-archive">
  <div class="container-fluid">
    @if ($block['full_width_controls'])
      <div class="row">
        <div class="col-12">
          <div class="controls">
            <span class="icon slick-prev">PREV</span>
            <span class="icon slick-next">NEXT</span>
          </div>
        </div>
      </div>
    @endif
    <div class="row">
      <div class="col-12 col-md-6">
        @if (!$block['full_width_controls'])
          <div class="row">
            <div class="col-12">
              <div class="controls">
                <span class="icon slick-prev">PREV</span>
                <span class="icon slick-next">NEXT</span>
              </div>
            </div>
          </div>
        @endif
        @if(!empty($block['history_slider']))
          <div class="history-archive__slider">
            @foreach ($block['history_slider'] as $slider)
              <div class="history-archive__slide">
                @foreach($slider['histories'] as $block_content)
                  @php $blocks = App::parseBlock($block_content, 'single-history'); @endphp

                  @if($blocks)
                    @foreach($blocks as $history)
                      <div class="history-archive__history">
                        <div class="content">
                          @if(isset($history['block_date']))
                            <h4>{{ $history['block_date'] }}</h4>
                          @endif
                          @if(isset($history['block_copy']))
                            <div>{!! $history['block_copy'] !!}</div>
                          @endif
                        </div>
                      </div>

                    @endforeach
                  @endif
                @endforeach
              </div>
            @endforeach
          </div>
        @endif
      </div>
      <div class="col-12 col-md-6">
        @if (!empty($block['image']))
          <img src="{{ wp_get_attachment_url($block['image']) }}" loading="lazy" alt="">
        @endif
      </div>
    </div>
  </div>
</section>

