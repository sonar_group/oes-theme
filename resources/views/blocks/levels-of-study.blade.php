{{--
  Title: Levels of Study
  Description: Show all levels of study
  Icon: clipboard
  Keywords: Levels of Study Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block levels-of-study">
  <div class="container levels-of-study__heading">
    <div class="row">
      <div class="col-12 col-md-6">
        <h2>{!! $block['fields']['heading'] !!}</h2>
      </div>
      <div class="col-12 col-md-6">
        <a href="#">View all levels of study</a>
      </div>
    </div>
  </div>
  <div class="container levels-of-study__container">
    <div class="row">
      @if (!empty($block['level_of_study']))
        @foreach ($block['level_of_study'] as $item)
          <div class="col-12 col-md-4 levels-of-study__single">
            <div class="levels-of-study__single__details">
              <h5>{{ $item->name }}</h5>
            </div>
          </div>
        @endforeach
        @php wp_reset_postdata(); @endphp
      @endif
    </div>
  </div>
</section>
