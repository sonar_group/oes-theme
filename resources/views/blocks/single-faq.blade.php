{{--
  Title: Single FAQ
  Description: Single FAQ Block - Content Blocks
  Icon: excerpt-view
  Keywords: Single FAQ Block
  Category: faq-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block single-faq">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($block['question'])
          <h2>{{ $block['question'] }}</h2>
        @endif
      </div>
      <div class="col-12">
        @if($block['answer'])
          {!! $block['answer'] !!}
        @endif
      </div>
    </div>
  </div>
</section>
