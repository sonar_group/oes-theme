{{--
  Title: Image Gallery with Text
  Description: Image Gallery with Text Block - Content Blocks
  Icon: images-alt
  Keywords: Image Gallery Text Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php $block = get_field('block'); @endphp

<section class="block image-gallery-text">
  <div class="container image-gallery-text-content">
    <div class="row">
      <div class="col-md-8">
        <h2>{!! $block['heading'] !!}</h2>
        <div class="body-1">{!! $block['content'] !!}</div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="image-gallery-text-slider">
      @if(isset($block['image_1']['url']))
        <div>
          <div class="image-gallery-text-slide image-1">
            <img class="" src="{{ $block['image_1']['url'] }}" alt="">
          </div>
        </div>
      @endif
      @if(isset($block['image_2']['url']))
        <div>
          <div class="image-gallery-text-slide image-2">
            <img class="" src="{{ $block['image_2']['url'] }}" alt="">
          </div>
        </div>
      @endif
      @if(isset($block['image_3']['url']))
        <div>
          <div class="image-gallery-text-slide image-3">
            <img class="" src="{{ $block['image_3']['url'] }}" alt="">
          </div>
        </div>
      @endif
    </div>
  </div>
</section>
