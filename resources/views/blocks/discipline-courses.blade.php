{{--
  Title: Discipline Courses
  Description: Show all courses based on a select discipline
  Icon: clipboard
  Keywords: Discipline Courses Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block discipline-courses">
  <div class="container discipline-courses__heading">
    <div class="row">
      <div class="col-12 col-md-6">
        <h2>{!! $block['fields']['heading'] !!}</h2>
      </div>
    </div>
  </div>

  @if($block['fields']['show_filters'] === 'on')
  <div class="container discipline-courses__filters">
    <div class="row">
      <div class="col-12">
        @if (!empty($block['levels_of_study']))
          <form action="" method="post" class="discipline-courses__form" id="discipline-courses__form" name="discipline-courses__form">
            <input type="hidden" name="level_of_study" value="{{ isset($block['selected_level_of_study']) ? $block['selected_level_of_study'] : ''}}">
            <input type="hidden" name="discipline" value="{{ isset($block['fields']['discipline']) ? $block['fields']['discipline']->term_id : ''}}">
            <input type="hidden" name="ajax_url" id="ajax_url" value="/wp/wp-admin/admin-ajax.php">
            <a class="discipline-courses__selector active" href="?level_of_study=all" data-id='All' >
              <span>All</span>
            </a>
            @foreach ($block['levels_of_study'] as $item)
              @if($item->name)
                <a class="discipline-courses__selector level-of-study" href="?level_of_study={{ $item->term_id }}" data-id={{ $item->term_id}}>
                  <span>{!! $item->name !!}</span>
                </a>
              @endif
            @endforeach
          </form>
          @php wp_reset_postdata(); @endphp
        @endif
      </div>
    </div>
  </div>
  @endif

  <div class="container">
    <div class="row discipline-courses__courses">
      @if (!empty($block['courses']))
        @php
          $term = $block['fields']['discipline'];
        @endphp
        @foreach ($block['courses'] as $item)
          @if($term->name == $item['term'])
            @include('components.course', ['item' => $item])
          @endif
        @endforeach
        @php wp_reset_postdata(); @endphp
      @endif
    </div>
  </div>
</section>
