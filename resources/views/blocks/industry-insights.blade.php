{{--
  Title: industry insights
  Description: industry insights Block - Content Blocks
  Icon: index-card
  Keywords: industry insights Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php $block = get_field('block') @endphp

<section class="block industry-insights">
  <div class="container">
    <div class="row">
      <div class="col-md-5 content">
        @if($block['heading'])
          <h2>{{ $block['heading'] }}</h2>
        @endif
        @if($block['content'])
          {!! $block['content'] !!}
        @endif
      </div>
      <div class="col-md-7">
        <div class="row">
          <div class="col-12 top">
            <img src="{!! ($block['top_icon'])? $block['top_icon']['url']: '' !!}" class="img-fluid icon">
            <p>
              @if($block['top_highlighted'])
                @if($block['top_highlighted']['url'])
                  <a href="{{ $block['top_highlighted']['url'] }}" target="{{ $block['top_highlighted']['target'] }}">
                    {{ $block['top_highlighted']['title'] }}
                  </a>
                 @endif
                @endif
              @if($block['top_text']){!! $block['top_text'] !!}@endif
            </p>
          </div>
          <div class="col-6 left">
            <img src="{!! ($block['left_icon'])? $block['left_icon']['url'] : '' !!}" class="img-fluid icon">
            <p>
              @if($block['left_highlighted']['url'])
                <a href="{{ $block['left_highlighted']['url'] }}" target="{{ $block['left_highlighted']['target'] }}">
                  {{ $block['left_highlighted']['title'] }}
                </a>
              @endif
              @if($block['left_text']){!! $block['left_text'] !!}@endif
            </p>
          </div>
          <div class="col-6 right">
            <img src="{!! ($block['right_icon'])? $block['right_icon']['url']: '' !!}" class="img-fluid icon">
            <p>
              @if($block['right_highlighted']['url'])
                <a href="{{ $block['right_highlighted']['url'] }}" target="{{ $block['right_highlighted']['target'] }}">
                  {{ $block['right_highlighted']['title'] }}
                </a>
              @endif
              @if($block['right_text']){!! $block['right_text'] !!}@endif
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
