{{--
  Title: Course Archive
  Description: Course Archive Block - Content Blocks
  Icon: cover-image
  Keywords: Course Archive Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block course-archive">
  @if (!empty($block['disciplines']))
    <div class="container">
      <div class="row">
        <form action="" method="post" class="course-archive__form" id="course-archive__form" name="course-archive__form">
          <input type="hidden" name="discipline" value="{{ isset($block['selected_discipline']) ? $block['selected_discipline'] : ''}}">
          <input type="hidden" name="ajax_url" id="ajax_url" value="/wp/wp-admin/admin-ajax.php">
          <a class="course-archive__selector active" href="" data-id='All' >
            <span>All</span>
          </a>
          @foreach ($block['disciplines'] as $item)
            <a class="course-archive__selector discipline" href="{{ $item->slug }}" data-id={{ $item->term_id}}>
              <span>{!! $item->name !!}</span>
            </a>
          @endforeach
        </form>
      </div>
    </div>
  @endif
  <div class="container">
    <div class="row course-archive__courses">
      @if (!empty($block['courses']))
        @foreach ($block['courses'] as $item)
          @include('components.course', ['item' => $item])
        @endforeach
        @php wp_reset_postdata(); @endphp
      @endif
    </div>
  </div>
</section>
