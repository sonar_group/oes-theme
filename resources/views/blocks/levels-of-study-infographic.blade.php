{{--
  Title: Levels of Study Infographic
  Description: Levels of Study Infographic Block - Content Blocks
  Icon: cover-image
  Keywords: Levels of Study Infographic Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

@php
  $block = get_field('block');
@endphp

<section class="block levels-of-study-infographic">
  <div class="container">
    <div class="row infographic">
      @foreach($block['level_of_study'] as $i => $level)
        @php($unit_counter = $level['number_of_units'])
        <div class="col infographic__item {{ $i === 0 ? 'active' : '' }}" data-id="{{$i}}">
          <h6 class="title text-center">{{$level['title']}}</h6>
          <div class="units">
            @for($i = 0; $i < 12; $i++)
              <div class="units__unit{{ $unit_counter > 0 ? ' ' . $level['unit_type'] : '' }}"></div>
              @php($unit_counter--)
            @endfor
          </div>
        </div>
      @endforeach
    </div>
  </div>
</section>

@foreach($block['level_of_study'] as $i => $level)
  <div class="results {{ $i === 0 ? 'show' : '' }}" id="level-of-study-{{$i}}">
    <section>
      <div class="container">
        <div class="row content" data-id="{{$i}}">
          <div class="col-md-4">
            {!! $level['content_column_1'] !!}
          </div>
          <div class="col-md-4">
            {!! $level['content_column_2'] !!}
          </div>
          <div class="col-md-4">
            {!! $level['content_column_3'] !!}
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row courses" data-id="{{$i}}">
          @if (!empty($level['courses']))
            @foreach($level['courses'] as $post)

              @php($term = get_the_terms( $post->ID, 'discipline'))

              <div class="col-12 col-md-4">
                <a href="{{get_permalink($post)}}">
                  @if(wp_get_attachment_image_url(get_post_thumbnail_id($post)))
                    <img src="{{wp_get_attachment_image_url(get_post_thumbnail_id($post))}}" loading="lazy" alt="">
                  @endif
                  <div>{{get_the_title($post)}}</div>
                  @if($term)
                    <div>{!! $term[0]->name !!}</div>
                  @endif
                </a>
              </div>
            @endforeach
          @endif
        </div>
      </div>
    </section>
  </div>
@endforeach
