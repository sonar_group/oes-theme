{{--
  Title: Left Right Content
  Description: Left Right - Content Blocks
  Icon: excerpt-view
  Keywords: Left Right Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
  $r = rand(51,99);

  if($block['layout'] == 'right') {
      $position = 'order-2';
  }
  if($block['layout'] == 'left') {
      $position = '';
  }
@endphp

<section class="block left-right">
  <div class="{!! ($block['container'])? $block['container']: 'container' !!}">
    <div class="row">
      <div class="col-md-6 {{$position}}" {!! ($block['video_url'])? 'data-toggle="modal" data-target="#videoModal_leftright_' . $r . '"': '' !!}">
        <div class="{!! ($block['video_url'])? 'bg-video': 'bg-image' !!}" style="background-image:url({!! ($block['image'])? $block['image']['url']: '' !!});">
          @if($block['video_url'])
            <span class="icon">X</span>
          @endif
        </div>
      </div>
      <div class="col-md-6 content" {!! (isset($block['background_colour']))? 'style="background-color:' . $block['background_colour'] .'"': '' !!}>
        @if($block['heading'])
          <h2>{{ $block['heading'] }}</h2>
        @endif
        @if($block['copy'])
          {!! $block['copy'] !!}
        @endif
        @if($block['link'])
          <a class="btn" href="{{ $block['link']['url'] }}" target="{{ $block['link']['target'] }}">
            {{ $block['link']['title'] }}
          </a>
        @endif
      </div>
      @if($block['video_url'])
        @include('components.video-modal', ['video' => $block['video_url'], 'modalName' => '_leftright'])
      @endif
    </div>
  </div>
</section>
