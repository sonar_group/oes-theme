{{--
  Title: Units Archive
  Description: Units Archive Block - Content Blocks
  Icon: cover-image
  Keywords: Units Archive Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block units-archive" style="margin-top:200px;">
    <div class="container">
      <div class="row">
        <form role="search" method="get" class="search-form" action="{{ home_url('/')}}">
          <p class="caption reset-search mb-2">CLEAR SEARCH</p>
          <div class="search-container">
            <input type="search" class="search-field" placeholder="Search Units" value="{{ get_search_query() }}" name="s">
            <button class="search-btn" type="submit" value="">
              <img src="@asset('images/icon-search.svg')" class="icon search-icon" alt="search">
              <span>SEARCH</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  <div class="container">
    <div class="row units-archive__units">
      @if (!empty($block['units']))

            @php $count = count($block['units']); @endphp
            @foreach ($block['units'] as $i => $item)

              @php $blocks = App::parseBlock($item['content'], 'single-unit'); @endphp
              @if($blocks)

                @foreach($blocks as $unit)
                  @php $i++ @endphp

                  <a class="col-6 col-md-4" data-toggle="modal" data-target="#unitModal_{{$i}}_{{ $unit['block_code'] }}">
                    <h3>{!! $unit['block_name'] !!}</h3>
                    <p>{!! $unit['block_description'] !!}</p>
                    <p><strong>{!! $unit['block_code'] !!}</strong></p>
                  </a>
                  @include('components.units-modal', [
                    'id' => $i,
                    'name' => $unit['block_name'],
                    'code' => $unit['block_code'],
                    'contact_hours' =>  $unit['block_contact_hours'],
                    'description' => $unit['block_description']
                  ])

                @endforeach
              @endif

            @endforeach
            @php wp_reset_postdata(); @endphp

      @endif
    </div>
  </div>
</section>
