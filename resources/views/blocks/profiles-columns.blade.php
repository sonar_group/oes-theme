{{--
  Title: Profiles Columns
  Description: Profiles Columns Block - Content Blocks
  Icon: id-alt
  Keywords: Profiles Columns Block
  Category: course-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block profiles-columns">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($block['heading'])
          <h2>{{$block['heading']}}</h2>
        @endif
      </div>

      @if($block['columns'])
        @foreach($block['columns'] as $tile)
          @include('components.tiles', [
              'blockClass' => 'profile-columns',
              'image' => $tile['image'],
              'heading' => $tile['heading'],
              'copy' => $tile['copy'],
              'role' => $tile['role']
          ])
        @endforeach
      @endif

    </div>
  </div>
</section>
