{{--
  Title: Inline Brochure Form
  Description: Inline Brochure Form Block - Content Blocks
  Icon: cover-image
  Keywords: Brochure Form Block
  Category: course-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $fields = $block['fields'];
  $block = get_field('block');
@endphp

@if($fields['inpage']['brochure_button'] && $fields['inpage']['brochure_button'] === 'on')
  <section class="block inline-brochure-form">
    <div class="container">
      <div class="row">
        <div class="col-12">
          @include('forms.brochure')
        </div>
      </div>
    </div>
    </div>
  </section>
@endif
