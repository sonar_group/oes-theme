{{--
  Title: Testimonial
  Description: Testimonial Block - Content Blocks
  Icon: id
  Keywords: Testimonial Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block testimonial">
  <div class="container">
    <div class="row">
      <div class="col-md-5 image"><img src="{!! ($block['image'])? $block['image']['url']: '' !!}" class="img-fluid"></div>
      <div class="col-md-7 content">
        @if($block['testimonial'])
          <p>{{$block['testimonial']}}</p>
        @endif
        @if($block['name'])
          <p>{{$block['name']}}</p>
        @endif
        @if($block['course'])
          <p>{{$block['course']}}</p>
        @endif
      </div>
    </div>
  </div>
</section>
