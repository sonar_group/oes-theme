{{--
  Title: Sticky Sub Nav
  Description: Sticky Sub Nav Block - Content Blocks
  Icon: cover-image
  Keywords: Sticky Sub Nav Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block sticky-sub-nav">
  <div class="container">
    <div class="row">
      <div class="col-12 sticky-sub-nav__container">
        @foreach($block['menu_item'] as $item)
          <a class="sticky-sub-nav__item" href="#{{$item['anchor']}}">{{$item['label']}}</a>
        @endforeach
      </div>
    </div>
  </div>
</section>
