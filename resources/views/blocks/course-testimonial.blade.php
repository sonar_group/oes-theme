{{--
  Title: Course Testimonial
  Description: Course Testimonial Block - Content Blocks
  Icon: id
  Keywords: Course Testimonial Block
  Category: course-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block course-testimonial">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <img src="{!! ($block['image'])? $block['image']['url']: '' !!}" class="img-fluid" alt="">
        @if($block['title'])
          <p>{{$block['title']}}</p>
        @endif
        @if($block['testimonial'])
          <p>{{$block['testimonial']}}</p>
        @endif
        @if($block['subtitle'])
          <p>{{$block['subtitle']}}</p>
        @endif
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-12">
            @if($block['salary_title'])
              <h3>{{$block['salary_title']}}</h3>
            @endif
              @if($block['salary_subtitle'])
                <p>{{$block['salary_subtitle']}}</p>
              @endif
          </div>
          <div class="col-12">
            <img src="{!! ($block['motivation_image'])? $block['motivation_image']['url']: '' !!}" class="img-fluid" alt="">
            @if($block['motivation_copy'])
              <p>{{$block['motivation_copy']}}</p>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
