{{--
  Title: Related News
  Description: Related News Block - Content Blocks
  Icon: excerpt-view
  Keywords: Related News Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block related-news">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6">
        @if(isset($block['heading']))
          <h2>{{ $block['heading'] }}</h2>
        @endif
      </div>
      <div class="col-12 col-md-6">
        @if($block['link'])
          <a class="btn" href="{{ $block['link']['url'] }}" target="{{ $block['link']['target'] }}">
            {{ $block['link']['title'] }}
          </a>
        @endif
      </div>
    </div>
    <div class="row">
      @foreach($block['news'] as $post)
        <a href="{!! get_permalink($post) !!}" class="col-12 col-md-4 column">
          <div class="content">
            <div class="bg-image" style="background-image: url({!! get_the_post_thumbnail_url($post) !!});"></div>
            <h4>{{get_the_title($post)}}</h4>
            <p>{{get_the_excerpt($post)}}</p>
            <p>{{get_field('read_time', $post)}}</p>
          </div>
        </a>
      @endforeach
      @php(wp_reset_postdata())
    </div>
  </div>
</section>
