{{--
  Title: Course Archive Multiple
  Description: Show all courses and filter by discipline, level of study and search
  Icon: cover-image
  Keywords: Course Archive Multiple, Online Courses
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block course-archive-multiple">
  @if (!empty($block['disciplines']))
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2>{{$block['fields']['heading']}}</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <input type="text" id="keyword" class="course-archive-multiple__keyword" name="discipline"
                 placeholder="Search">
          <button class="course-archive-multiple__keyword-button">Search</button>
          <button class="course-archive-multiple__reset-button">Reset</button>
        </div>
        <div class="col-12">
          <form action="" method="post" class="course-archive-multiple__form" id="course-archive-multiple__form"
                name="course-archive-multiple__form">
            <input type="hidden" name="ajax_url" id="ajax_url" value="/wp/wp-admin/admin-ajax.php">
            <input type="hidden" name="discipline"
                   value="{{ isset($block['selected_discipline']) ? $block['selected_discipline'] : ''}}">

            @if(!empty($block['levels_of_study']))
              <input type="hidden" name="level_of_study"
                     value="{{ isset($block['selected_level_of_study']) ? $block['selected_level_of_study'] : ''}}">
            @endif

            <div class="course-archive-multiple__selector disciplines">
              {{-- <a class="discipline active" href="" data-id="all" data-type="discipline">
                <span>All</span>
              </a> --}}
              @foreach ($block['disciplines'] as $item)
                <a class="discipline" href="?discipline={{ $item->term_id }}" data-type="discipline"
                   data-id={{ $item->term_id}}>
                  <span>{!! $item->name !!}</span>
                </a>
              @endforeach
            </div>

            @if(!empty($block['levels_of_study']))
              <div class="w-100"></div>
              <div class="course-archive-multiple__selector levels-of-study">
                {{-- <a class="level-of-study active" href="" data-id="all" data-type="level_of_study">
                  <span>All</span>
                </a> --}}
                @foreach ($block['levels_of_study'] as $item)
                  <a class="level-of-study" href="?level_of_study={{ $item->term_id }}" data-type="level_of_study"
                     data-id={{ $item->term_id}}>
                    <span>{!! $item->name !!}</span>
                  </a>
                @endforeach
              </div>
            @endif

          </form>
        </div>
      </div>
    </div>
  @endif
  <div class="container">
    <div class="row">
      <div class="col-12 course-archive-multiple__info">
        Swinburne Online has
        <span class="course-archive-multiple__courses-count">{{ count($block['courses']) }}</span>
        <span class="course-archive-multiple__level-of-study-name"></span>
        <span class="course-archive-multiple__discipline-name"></span>
        online courses.
      </div>
      <div class="col-12 col-md-3 course-archive-multiple__dropdown-filters">
        <div class="course-archive-multiple__selector disciplines">
          @foreach ($block['disciplines'] as $item)
            <a class="discipline" href="?discipline={{ $item->term_id }}" data-type="discipline"
               data-id={{ $item->term_id}}>
              <span>{!! $item->name !!}</span>
            </a>
          @endforeach
        </div>
        @if(!empty($block['levels_of_study']))
          <div class="course-archive-multiple__selector levels-of-study">
            @foreach ($block['levels_of_study'] as $item)
              <a class="level-of-study" href="?level_of_study={{ $item->term_id }}" data-type="level_of_study"
                 data-id={{ $item->term_id}}>
                <span>{!! $item->name !!}</span>
              </a>
            @endforeach
          </div>
        @endif
      </div>
      <div class="col-12 col-md-9 course-archive-multiple__courses">
        @if (!empty($block['courses']))
          @foreach ($block['courses'] as $item)
            @include('components.course', ['item' => $item])
          @endforeach
          @php wp_reset_postdata(); @endphp
        @endif
      </div>
    </div>
  </div>
</section>
