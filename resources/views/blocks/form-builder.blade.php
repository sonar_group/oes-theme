{{--
  Title: Form Builder
  Description: Form Builder - Content Blocks
  Icon: forms
  Keywords: Form Builder Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  global $form_id;

  $block = get_field('block');
  $form_id = $block['form_name']->ID;
  $form = Form::getData($form_id);
  $salesforce = App::salesforce();
  $form_settings = get_field('settings', 'options');
  $experian = false;

  if(isset($form_settings['experian'])){
      $experian = $form_settings['experian'];
  }

  $formCode = 'formbuilder-'.$form_id.rand(10,100);

@endphp

@if($block['form_name'])

  @if($post->post_type != 'course' && $post->post_parent != true)
    <script type='text/javascript' src='{{ \App\google_recaptcha_url() }}'></script>
  @endif

  <section class="block form {{ $form['wrapper_class'] }}">
    <div class="container">
      <form method="POST" class="form-builder-form form-data {{ $form['form_class'] }}" id="form-{{$form_id}}" _lpchecked="1" data-experian="{{$experian}}" intl="{!! $formCode !!}">
        <input type="hidden" name="action" value="form_builder_submit">
        <input type="hidden" name="form_id" value="{{ $form_id }}">
        <div class="row">
          {!! apply_filters('the_content', get_post_field('post_content', $block['form_name']->ID)) !!}
        </div>
        @if($experian)
          <div class="row">
            <div class="col-12">
              <div class="form-group full-width experian-loader"></div>
            </div>
          </div>
        @endif
      </form>
    </div>
  </section>

@endif


