{{--
  Title: FAQs Archive
  Description: FAQs Archive Block - Content Blocks
  Icon: cover-image
  Keywords: FAQs Archive Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block faqs-archive">
  @if (!empty($block['faqs_category']))
    <div class="container">
      <div class="row">
        <form action="" method="post" class="faqs-archive__form" id="faqs-archive__form" name="faqs-archive__form">
          <input type="hidden" name="faqs_category" value="{{ isset($block['selected_faqs_category']) ? $block['selected_faqs_category'] : ''}}">
          <input type="hidden" name="ajax_url" id="ajax_url" value="/wp/wp-admin/admin-ajax.php">
          <a class="faqs-archive__selector active" href="" data-id='All' >
            <span>All</span>
          </a>
          @foreach ($block['faqs_category'] as $item)
            <a class="faqs-archive__selector faqs-category" href="{{ $item->slug }}" data-id={{ $item->term_id}}>
              <span>{!! $item->name !!}</span>
            </a>
          @endforeach
        </form>
      </div>
    </div>
  @endif
  <div class="container">
    <div class="row faqs-archive__faqs">
      @if (!empty($block['faqs']))
        <div class="col-12">
          <div class="accordion faqs-group" id="faqsAccordion_archive">
            @php $count = count($block['faqs']); @endphp
            @foreach ($block['faqs'] as $i => $item)

              @php $blocks = App::parseBlock($item['content'], 'single-faq'); @endphp
              @if($blocks)
                @foreach($blocks as $faq)
                  @php $i++ @endphp

                  <div class="card {{$i}}">
                    <div class="card-header {{ $i !== 0 ? 'collapsed' : '' }}" id="heading_archive_{{$i}}"
                         data-toggle="collapse"
                         data-target="#collapse_archive_{{$i}}" aria-expanded="true"
                         aria-controls="collapse_archive_{{$i}}">
                      <h5>{{ $faq['block_question'] }}</h5>
                    </div>

                    <div id="collapse_archive_{{$i}}" class="collapse {{ $i === 0 ? 'show' : '' }}"
                         aria-labelledby="heading_archive_{{$i}}"
                         data-parent="#faqsAccordion_archive">
                      <div class="card-body">
                        {!! $faq['block_answer'] !!}
                      </div>
                    </div>
                  </div>

                @endforeach
              @endif

            @endforeach
            @php wp_reset_postdata(); @endphp
          </div>
        </div>
      @endif
    </div>
  </div>
</section>
