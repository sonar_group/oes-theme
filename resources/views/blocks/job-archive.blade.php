{{--
  Title: Job Archive
  Description: Job Archive Block - Content Blocks
  Icon: cover-image
  Keywords: Job Archive Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block job-archive">
  <div class="container">
    <div class="row job-archive__filters">
      <div class="col-12 col-md-4">
        <div class="job-archive__filter_values departments">
          @if (!empty($block['departments']))
            <div class="filter-label">
              <span class="current-sort closed" data-sorting="all"><span class="current-sort__details">Department</span></span>
            </div>
            <ul class="filter-check">
              <li class="department" data-type="department" data-id="all">All Departments</li>
              @foreach ($block['departments'] as $department)
                <li class="department" data-type="department" data-id="{{ $department->term_id }}">{{ $department->name }}</li>
              @endforeach
            </ul>
          @endif
        </div>
      </div>
      <div class="col-12 col-md-4">
        <div class="job-archive__filter_values locations">
          @if (!empty($block['locations']))
            <div class="filter-label">
              <span class="current-sort closed" data-sorting="all"><span class="current-sort__details">Location</span></span>
            </div>
            <ul class="filter-check">
              <li class="location" data-type="location" data-id="all">All Locations</li>
              @foreach ($block['locations'] as $location)
                <li class="location" data-type="location" data-id="{{ $location->term_id }}">{{ $location->name }}</li>
              @endforeach
            </ul>
          @endif
        </div>
      </div>
      <div class="col-12 col-md-4">
        <div class="job-archive__filter_values dates">
          <div class="filter-label">
            <span class="current-sort closed" data-sorting="sort-most-recent"><span class="current-sort__details">Date</span></span>
          </div>
          <ul class="filter-check">
            <li class="date" data-type="date" data-id="sort-most-recent">Most Recent</li>
            <li class="date" data-type="date" data-id="sort-closing-date">Closing Date</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row job-archive__articles">
      @if (!empty($block['job']))
        @foreach ($block['job'] as $item)
          <div class="col-12">
            <div class="job-archive__article">
              @php $blocks = App::parseBlock($item['content'], 'single-job'); @endphp
              @if($blocks)
                @foreach($blocks as $job)
                  <h4 class="job-archive__article__title">{{ $job['block_title'] }}</h4>
                  <div class="job-archive__article__description">{{ $job['block_short_description'] }}</div>
                  <div class="row">
                    <div class="col-12 col-md-6">
                      <div class="job-archive__article__link">
                        @if ($job['block_link'])
                          <a href="{{$job['block_link']['url']}}" target="{{$job['block_link']['target']}}">{{$job['block_link']['title']}}</a>
                        @endif
                      </div>
                    </div>
                    <div class="col-12 col-md-6">
                      <div class="job-archive__article__date">
                        @if ($job['block_closing_date'])
                          <p>Application closes <span>@php echo esc_html(preg_replace('/(\d{4})(\d{2})(\d{2})/i', '$3/$2/$1', $job['block_closing_date'])); @endphp</span></p>
                        @endif
                      </div>
                    </div>
                  </div>
                @endforeach
              @endif
            </div>
          </div>
        @endforeach
        @php wp_reset_postdata(); @endphp
      @endif
    </div>
  </div>
</section>
