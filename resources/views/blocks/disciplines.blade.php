{{--
  Title: Disciplines
  Description: Show all disciplines
  Icon: clipboard
  Keywords: Disciplines Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block disciplines">
  <div class="container disciplines__heading">
    <div class="row">
      <div class="col-12 col-md-6">
        <h2>{!! $block['fields']['heading'] !!}</h2>
      </div>
      <div class="col-12 col-md-6">
        <a href="#">View all courses and degrees</a>
      </div>
    </div>
  </div>
  <div class="container disciplines__container">
    <div class="row">
      @if (!empty($block['disciplines']))
        @foreach ($block['disciplines'] as $item)
          <div class="col-12 col-md-4 disciplines__single">
            <div class="disciplines__single__details">
              <h5>{{ $item->name }}</h5>
              <div class="body-2 disciplines__single__description">{{ $item->description }}</div>
            </div>
          </div>
        @endforeach
        @php wp_reset_postdata(); @endphp
      @endif
    </div>
  </div>
</section>
