{{--
  Title: News Archive
  Description: News Archive Block - Content Blocks
  Icon: cover-image
  Keywords: News Archive Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block news-archive">
  @if (isset($block['featured']))
    <div class="container news-archive__featured-news">
      <div class="row">
        <div class="col-12 col-md-5 bg-image"
             style="background-image:url({!! $block['featured']['image'] !!});">
        </div>
        <div class="col-12 col-md-6 offset-md-1 content">
          <p>{{ $block['featured']['term'] }}</p>
          <p>{{ $block['featured']['read_time'] }}</p>
          <h4>{{ $block['featured']['title'] }}</h4>
          <p>{!! $block['featured']['excerpt'] !!}</p>
          <a href="{{ $block['featured']['permalink'] }}">Read More</a>
        </div>
      </div>
    </div>
  @endif
  @if (!empty($block['categories']))
    <div class="container news-archive__categories">
      <div class="row">
        <form action="" method="post" class="col-12 news-archive__categories__form" id="news-archive__categories__form"
              name="news-archive__categories__form">
          <input type="hidden" name="category"
                 value="{{ isset($block['selected_categories']) ? $block['selected_categories'] : ''}}">
          <input type="hidden" name="ajax_url" id="ajax_url" value="/wp/wp-admin/admin-ajax.php">
          <a href="" data-id='All'>
            <div class="news-archive__category {{ !empty($block['selected_categories']) ? '' : 'active'}}">
              All
            </div>
          </a>
          @foreach ($block['categories'] as $item)
            <a href="{{ $item->slug }}" data-id={{ $item->term_id}}>
              <div
                class="news-archive__category {{ !empty($block['selected_categories']) && $block['selected_categories'] == $item->term_id ? 'active' : ''}}">
                {!! $item->name !!}
              </div>
            </a>
          @endforeach
        </form>
      </div>
    </div>
  @endif
  <div class="container">
    <div class="row news-archive__articles" id="ajax-posts">
      @if (!empty($block['news']))
        @php $count = count($block['news']); @endphp
        @foreach ($block['news'] as $i => $item)
          <div class="col-12 col-md-4">
            <div class="news-archive__article">
              <a href="{{ $item['permalink'] }}">
                @if (!empty($item['image']))
                  <img src="{{ $item['image'] }}" loading="lazy" alt="">
                @endif
                <h4>{{ $item['title'] }}</h4>
                <p>{!! $item['excerpt'] !!}</p>
                <p>{{ $item['term'] }}</p>
                <p>{{ $item['read_time'] }}</p>
              </a>
            </div>
          </div>
        @endforeach
        @php wp_reset_postdata(); @endphp
      @endif
    </div>
    <div id="more_posts" class="btn btn-primary"
         data-category="{{ !empty($block['selected_categories']) ? $block['selected_categories'] : '' }}"
         data-featured="{{ !empty($block['featured']['id']) ? $block['featured']['id'] : '' }}" disabled="{!! ($count > 5) ? '' : 'disabled' !!}">Load More
    </div>
  </div>
</section>
