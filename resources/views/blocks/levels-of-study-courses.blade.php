{{--
  Title: Levels of Study Courses
  Description: Show all courses based on a select level of study
  Icon: clipboard
  Keywords: Levels of Study Courses Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block levels-of-study-courses">
  <div class="container levels-of-study-courses__heading">
    <div class="row">
      <div class="col-12">
        <h2>{!! $block['fields']['heading'] !!}</h2>
      </div>
    </div>
  </div>

  @if($block['fields']['show_filters'] === 'on')
    <div class="container levels-of-study-courses__filters">
      <div class="row">
        <div class="col-12">
          @if (!empty($block['disciplines']))
            <form action="" method="post" class="levels-of-study-courses__form" id="levels-of-study-courses__form" name="levels-of-study-courses__form">
            <input type="hidden" name="level_of_study" value="{{ isset($block['fields']['level_of_study']) ? $block['fields']['level_of_study']->term_id : ''}}">
            <input type="hidden" name="discipline" value="{{ isset($block['selected_discipline']) ? $block['selected_discipline'] : ''}}">
              <input type="hidden" name="ajax_url" id="ajax_url" value="/wp/wp-admin/admin-ajax.php">
              <a class="levels-of-study-courses__selector active" href="?discipline=all" data-id='All' >
                <span>All</span>
              </a>
              @foreach ($block['disciplines'] as $item)
                <a class="levels-of-study-courses__selector discipline" href="?discipline={{ $item->term_id }}" data-id={{ $item->term_id}}>
                  <span>{!! $item->name !!}</span>
                </a>
              @endforeach
            </form>
          @endif
        </div>
      </div>
    </div>
  @endif

  <div class="container">
    <div class="row levels-of-study-courses__courses">
      @if (!empty($block['courses']))
        @php
          $term = $block['fields']['level_of_study'];
        @endphp
        @foreach ($block['courses'] as $item)
          @if($term->name == $item['level_of_study'])
            @include('components.course', ['item' => $item])
          @endif
        @endforeach
        @php wp_reset_postdata(); @endphp
      @endif
    </div>
  </div>
</section>
