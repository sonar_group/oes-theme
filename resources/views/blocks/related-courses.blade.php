{{--
  Title: Related Courses
  Description: Related Courses Block - Content Blocks
  Icon: excerpt-view
  Keywords: Related Courses Block
  Category: course-blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp

<section class="block related-courses">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6">
        @if(isset($block['heading']))
          <h2>{{ $block['heading'] }}</h2>
        @endif
      </div>
      <div class="col-12 col-md-6">
          @if(!empty($block['link']))
            <a class="btn" href="{{ $block['link']['url'] }}" target="{{ $block['link']['target'] }}">
              {{ $block['link']['title'] }}
            </a>
          @endif
      </div>
    </div>
    <div class="row">
      @foreach($block['courses'] as $item)
        <a href="{!! get_permalink($item) !!}" class="col-12 col-md-4 column">
          <div class="content">
              @if ((!empty(get_the_post_thumbnail_url($item->ID, 'full'))))
                <img src="{{get_the_post_thumbnail_url($item->ID, 'full')}}" loading="lazy" alt="">
              @endif
              <div>{{get_the_title($item)}}</div>
              <div>{{get_the_terms($item->ID, 'discipline')[0]->name}}</div>
          </div>
        </a>
      @endforeach
      @php(wp_reset_postdata())
    </div>
  </div>
</section>
