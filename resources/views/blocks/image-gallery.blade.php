{{--
  Title: Image Gallery
  Description: Image Gallery Block - Content Blocks
  Icon: images-alt2
  Keywords: Image Gallery Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php $block = get_field('block'); @endphp

<section class="block image-gallery">
  <div class="container">
    @foreach($block['image_group'] as $group)
      <div class="row">
        <div class="col-md-4 {!! ($group['layout'] === 'left')? 'order-2': '' !!}">
          <div class="bg-image" style="background-image:url({!! ($group['left_image'])? $group['left_image']['url']: '' !!});"></div>
        </div>
        <div class="col-md-8">
          <div class="bg-image" style="background-image:url({!! ($group['right_image'])? $group['right_image']['url']: '' !!});"></div>
        </div>
      </div>
    @endforeach
  </div>
</section>
