{{--
  Title: Tabbed Content
  Description: Tabbed Content Block - Content Blocks
  Icon: portfolio
  Keywords: Tabbed Content Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
  $r = rand(50,99);

  if ($block['stacked_tabs'] === true) {
      $tabLayout = 'stacked';
      $open = 'col-md-2';
      $close = '</div><div class="col-md-10">';
  } else {
      $tabLayout = 'top';
      $open = 'col-md-12';
      $close = '';
  }
@endphp

<section class="block tabbed-content">
  <div class="container">
    <div class="row">
      <div class="{{ $open }}">

        <ul id="tabs" class="nav nav-fill" role="tablist">
          @foreach($block['tab_group'] as $i => $tab)
            @php($title = str_replace(' ', '', $tab['title']))
            <li class="nav-item">
              <h5 id="tab-structure" href="#{{$title}}" class="nav-link {{ $i === 0 ? 'active' : '' }}"
                  data-toggle="tab" role="tab">
                {{$tab['title']}}
              </h5>
            </li>
          @endforeach
        </ul>

        {!! $close !!}

        @php(reset_rows())

        <div id="content" class="tab-content" role="tablist">
          @foreach($block['tab_group'] as $i => $tab)
            @php($title = str_replace(' ', '', $tab['title']))
            <div id="{{$title}}" class="card parent tab-pane fade {{ $i === 0 ? 'show active' : '' }}" role="tabpanel"
                 aria-labelledby="tab-{{$title}}">
              <div class="card-header" role="tab" id="heading-{{$title}}" data-toggle="collapse"
                   href="#collapse-{{$title}}" aria-expanded="true">
                <h5>{{$tab['title']}}</h5>
              </div>
              <div class="collapse show" id="collapse-{{$title}}" data-parent="#content"
                   role="tabpanel">
                <div class="card-body">
                  {!! $tab['content'] !!}

                  @if($tab['faqs'])
                    @include('components.faq', ['faqs' => $tab['faqs'], 'parent' => $r])
                  @endif

                </div>
              </div>
            </div>
          @endforeach
        </div>

      </div>
    </div>
  </div>
</section>
