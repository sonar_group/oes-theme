{{--
  Title: Parallax Page
  Description: Parallax Page Block - Content Blocks
  Icon: cover-image
  Keywords: Parallax Page Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

@php
  $block = get_field('block');
  $r = rand(1,99);
@endphp

<section class="block parallax-page">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-2">
        <ul class="nav flex-column sidebar">
          @if($block['builder'])
            @foreach($block['builder'] as $i => $nav)
              @php($title = str_replace(' ', '', $nav['navigation_label']))
              <li class="nav-item">
                <a class="nav-link{{ $i === 0 ? ' active' : '' }}" href="#{{$title}}">{{$nav['navigation_label']}}</a>
              </li>
            @endforeach
          @endif
        </ul>
      </div>
      <div class="col-sm-10">
        <div class="row banner">
          <div class="bg-image"
               style="background-image:url({!! ($block['page_header']['image'])? $block['page_header']['image']['url']: '' !!});"></div>
          <div class="col-md-6">
            <h1>{{ $block['page_header']['heading'] }}</h1>
            {!! $block['page_header']['copy'] !!}
            @if(isset($block['page_header']['link']['url']))
              <a class="btn" href="{{ $block['page_header']['link']['url'] }}"
                 target="{{ $block['page_header']['link']['target'] }}">
                {{ $block['page_header']['link']['title'] }}
              </a>
            @endif
          </div>
        </div>
        @if($block['builder'])
          @foreach($block['builder'] as $i => $content)

            <?php
              $title = str_replace(' ', '', $content['navigation_label']);

              if($content['layout'] == 'right') {
                $position = 'order-2';
              }
              if($content['layout'] == 'left') {
                $position = '';
              }
            ?>

            <div class="row" id="{{$title}}">
              <div class="col-md-6 {{$position}} {!! ($content['video_url'])? 'bg-video': 'bg-image' !!}" {!! ($content['video_url'])? 'data-toggle="modal" data-target="#videoModal_parallax' . $r . '"': '' !!} style="background-image:url({!! ($content['image'])? $content['image']['url']: '' !!});">
                  @if($content['video_url'])
                    <span class="icon">X</span>
                  @endif
              </div>
              <div class="col-md-6 content">
                {!! $content['copy'] !!}
                @if(isset($content['link']['url']))
                  <a class="text-link" href="{{ $content['link']['url'] }}" target="{{ $content['link']['target'] }}">
                    {{ $content['link']['title'] }}
                  </a>
                @endif
              </div>
              @if($content['video_url'])
                @include('components.video-modal', ['video' => $content['video_url'], 'modalName' => '_parallax'])
              @endif
            </div>
          @endforeach
        @endif
      </div>
    </div>
  </div>
</section>

