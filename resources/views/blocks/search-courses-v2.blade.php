{{--
  Title: Search Courses V2
  Description: Search courses by discipline or title
  Icon: search
  Keywords: Search Course Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

<section class="block search-courses-v2">
  @if (!empty($block['disciplines']))
    <div class="container">
      <div class="row">
        @if($block['fields']['heading'])
          <div class="col-12">
            <h1>{{$block['fields']['heading']}}</h1>
          </div>
        @endif
        <div class="col-12">
          <div class="search-courses-v2__disciplines">
            <input type="text" id="discipline" name="discipline" placeholder="Search"
                   data-disciplines="{{$block['disciplines_name'] . '|' . $block['courses_name']}}">
          </div>
        </div>
      </div>
    </div>
  @endif
  <div class="search-courses-v2__results">
    <div class="container">
      <div class="row search-courses-v2__courses">
        @if (!empty($block['courses']))
          @foreach ($block['courses'] as $item)
            <div class="col-md-4">
              <div class="content search-courses-v2__course" data-course="{{ $item['title'] }}"
                   data-discipline="{{ $item['term'] }}" data-link="{{ $item['permalink'] }}">
              </div>
            </div>
          @endforeach
        @endif
        @if (!empty($block['disciplines']))
          @foreach ($block['disciplines'] as $item)
            <div class="col-md-4">
              <div class="content search-courses-v2__course" data-course="{{ $item->name }}"
                   data-link="/online-courses/?discipline={{ $item->term_id }}">
              </div>
            </div>
          @endforeach
        @endif
        <div class="col-12 no-results">
          No Results
        </div>
      </div>
    </div>
  </div>
</section>
