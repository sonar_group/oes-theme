{{--
  Title: Team
  Description: Team Block - Content Blocks
  Icon: excerpt-view
  Keywords: Team Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
  $r = rand(1,50);
@endphp


<section class="block team">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if(isset($block['heading']))
          <h2>{{ $block['heading'] }}</h2>
        @endif

        @if(isset($block['content']))
          {!! $block['content'] !!}
        @endif

      </div>
    </div>
    <div class="row">

        @if($block['team'])
          @foreach($block['team'] as $block_content)
            @php $blocks = App::parseBlock($block_content, 'single-team'); @endphp

            @if($blocks)
              @foreach($blocks as $team)
                @php $r++ @endphp

                @php($title = str_replace(' ', '', $team['block_name']))

                <div class="column col-md-4">
                  <div class="content">
                    @if(isset($team['block_image']))
                      <div class="bg-image" style="background-image: url({{ wp_get_attachment_url($team['block_image']) }});"></div>
                    @endif
                    @if(isset($team['block_name']))
                      <h4>{{ $team['block_name'] }}</h4>
                    @endif
                      @if(isset($team['block_role']))
                        <h4>{{ $team['block_role'] }}</h4>
                      @endif
                    @if(isset($team['block_short_bio']))
                      {!! $team['block_short_bio'] !!}
                    @endif
                      <button data-toggle="modal"
                              data-target="#teamModal_{{$title}}">Read Bio</button>
                  </div>
                </div>

                @include('components.team-modal', ['team' => $team])

              @endforeach
            @endif

          @endforeach
        @endif

      </div>
    </div>
  </div>
</section>

