{{--
  Title: Icons
  Description: Icons Block - Content Blocks
  Icon: excerpt-view
  Keywords: Icons Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php
  $block = get_field('block');
@endphp



<section class="block icons">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($block['heading'])
          <h2>{{$block['heading']}}</h2>
        @endif
      </div>

      @if($block['icon'])
        @foreach($block['icon'] as $tile)
          @include('components.tiles', [
              'blockClass' => 'icon',
              'tile' => $block['icon'],
              'layout' => $block['layout'],
              'image' => $tile['icon'],
              'link' => $tile['link'],
              'btnClass' => null,
          ])
        @endforeach
      @endif

    </div>
  </div>
</section>
