{{--
  Title: Testimonial Carousel
  Description: Testimonial Carousel - Content Blocks
  Icon: embed-photo
  Keywords: Testimonial Carousel Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}

@php
  $block = get_field('block');
  $modalName = 'testimonial';
  $r = rand(51,99);
@endphp

<section class="block testimonial-carousel">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($block['heading'])
          <h2>{{$block['heading']}}</h2>
        @endif
      </div>
      <div class="col-12">
        <div class="testimonial-carousel__controls">
          <span class="icon slick-prev">PREV</span>
          <span class="icon slick-next">NEXT</span>
        </div>
        <div class="testimonial-carousel__slider">
          @foreach($block['carousel_item'] as $r => $item)
            <div class="slide" {!! ($item['video_url'])? 'data-toggle="modal" data-target="#videoModal_testimonial_' . $r . '"': '' !!}>
              <div class="row">
                <div class="col-md-6">
                  <div class="{!! ($item['video_url'])? 'bg-video': 'bg-image' !!}" style="background-image:url({!! ($item['image'])? $item['image']['url']: '' !!});">
                    @if($item['video_url'])
                      <span class="icon">X</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <p>{{$item['testimonial']}}</p>
                  <p>{{$item['name']}}</p>
                  <p>{{$item['subtitle']}}</p>
                  @if(isset($item['long_testimonial']))
                    <a class="btn" data-toggle="modal" data-target="#contentModal_{{$r}}">View full testimonial</a>
                  @endif
                </div>
              </div>
            </div>
          @endforeach
        </div>
        @foreach($block['carousel_item'] as $r => $item)
          @if(isset($item['video_url']))
            @include('components.video-modal', ['video' => $item['video_url'], 'r' => $r, 'modalName' => '_testimonial'])
          @endif
        @endforeach
      </div>
    </div>
  </div>
</section>

{{--@foreach($block['carousel_item'] as $r => $item)--}}
{{--  @if(isset($item['long_testimonial']))--}}
{{--    @include('components.content-modal', ['content' => $item['long_testimonial'], 'r' => $r])--}}
{{--  @endif--}}
{{--@endforeach--}}




