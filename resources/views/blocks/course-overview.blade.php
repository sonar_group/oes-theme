{{--
  Title: Course Overview
  Description: Course Overview Block - Content Blocks
  Icon: index-card
  Keywords: Course Overview Block
  Category: course-blocks
  Mode: edit
  SupportsMode: false
--}}
@php $block = get_field('block'); @endphp

<section class="block course-overview">
  <div class="container">
    <div class="row">
      <div class="col-md-7 left">
        @if($block['left_content'])
          {!! $block['left_content'] !!}
        @endif
        <button class="btn" data-toggle="modal" id="brochureBtnInpage" data-target="#brochureModal"><span>Download Brochure</span></button>
      </div>
      <div class="col-md-5 right">
        @if($block['right_content'])
          {!! $block['right_content'] !!}
        @endif
      </div>
    </div>
  </div>
</section>
