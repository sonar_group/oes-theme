{{--
  Title: Call Out Banner
  Description: Call Out Banner Block - Content Blocks
  Icon: cover-image
  Keywords: Call Out Banner Block
  Category: blocks
  Mode: edit
  SupportsMode: false
--}}
@php $block = get_field('block'); @endphp

<section class="block call-out-banner">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        @if($block['heading'])
          <h2>{{$block['heading']}}</h2>
        @endif
      </div>
      <div class="col-md-6">
        @if($block['content'])
          {!! $block['content'] !!}
        @endif
        @if(isset($block['link']['url']))
          <a class="btn" href="{{ $block['link']['url'] }}" target="{{ $block['link']['target'] }}">
            {{ $block['link']['title'] }}
          </a>
        @endif
          @if(isset($block['link_2']['url']))
            <a class="btn" href="{{ $block['link_2']['url'] }}" target="{{ $block['link_2']['target'] }}">
              {{ $block['link_2']['title'] }}
            </a>
          @endif
      </div>
    </div>
  </div>
</section>
