@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
  @endif

  @while (have_posts()) @php the_post() @endphp
    @if(get_post_type() != 'post')
      @include('partials.content.content-'.get_post_type())
    @endif
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection
