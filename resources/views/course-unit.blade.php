@extends('layouts.app')

@section('content')

  <pre><h1>Course Unit Page</h1></pre>
  @while(have_posts()) @php the_post() @endphp
  @php
    global $post;
    echo get_the_title( wp_get_post_parent_id( $post->post->ID ) );
  @endphp
  {{--  @include('partials.page-header')--}}
  @php the_content() @endphp
  @endwhile

@endsection


