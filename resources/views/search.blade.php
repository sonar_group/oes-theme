@extends('layouts.app')

@section('content')
  @include('partials.page-header')
  @php
  $result = App::searchPageResults();
  @endphp
  <div class="wrap">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
        <?php get_search_form(); ?>
        </main><!-- #main -->
    </div><!-- #primary -->
  </div><!-- .wrap -->

  <div class="tab">
    <a href="{{$result['actual_link']}}" class="{{ $result['current_tab'] == 'all' ? 'active' : '' }}">All ({{ $result['total_results'] }})</a>
    <a href="{{$result['actual_link'] . '&post_type=course'}}" class="{{ $result['current_tab'] == 'course' ? 'active' : '' }}">Course ({{ $result['total_courses'] }})</a>
    <a href="{{$result['actual_link'] . '&post_type=post'}}" class="{{ $result['current_tab'] == 'article' ? 'active' : '' }}">Article ({{ $result['total_articles'] }})</a>
  </div>

  <div class="filters">
    <div class="filter-current">
      Sort by: {{$result['current_sort']}}
    </div>
    <a href="{{$result['link']}}">Relevance</a>
    <a href="{{$result['link'] . '&order=ASC&orderby=title'}}">Title: A-Z</a>
    <a href="{{$result['link'] . '&order=DESC&orderby=title'}}">Title: Z-A</a>
  </div>

  @if (!have_posts())
    <section class="block search-results">
      <div class="alert alert-warning">
        {{ __('Sorry, no results were found.', 'sage') }}
      </div>
    </section>
  @endif

  @while(have_posts()) @php the_post() @endphp
  @include('partials.content.content-search')
  @endwhile

  @include('partials.pagination')
@endsection
