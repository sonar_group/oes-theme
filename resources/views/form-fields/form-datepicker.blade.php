{{--
  Title: Datepicker Field
  Description: Salutation Dropdown Field - Form Fields
  Icon: calendar-alt
  Keywords: Dropdown Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $form = $block['form'];
  $block = get_field('block');
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
    <div class="form-group">
      @if($block['label'])
        <label for="{{ ($block['name'])? $block['name']: '' }}">{{ $block['label'] }}
          @if($block['required'])
            <span class="required-field" title="Required">*</span>
          @endif
        </label>
      @endif

      @if($block['name'])
        <input type="date" {{ ($block['required'])? 'required': '' }}
                id="{{ $block['id'] }}"
                name="{{ $block['name'] }}"
                class="form-control {{ $block['custom_css_class'] }}"
                title="{{ $block['title'] }}">
      @endif
    </div>
  </div>
@endif
