{{--
  Title: Recaptcha
  Description: Recaptcha - Form Fields
  Icon: image-rotate
  Keywords: Recaptcha
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $block = get_field('block');
  $recaptcha = App::recaptcha();
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
    <div class="form-group form-group-recaptcha">
      @if($block['label'])
        <label for="{{ ($block['name'])? $block['name']: '' }}">{{ $block['label'] }}
          @if($block['required'])
            <span class="required-field" title="Required">*</span>
          @endif
        </label>
      @endif

      @if(isset($recaptcha['site_key']))
          <div class="recaptcha-error d-none">{{ $block['error_message'] }}</div>
          <div class="g-recaptcha {{ $block['custom_css_class'] }}" data-sitekey="{{ $recaptcha['site_key'] }}"></div><br>
      @endif
    </div>
  </div>
@endif
