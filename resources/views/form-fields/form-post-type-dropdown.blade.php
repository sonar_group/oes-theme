{{--
  Title: Dropdown Field - Post Types
  Description: Dropdown Field - Form Fields - Post Types
  Icon: list-view
  Keywords: Dropdown Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $form = $block['form'];
  $block = get_field('block');
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
    <div class="form-group form-group-dropdown">
      @if($block['label'])
        <label for="{{ ($block['name'])? $block['name']: '' }}">{{ $block['label'] }}
          @if($block['required'])
            <span class="required-field" title="Required">*</span>
          @endif
        </label>
      @endif

      @if($block['name'] &&  $block['list'])
        <select name="{{ $block['name'] }}" id="{{ $block['id'] }}"
                class="form-control {{ $block['custom_css_class'] }}"
                title="{{ $block['title'] }}" {{ ($block['required'])? 'required': '' }}>
          @if($block['initial_item'])
            <option> {{ $block['initial_item'] }} {{ ($block['required'])? '*': '' }}</option>
          @endif

          @foreach($block['list'] as $item)
            <option value="{{ $item->ID }}">
              {{ $item->post_title }}
            </option>
          @endforeach
        </select>
      @endif
    </div>
  </div>
@endif
