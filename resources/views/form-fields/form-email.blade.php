{{--
  Title: Email Field
  Description: Email Field - Form Fields
  Icon: email
  Keywords: Email Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $form = $block['form'];
  $block = get_field('block');
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
    <div class="form-group form-group-email">
      @if($block['label'])
        <label for="{{ ($block['name'])? $block['name']: '' }}">{{ $block['label'] }}
          @if($block['required'])
            <span class="required-field" title="Required">*</span>
          @endif
        </label>
      @endif

      @if($block['name'])
        <input type="email" {{ ($block['required'])? 'required': '' }} id="{{ $block['id'] }}"
               name="{{ $block['name'] }}"
               class="form-control {{ $block['custom_css_class'] }}"
               placeholder="{{ $block['placeholder'] }} {{ ($block['required'])? '*': '' }}"
               pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$" maxlength="80" size="20"
               title="{{ $block['title'] }}">
      @endif
    </div>
  </div>
@endif
