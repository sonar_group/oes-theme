{{--
  Title: Yes/No Field
  Description: Yes/No Field - Form Fields
  Icon: saved
  Keywords: Text Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $form = $block['form'];
  $block = get_field('block');
@endphp

@if($block)
    <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
        <div class="form-group form-group-text">
            @if($block['name'])
                @if($block['label'])
                    <label for="{{ ($block['name'])? $block['name']: '' }}">{{ $block['label'] }}
                        @if($block['required'])
                            <span class="required-field" title="Required">*</span>
                        @endif
                    </label>
                @endif
                <div class="form-options-wrap">
                    <div class="form-check form-check-inline">
                        <input type="radio" {{ ($block['required'])? 'required': '' }}
                        id="{{ $block['id'] }}-yes"
                               name="{{ $block['name'] }}"
                               class="form-check-input {{ $block['custom_css_class'] }}"
                               value="yes"
                        >
                        <label for="{{ $block['id'] }}-yes" class="form-check-label">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="radio" {{ ($block['required'])? 'required': '' }}
                        id="{{ $block['id'] }}-no"
                               name="{{ $block['name'] }}"
                               class="form-check-input {{ $block['custom_css_class'] }}"
                               value="yes"
                        >
                        <label for="{{ $block['id'] }}-no" class="form-check-label">No</label>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endif