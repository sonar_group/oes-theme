{{--
  Title: Phone Field
  Description: Phone Field - Form Fields
  Icon: phone
  Keywords: Phone Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $form = $block['form'];
  $block = get_field('block');
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
    <div class="form-builder-phone form-group form-group-phone">
      <div class="form-phone-wrap">
      @if($block['label'])
        <label for="{{ ($block['name'])? $block['name']: '' }}">{{ $block['label'] }}
          @if($block['required'])
            <span class="required-field" title="Required">*</span>
          @endif
        </label>
      @endif

      @if($block['name'])
        <input type="text" {{ ($block['required'])? 'required': '' }} id="{{ $block['id'] }}"
               name="{{ $block['name'] }}"
               class="form-phone form-control {{ $block['custom_css_class'] }}"
               placeholder="{{ $block['placeholder'] }} {{ ($block['required'])? '*': '' }}"
               pattern="(?=(?:\D*\d){10}).*" maxlength="80" size="20"
               title="{{ $block['title'] }}">
      @endif
      </div>
    </div>
  </div>
@endif
