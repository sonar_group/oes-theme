{{--
  Title: Text Area Field
  Description: Text Area Field - Form Fields
  Icon: text-page
  Keywords: Text Area Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $form = $block['form'];
  $block = get_field('block');
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6' }}">
    <div class="form-group form-group-textarea">
      @if($block['label'])
        <label for="{{ ($block['name'])? $block['name']: '' }}">{{ $block['label'] }}
          @if($block['required'])
            <span class="required-field" title="Required">*</span>
          @endif
        </label>
      @endif

      @if($block['name'])
        <textarea
          id="{{ $block['id'] }}"
          wrap="soft"
          name="{{ $block['name'] }}"
          class="form-control {{ $block['custom_css_class'] }}"
          title="{{ $block['title'] }}"
          {{ ($block['required'])? 'required': '' }}
{{--          placeholder="{{ $block['placeholder'] }}--}}
{{--          {{ ($block['required'])? '*': '' }}"--}}
        ></textarea>
      @endif
    </div>
  </div>
@endif