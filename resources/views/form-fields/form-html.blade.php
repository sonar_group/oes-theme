{{--
  Title: HTML Field
  Description: HTML Field - Form Fields
  Icon: html
  Keywords: HTML Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $form = $block['form'];
  $block = get_field('block');
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6' }}">
    <div class="form-group form-group-html">
      @if($block['content'])
        <div id="{{ $block['id'] }}" class="{{ $block['custom_css_class'] }}">
          {!! $block['content'] !!}
        </div>
      @endif
    </div>
  </div>
@endif

