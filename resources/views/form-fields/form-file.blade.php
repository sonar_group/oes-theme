{{--
  Title: File Upload Field
  Description: File Upload Field - Form Fields
  Icon: upload
  Keywords: Dropdown Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $form = $block['form'];
  $block = get_field('block');
  $form_settings = get_field('settings', 'options');
@endphp

@if($block)
    <div class="form-file {{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
        @if($block['label'])
            <div>{{ $block['label'] }}
                @if($block['required'])
                    <span class="required-field" title="Required">*</span>
                @endif
            </div>
        @endif
        <div class="mb-3">
            <div class="input-group">
                @if($block['name'])
                    <div class="custom-file">
                        <input type="file" {{ ($block['required']) ? 'required' : '' }} id="{{ $block['id'] }}"
                               name="{{ $block['name'] }}"
                               class="custom-file-input {{ $block['custom_css_class'] }}"
                               title="{{ $block['title'] }}"
                               accept="{{ isset($form_settings['file_upload_mime_type']) && $form_settings['file_upload_mime_type'] != false ? $form_settings['file_upload_mime_type'] : '*' }}"
                               {!! isset($block['multiple_file']) && $block['multiple_file'] === true ? 'multiple' : '' !!}
                               hidden
                        >
                        <label for="{{ $block['id'] }}" class="custom-file-label">{!! isset($block['placeholder']) && strlen($block['placeholder']) > 0 ? $block['placeholder'] : 'Choose file' !!}
                            @if($block['required'])
                                <span class="required-field" title="Required">*</span>
                            @endif
                        </label>
                    </div>
                @endif
            </div>
            <div class="form-file-description d-flex align-items-center"></div>
        </div>
    </div>
@endif
