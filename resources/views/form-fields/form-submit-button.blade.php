{{--
  Title: Submit Button
  Description: Submit Button - Form Fields
  Icon: button
  Keywords: Submit Button
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $form = $block['form'];
  $block = get_field('block');
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
    <div class="form-group form-group-submit text-{{ $block['alignment'] }}">
      @if($block['label'])
        <button type="submit" name="submit" class="btn {{ $block['custom_css_class'] }}">{{ $block['label'] }}</button>
      @endif
    </div>
    <div class="form-group">
      <div class="message"></div>
    </div>
  </div>
@endif