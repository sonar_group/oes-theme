{{--
  Title: Text Field
  Description: Text Field - Form Fields
  Icon: text-page
  Keywords: Text Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $form = $block['form'];
  $block = get_field('block');
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
    <div class="form-group form-group-text">
      @if($block['label'])
        <label for="{{ ($block['name'])? $block['name']: '' }}">{{ $block['label'] }}
          @if($block['required'])
            <span class="required-field" title="Required">*</span>
          @endif
        </label>
      @endif

      @if($block['name'])
        <input type="text" {{ ($block['required'])? 'required': '' }} id="{{ $block['id'] }}"
               name="{{ $block['name'] }}"
               class="form-control {{ $block['custom_css_class'] }}" placeholder="{{ $block['placeholder'] }} {{ ($block['required'])? '*': '' }}"
               title="{{ $block['title'] }}">
      @endif
    </div>
  </div>
@endif