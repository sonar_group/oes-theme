{{--
  Title: Dropdown Field - Courses
  Description: Dropdown Field - Form Fields - Courses
  Icon: welcome-learn-more
  Keywords: Dropdown Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
    $id = $block['post_id'];
    $form = $block['form'];
    $block = get_field('block');

    $parent_courses = new WP_Query([
        'post_type' => 'course',
        'post_status' => ['publish'],
        'post_parent' => 0,
        'nopaging' => true,
        'fields' => 'ids'
    ]);
    $courses = new WP_Query([
        'post_type' => 'course',
        'post_status' => 'publish',
        'post__not_in' => $parent_courses->posts,
        'nopaging' => true,
    ]);
    $courses_result = [];
    foreach ($courses->posts as $course) {
        $course_code = get_field('course_code', $course->ID);
        $courses_result[] = [
            'value' => $course_code ? : $course->post_name,
            'key' => $course->post_title
        ];
    }
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
    <div class="form-group form-group-dropdown">
      @if($block['label'])
        <label for="{{ ($block['name'])? $block['name']: '' }}">{{ $block['label'] }}
          @if($block['required'])
            <span class="required-field" title="Required">*</span>
          @endif
        </label>
      @endif

      @if($block['name'])
        <select name="{{ $block['name'] }}" id="{{ $block['id'] }}"
                class="form-control {{ $block['custom_css_class'] }}"
                title="{{ $block['title'] }}" {{ ($block['required'])? 'required': '' }} >
        <option value="" selected="selected">--None--</option>
        @if(!empty($courses_result))
          @foreach($courses_result as $course)
            <option value="{{ $course['value'] }}">{{ $course['key'] }}</option>
          @endforeach
        @endif
        </select>
      @endif
    </div>
  </div>
@endif

