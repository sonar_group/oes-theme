{{--
  Title: Hidden Field
  Description: Hidden Field - Form Fields
  Icon: hidden
  Keywords: Hidden Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php
  $id = $block['post_id'];
  $form = $block['form'];
  $block = get_field('block');
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
    <div class="form-group form-group-hidden d-none">
      @if($block['name'])
        @if($block['dynamic_value'])
          <input type="hidden" id="{{ $block['id'] }}" name="{{ $block['name'] }}"
                 class="form-control {{ $block['custom_css_class'] }}"
                 value="{{ Form::getHiddenField($block['preselected_value']) }}">
        @else
          <input type="hidden" id="{{ $block['id'] }}" name="{{ $block['name'] }}"
                 class="form-control {{ $block['custom_css_class'] }}" value="{{ $block['value'] }}">
        @endif
      @endif
    </div>
  </div>
@endif