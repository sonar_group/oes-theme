{{--
  Title: Dropdown Field - Predefined Options
  Description: Dropdown Field - Form Fields - Predefined Options
  Icon: editor-ul
  Keywords: Dropdown Field
  Category: form-fields
  Mode: edit
  SupportsMode: false
--}}

@php

    $id = $block['post_id'];
    $form = $block['form'];
    $block = get_field('block');
    $options = [];
    if ($block['predefined_options'] === 'country') {
        $options = include get_template_directory() . '/../app/Modules/Forms/countries.php';
        asort($options);
        $options = array_map(function ($country) {
            return [
                'label' => $country,
                'value' => $country,
            ];
        }, $options);
    }
    elseif (strlen($block['predefined_options']) > 0) {
        $options = get_field($block['predefined_options'], 'options');
    }
@endphp

@if($block)
  <div class="{{ ($block['size']) == 'full'? 'col-12': 'col-lg-6'  }}">
    <div class="form-group form-group-dropdown">
      @if($block['label'])
        <label for="{{ ($block['name'])? $block['name']: '' }}">{{ $block['label'] }}
          @if($block['required'])
            <span class="required-field" title="Required">*</span>
          @endif
        </label>
      @endif

      @if($block['name'])
        <select name="{{ $block['name'] }}" id="{{ $block['id'] }}"
                class="form-control {{ $block['custom_css_class'] }}"
                title="{{ $block['title'] }}" {{ ($block['required'])? 'required': '' }} >
        <option value="" selected="selected">--None--</option>
        @if(!empty($options))
          @foreach($options as $item)
            <option value="{{ $item['value'] }}">{!! $item['label'] !!}</option>
          @endforeach
        @endif
        </select>
      @endif
    </div>
  </div>
@endif

