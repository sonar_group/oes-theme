<section class="block sticky-bar" id="stickyBar">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 d-flex align-items-center justify-content-center">
          <span class="sticky-bar__content">
            {!! $fields['message'] !!}
          </span>
        @if($fields['countdown_button'] === 'on')
          @php
            $now = new DateTime();
            $date = $fields['countdown'];
            $date = new DateTime( $date );
            $interval = $now->diff($date);
            echo
              '<span class="sticky-bar__countdown"><p>
              ' . $interval->format( '%d' ) . ' Day,
              ' . $interval->format( '%h' ) . ' Hours &
              ' . $interval->format( '%i' ) . ' Minutes
              </p></span>';
          @endphp
        @endif
      </div>
    </div>
  </div>
  <span id="stickyBarClose" type="button" class="sticky-bar__close">
    <span aria-hidden="true">&times;</span>
  </span>
</section>

