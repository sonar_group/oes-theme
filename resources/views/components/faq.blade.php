@php
  $i = 0;
@endphp


  <div class="accordion faqs-group" id="faqsAccordion_{{$parent}}">
    @foreach($faqs as $block_content)
      @php $blocks = App::parseBlock($block_content, 'single-faq'); @endphp

      @if($blocks)
        @foreach($blocks as $faq)
          @php $i++ @endphp

          <div class="card">
            <div class="card-header {{ $i !== 0 ? 'collapsed' : '' }}" id="heading_{{$parent}}_{{$i}}" data-toggle="collapse"
                 data-target="#collapse_{{$parent}}_{{$i}}" aria-expanded="true" aria-controls="collapse_{{$parent}}_{{$i}}">
              <h5>{{ $faq['block_question'] }}</h5>
            </div>

            <div id="collapse_{{$parent}}_{{$i}}" class="collapse {{ $i === 0 ? 'show' : '' }}" aria-labelledby="heading_{{$parent}}_{{$i}}"
                 data-parent="#faqsAccordion_{{$parent}}">
              <div class="card-body">
                {!! $faq['block_answer'] !!}
              </div>
            </div>
          </div>

        @endforeach
      @endif

    @endforeach
  </div>

