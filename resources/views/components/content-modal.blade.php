<div class="modal fade" id="contentModal_{{$r}}" tabindex="-1" role="dialog" aria-labelledby="contentModalLabel"
     aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <span type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </span>
      {!! $content !!}
    </div>
  </div>
</div>
