@php
    $getCountdowns = App::getCountdowns();
    $post_id =  get_the_id();
@endphp

<div class="course-buttons" id="courseButtons">
  @if($fields['inpage']['apply_button'] && $fields['inpage']['apply_button'] === 'on')
    <button class="btn" id="applyBtn" data-toggle="modal" data-target="#applyModal">
      <span>Apply Now</span>
      @foreach($getCountdowns as $countdown)
        @if(in_array($post_id, $countdown['posts']))
          @if($countdown['show_countdown'] === true)
            @php
              date_default_timezone_set('Australia/Melbourne');
              $today = strtotime(date('Y-m-d'));
              $startDate = strtotime($countdown['start_date']);
              $cutOffDate = strtotime($countdown['cut_off_date']);
              $remainingDays = round(($cutOffDate - $today) / (60 * 60 * 24));
            @endphp
            @if($today > $startDate && $today < $cutOffDate)
              <span>{{$remainingDays}} {!! ($remainingDays >= 2) ? 'days' : 'day' !!} remaining</span>
            @endif
          @endif
        @endif
      @endforeach
    </button>
  @endif
  @if($fields['inpage']['brochure_button'] && $fields['inpage']['brochure_button'] === 'on')
    <button class="btn" id="brochureBtn" data-toggle="modal" data-target="#brochureModal"><span>Download Brochure</span>
    </button>
  @endif
</div>
