@if($countdown['apply_button_only'] === false)
<section class="block countdown" id="countdownStickyBar">
  <div class="container-fluid">
    <div class="row">
      @if(!empty($countdown['link']['url']))
        <a class="col-12 d-flex align-items-center justify-content-center" href="{{$countdown['link']['url']}}">
      @else
        <div class="col-12 d-flex align-items-center justify-content-center">
      @endif
        <div class="countdown__content">
          <span class="caption">{{$countdown['caption']}}</span><br />
          @if($countdown['show_countdown'] === true)
            @php
              date_default_timezone_set('Australia/Melbourne');
              $today = strtotime(date('Y-m-d'));
              $startDate = strtotime($countdown['start_date']);
              $cutOffDate = strtotime($countdown['cut_off_date']);
              $remainingDays = round(($cutOffDate - $today) / (60 * 60 * 24));
            @endphp
            @if($today > $startDate && $today < $cutOffDate)
              <span>{{$remainingDays}} {!! ($remainingDays >= 2) ? 'days' : 'day' !!} remaining</span>
            @endif
          @endif
        </div>
        {!! isset($countdown['link']) ? '</a>' : '</div>' !!}
    </div>
    </div>
  <span id="countdownClose" type="button" class="countdown__close">
    <span aria-hidden="true">&times;</span>
  </span>
</section>
@endif
