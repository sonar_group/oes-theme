@php
  if (isset($layout)) {
      $columnClass = $layout;
  } elseif (count($tile) === 1) {
      $columnClass = 'col-12';
  } elseif (count($tile) === 2) {
      $columnClass = 'col-md-6';
  } else {
      switch (count($tile) % 4) {
          case 1:
          case 2:
          case 3:
              $columnClass = 'col-md-4';
              break;
          case 0:
              $columnClass = 'col-md-4';
              break;
          default:
              $columnClass = 'col-12';
              break;
      }
  }
@endphp

@if(isset($link['url']))
  <a class="column column-link {{$columnClass}} {{$blockClass}}"
     href="{!! $link['url'] !!}"
     target="{!! $link['target'] !!}" style="{!! (!empty($backgroundColour))? 'background-color:' . $backgroundColour .';': '' !!}{!! (isset($backgroundImage['url']))? 'background-image:url(' . $backgroundImage['url'] .');': '' !!}">
    @else
      <div class="column {{$columnClass}} {{$blockClass}}" style="{!! (!empty($backgroundColour))? 'background-color:' . $backgroundColour .';': '' !!}{!! (isset($backgroundImage['url']))? 'background-image:url(' . $backgroundImage['url'] .');': '' !!}">
        @endif

        <div class="content">

          @if(isset($image))
            <div class="bg-image" style="background-image: url({!! $image['url'] !!});"></div>
          @endif

          @if(isset($heading))
            <h4>{{ $heading }}</h4>
          @endif

          @if(isset($role))
            <p>{{ $role }}</p>
          @endif

          @if(isset($copy))
            {!! $copy !!}
          @endif

          @if(isset($link['url']))

            <div class="link mt-auto">
                <span class="{{$btnClass}}">
                  <span>{{ $link['title'] }}</span>
                </span>
            </div>
          @endif

        </div>
      @if(isset($link['url']))
  </a>
  @else
  </div>

@endif

