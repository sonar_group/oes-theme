<div class="modal fade" id="teamModal_{{$title}}" tabindex="-1" role="dialog" aria-labelledby="teamModalLabel"
     aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <span type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </span>
      @if (!empty($team['block_image']))
        <img src="{{ wp_get_attachment_url($team['block_image']) }}" loading="lazy" alt="">
      @endif
      @if($team['block_name'])
        <h2>{{ $team['block_name'] }}</h2>
      @endif
      @if($team['block_role'])
        <h3>{{ $team['block_role'] }}</h3>
      @endif
      @if($team['block_long_bio'])
        {!! $team['block_long_bio'] !!}
      @endif
    </div>
  </div>
</div>
