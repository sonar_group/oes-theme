<div class="modal fade unit-modal" id="unitModal_{{$i}}_{{$code}}" tabindex="-1" role="dialog"
     aria-labelledby="unitModal_{{$i}}_{{$code}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </span>
        <p>{{$code}}<br/>{{$name}}</p>
      </div>
      <div class="modal-body">
        @if($code)
          <p>Unit Code:<span>{{ $code }}</span></p>
        @endif
        @if($contact_hours)
          <p>Contact Hours:<span>{{ $contact_hours }}</span></p>
        @endif
        @if($description)
          <p>Description:</p>
          {!! $description !!}
        @endif
      </div>
    </div>
  </div>
</div>
