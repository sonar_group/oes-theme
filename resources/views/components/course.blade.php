<a href="{{ $item['permalink'] }}" class="col-12 col-md-4 course">
  <div class="content">
    @if (!empty($item['image']))
      <img src="{{ $item['image'] }}" loading="lazy" alt="">
    @endif
    <div>{{ $item['title'] }}</div>
    <div>{{ $item['term'] }}</div>
  </div>
</a>
