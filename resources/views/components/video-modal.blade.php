<div class="modal fade" id="videoModal{{$modalName}}_{{$r}}" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel"
     aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <span type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </span>
      <iframe src="{!! $video !!}" frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen width="100%" height="500px"></iframe>
    </div>
  </div>
</div>
