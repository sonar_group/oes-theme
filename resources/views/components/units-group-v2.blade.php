{!! $content['copy'] !!}

@if($content['feature_unit'])
  <div class="feature-unit">
    @php
      $block_content = $content['feature_unit'];
      $blocks = App::parseBlock($block_content, 'single-unit');
    @endphp
    @if($blocks)
      @foreach($blocks as $feature)

        <h2>{{$feature['block_name']}}</h2>
        <p>{{$feature['block_code']}}</p>
        <p>{{$feature['block_description']}}</p>

      @endforeach
    @endif
  </div>
@endif

@if($content['show_all_units_button'])
  <button>View All</button>
@endif
