<div class="accordion uni
ts-group" id="unitsAccordion">
  @foreach($units_group as $i => $group)
    <div class="card">
      <div class="card-header {{ $i !== 0 ? 'collapsed' : '' }}" id="heading{{$i}}" data-toggle="collapse"
           data-target="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
        <h5>{{(isset($group['heading']))? $group['heading']: ((isset($group['title']))? $group['title'] : '')}}</h5>
      </div>

      <div id="collapse{{$i}}" class="collapse {{ $i === 0 ? 'show' : '' }}" aria-labelledby="heading{{$i}}"
           data-parent="#unitsAccordion">
        <div class="card-body">
          @if($group['copy'])
            {!! $group['copy'] !!}
          @endif

            @if($group['units'])
              @foreach($group['units'] as $block_content)
                @php $blocks = App::parseBlock($block_content, 'single-unit'); @endphp

                @if($blocks)
                  @foreach($blocks as $unit)

                  <p><a data-toggle="modal"
                        data-target="#unitModal_{{$i}}_{{ $unit['block_code'] }}">{{ $unit['block_name'] }}</a></p>

                  @include('components.units-modal', [
                    'id' => $i,
                    'name' => $unit['block_name'],
                    'code' => $unit['block_code'],
                    'contact_hours' =>  $unit['block_contact_hours'],
                    'description' => $unit['block_description']
                  ])

                  @endforeach
                @endif
              @endforeach
            @endif

        </div>
      </div>
    </div>
  @endforeach
</div>
