<div class="accordion" id="accordion_{{$parent}}">
  @foreach($accordion as $i => $item)
  <div class="card">
    <div class="card-header {{ $i !== 0 ? 'collapsed' : '' }}" id="heading_{{$parent}}_{{$i}}" data-toggle="collapse"
         data-target="#collapse_{{$parent}}_{{$i}}" aria-expanded="true" aria-controls="collapse_{{$parent}}_{{$i}}">
      <h5>{{$item['title']}}</h5>
    </div>

    <div id="collapse_{{$parent}}_{{$i}}" class="collapse {{ $i === 0 ? 'show' : '' }}"
         aria-labelledby="heading_{{$parent}}_{{$i}}"
         data-parent="#accordion_{{$parent}}">
      <div class="card-body">
        {!! $item['content'] !!}
      </div>
    </div>
  </div>
  @endforeach
</div>

