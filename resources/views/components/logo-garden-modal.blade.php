<div class="modal fade" id="logoGardenModal_{{$title}}" tabindex="-1" role="dialog" aria-labelledby="logoGardenModalLabel"
     aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <span type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </span>
      @if (!empty($logo_garden['block_image']))
        <img src="{{ wp_get_attachment_url($logo_garden['block_image']) }}" loading="lazy" alt="">
      @endif
      @if($logo_garden['block_name'])
        <h2>{{ $logo_garden['block_name'] }}</h2>
      @endif
      @if($logo_garden['block_long_bio'])
        {!! $logo_garden['block_long_bio'] !!}
      @endif
    </div>
  </div>
</div>
