<div class="row">
  <div class="col-5">
    <img src="{!! ($content['image'])? $content['image']['url']: '' !!}" class="img-fluid">
  </div>
  <div class="col-7">
    <p>{{$content['name']}}</p>
    <p>{{$content['role']}}</p>
    <p>{{$content['copy']}}</p>
  </div>
</div>
