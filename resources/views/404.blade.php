@extends('layouts.app')

@section('content')

  @php
  $notfound = App::notFound();
  @endphp

  <section class="block 404">
    <div class="container">
      <div class="alert alert-warning">
        @if($notfound != '')
          @if(isset($notfound['heading']) && $notfound['heading'] != "")
            <h2>{!! $notfound['heading'] !!}</h2>
          @endif
          @if(isset($notfound['content']) && $notfound['content'] != "")
            {!! $notfound['content'] !!}
          @endif
          @if(isset($notfound['link']) && $notfound['link'] != "")
            <a class="btn" href="{{ $notfound['link']['url'] }}" target="{{ $notfound['link']['target'] }}">
              {{ $notfound['link']['title'] }}
            </a>
          @endif
        @endif
      </div>
    </div>
  </section>

@endsection
