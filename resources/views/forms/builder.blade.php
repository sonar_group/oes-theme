@php
  global $form_id;

  $form_id = ($form_name)? $form_name->ID : false;
  $form = Form::getData($form_id);
  $recaptcha = App::recaptcha();
  $salesforce = App::salesforce();
@endphp

@if($form_name)

  @if($post->post_type != 'course' && $post->post_parent != true)
    <script type='text/javascript' src='{{ \App\google_recaptcha_url() }}'></script>
  @endif

  <section class="block form {{ $form['wrapper_class'] }}">
    <div class="container">
      <form method="POST" class="form-builder-form form-data {{ $form['form_class'] }}" id="{{ $form['form_id'] }}" _lpchecked="1">
        <input type="hidden" name="action" value="form_builder_submit">
        <input type="hidden" name="form_id" value="{{ $form_id }}">
        <div class="row">
          {!! apply_filters('the_content', get_post_field('post_content', $form_id)) !!}
        </div>
        <div class="row">
          <div class="g-recaptcha" data-sitekey="{{ $recaptcha['site_key'] }}"></div><br>
        </div>
        <p class="message"></p>
      </form>
    </div>
  </section>

@endif
