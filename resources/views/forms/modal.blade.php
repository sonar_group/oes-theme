<div class="modal fade form-modal" id="{{$title}}" tabindex="-1" role="dialog" aria-labelledby="{{$title}}Label"
     aria-hidden="true">
  <div class="modal-dialog modal-fullscreen" role="document">
    <div class="modal-content">
      <span type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </span>
      <div class="row">
        @if($form === 'apply')
          <div class="col-md-6">
            <h2>Apply for {{get_the_title()}}</h2>
            @if(isset($fields['forms']['apply_form_content']))
              {!! $fields['forms']['apply_form_content'] !!}
            @endif
          </div>
          <div class="col-md-6">

            @if($fields['inpage']['legacy'] === 'on')
              @include('forms.apply')
            @else
              @include('forms.builder', [ 'form_name' => $fields['inpage']['apply_form']])
            @endif


          </div>
        @endif
        @if($form === 'brochure')
          <div class="col-md-6">

            @if($fields['inpage']['register_interest'] && $fields['inpage']['register_interest'] === 'on')
              @if(isset($fields['forms']['register_form_content']))
                {!! $fields['forms']['register_form_content'] !!}
              @endif
            @else
              @if(isset($fields['forms']['brochure_form_content']))
                {!! $fields['forms']['brochure_form_content'] !!}
              @endif
            @endif

          </div>
          <div class="col-md-6">
            @if($fields['inpage']['legacy'] === 'on')
              @include('forms.brochure')
            @else
              @if($fields['inpage']['register_interest'] === 'on')
                @include('forms.builder', [ 'form_name' => $fields['inpage']['register_form']])
              @else
                @include('forms.builder', [ 'form_name' => $fields['inpage']['brochure_form']])
              @endif
            @endif
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
