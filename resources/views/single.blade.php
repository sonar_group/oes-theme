@extends('layouts.app')

@section('content')
  @while(have_posts())
  @include('partials.content.content-single-'.get_post_type())
  @endwhile
@endsection
