@if($footer['toggles']['content'])
  <div class="footer__content">
    @if($footer['content'])
      @foreach($footer['content'] as $content)
        <p>{!! $content['text'] !!}</p>
      @endforeach
    @endif
  </div>
@endif
