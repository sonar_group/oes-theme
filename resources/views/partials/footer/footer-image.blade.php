@if($footer['toggles']['footer_image'])
  <div class="footer__image">
    @if(isset($footer['footer_image']['url']))
      <img class="footer__image-img img-fluid" src="{{ $footer['footer_image']['url'] }}"
           alt="{{ $footer['footer_image']['title'] }}">
    @endif
  </div>
@endif
