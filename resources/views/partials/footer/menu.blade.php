@if($footer['toggles']['menu'])
  <div class="footer__menu">
    <nav class="footer__menu-nav">
      @if (has_nav_menu('footer_nav'))
        {!! wp_nav_menu(['theme_location' => 'footer_nav', 'menu_class' => 'footer__menu-menu', 'container' => '']) !!}
      @endif
    </nav>
  </div>
@endif
