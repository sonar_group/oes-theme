@if($footer['toggles']['ctas'])
  <section class="footer__ctas">
    <div class="container">
      <div class="row">
        <div class="col-12">
          @if($footer['ctas'])
            @foreach($footer['ctas'] as $cta)
              @if(isset($cta['link']['url']))
                <a class="btn" href="{{ $cta['link']['url'] }}" target="{{ $cta['link']['target'] }}">
                  {{ $cta['link']['title'] }}
                </a>
              @endif
            @endforeach
          @endif
        </div>
      </div>
    </div>
  </section>
@endif
