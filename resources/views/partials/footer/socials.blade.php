@if($footer['toggles']['socials'])
  <div class="footer__socials">
    @if($footer['socials'])
      <ul>
        @if($footer['socials']['facebook'])
          <li>
            <a href="{{ $footer['socials']['facebook'] }}" class="social-link facebook" target="_blank">
              <span>Facebook</span>
            </a>
          </li>
        @endif
        @if($footer['socials']['twitter'])
          <li>
            <a href="{{ $footer['socials']['twitter'] }}" class="social-link twitter" target="_blank">
              <span>Twitter</span>
            </a>
          </li>
        @endif
        @if($footer['socials']['instagram'])
          <li>
            <a href="{{ $footer['socials']['instagram'] }}" class="social-link instagram" target="_blank">
              <span>Instagram</span>
            </a>
          </li>
        @endif
        @if($footer['socials']['linkedin'])
          <li>
            <a href="{{ $footer['socials']['linkedin'] }}" class="social-link linkedin" target="_blank">
              <span>Linkedin</span>
            </a>
          </li>
        @endif
      </ul>
    @endif
  </div>
@endif
