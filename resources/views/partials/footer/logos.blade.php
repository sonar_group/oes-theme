@if($footer['toggles']['logos'])
  <div class="footer_logos">
    @if($footer['logos']['first'])
      <div class="footer__first-image">
        @if(isset($footer['logos']['first']['url']))
          <img class="footer-image img-fluid" src="{{ $footer['logos']['first']['url'] }}"
               alt="{{ $footer['logos']['first']['title'] }}">
        @endif
      </div>
    @endif

    @if($footer['logos']['second'])
      <div class="footer__second-image">
        @if(isset($footer['logos']['second']['url']))
          <img class="footer-image img-fluid" src="{{ $footer['logos']['second']['url'] }}"
               alt="{{ $footer['logos']['second']['title'] }}">
        @endif
      </div>
    @endif
  </div>
@endif
