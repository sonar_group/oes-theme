@if($footer['toggles']['legend'])
  <div class="footer__legend">
    {!! $footer['legend'] !!}
  </div>
@endif
