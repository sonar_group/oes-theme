@if($footer['toggles']['credits'])
  <div class="footer__credits">
    @if($footer['credits'])
      <ul class="footer__credits-list">
        @foreach($footer['credits'] as $content)
          <li>{!! $content['text'] !!}</li>
        @endforeach
      </ul>
    @endif
  </div>
@endif