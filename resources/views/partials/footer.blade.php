@if($footer)
  @include('partials.footer.ctas', $footer)
  <footer class="footer">
    <div class="container">
      <div class="row footer__top">
        <div class="col-12 col-md-8 col-lg-9 order-md-1 order-sm-12 footer__top-one">
          <div class="row">
            <div class="col-12 col-md-6 order-sm-12 footer__top-right">
              @include('partials.footer.content', $footer)
              @include('partials.footer.socials', $footer)
            </div>
            <div class="col-12 col-md-6 order-sm-1 footer__top-left">
              @include('partials.footer.legend', $footer)
            </div>
          </div>

          <div class="footer__top-bottom">
            @include('partials.footer.footer-image', $footer)
          </div>

        </div>
        <div class="col-12 col-md-4 col-lg-3 order-md-12 order-sm-1 footer__top-two">
          @include('partials.footer.logos', $footer)
        </div>
      </div>

      <div class="row footer__bottom">
        <div class="col-12 col-md-6 order-sm-12 footer__bottom-right">
          @include('partials.footer.credits', $footer)
        </div>
        <div class="col-12 col-md-6 order-sm-1 footer__bottom-left">
          @include('partials.footer.menu', $footer)
        </div>
      </div>

    </div>


  </footer>
@endif
