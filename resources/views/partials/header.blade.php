@php
  use App\Menu\Walker;
  $stickyBarGlobal = App::stickyBar();
  $stickyBarCourse = App::stickyBarCourse();
  $getCountdowns = App::getCountdowns();
  $post_id =  get_the_id();
  $post = get_post($post_id);
@endphp

<header class="main-header {{ (isset($header) && $header['toggles']['header_home'])? (is_front_page()? 'home' : '') : '' }}">
  @if($header)

    @if($header['toggles']['top_menu'])
      <div class="top-header d-none d-md-block">
        <div class="container-fluid">
          <div class="row">
            <div class="col-4">
              @if($header['toggles']['top_label'] && $header['top_label'])
                <p class="top-header__label">{{ $header['top_label'] }}</p>
              @endif
            </div>
            <div class="col-8">
              <nav class="top-header__nav text-right">
                @if (has_nav_menu('top_nav'))
                  {!! wp_nav_menu(['theme_location' => 'top_nav', 'menu_class' => 'top-header__menu', 'container' => '' ]) !!}
                @endif
              </nav>
            </div>
          </div>
        </div>
      </div>
    @endif

    <div class="main-header__wrap">
      <a class="logo d-block" href="{{ home_url('/') }}">
        @if($header['toggles']['logo'] && $header['logo'])
          <img src="{{ $header['logo']['url'] }}" alt="{{ get_bloginfo('name', 'display') }}"
               class="logo__main img-fluid">

          @if($header['toggles']['header_home'] && $header['logo_home'] )
            <img src="{{ $header['logo_home']['url'] }}" alt="{{ get_bloginfo('name', 'display') }}"
                 class="logo__home img-fluid">
          @endif
        @else
          {{ get_bloginfo('name', 'display') }}
        @endif
      </a>

      @if($header['toggles']['search'])
        <a href="{{ home_url('/') }}" class="search-button">
          <span>Search</span>
        </a>
      @endif


      @if($header['toggles']['apply_button'] && $header['apply_button']['link'])

        <a href="{{ $header['apply_button']['link']['url'] }}" class="apply-button"
           target="{{ $header['apply_button']['link']['target'] }}">
          <span>{{ $header['apply_button']['link']['title'] }}</span>
        </a>
      @endif

    </div>

    @if($header['toggles']['main_menu'])
      <div class="main_menu">
        <nav class="main-menu__nav">
          @if (has_nav_menu('main_nav'))
            {!! wp_nav_menu(['theme_location' => 'main_nav', 'menu_class' => 'main-menu__menu', 'container' => '' ,'walker' => new Walker]) !!}
          @endif
        </nav>

        @if($header['toggles']['content'] && $header['content'])
          <div class="main-menu__content">
            <ul class="main-menu__list">
              @foreach($header['content'] as $content)
                <li>{!! $content['text'] !!}</li>
              @endforeach
            </ul>
          </div>
        @endif

      </div>
    @endif

  @endif
</header>

@foreach($getCountdowns as $countdown)
  @if(in_array($post_id, $countdown['posts']))
    @include('components.countdown', ['countdown' => $countdown])
  @endif
@endforeach

@if($post->post_type == 'course' && $stickyBarCourse['override_sticky_bar'] === 'on' && $stickyBarCourse['status'] === 'enable')
  @include('components.sticky-bar', ['fields' => $stickyBarCourse])

@elseif($post->post_type == 'course' && $stickyBarGlobal['sticky_bar'] === 'on' && $stickyBarCourse['status'] === 'enable')
  @include('components.sticky-bar', ['fields' => $stickyBarGlobal])

@elseif(($post->post_type == 'page' || $post->post_type == 'post') && $stickyBarGlobal['sticky_bar'] === 'on')
  @include('components.sticky-bar', ['fields' => $stickyBarGlobal])
@endif
