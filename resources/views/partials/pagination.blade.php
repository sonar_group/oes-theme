@php
    $pagination_links = App::pagination();
@endphp

@if (!empty( $pagination_links))
    {!! $pagination_links !!}
@endif