@php
    the_post();
    global $post, $form_id;
    $form_id = $post->ID;
    $settings = (array) $settings;
    $salesforce = App::salesforce();
@endphp

@if($post->post_type != 'course' && $post->post_parent != true)
    <script type='text/javascript' src='{{ \App\google_recaptcha_url() }}'></script>
@endif

<section class="block content-single">
    <div class="container">
        <article @php post_class() @endphp>
            <header>
                <h1 class="entry-title">{!! get_the_title() !!}</h1>
            </header>
            <div class="entry-content">
                <section class="block form {{ $settings['wrapper_class'] }}">
                    <div class="container">
                        <form method="POST" class="form-builder-form form-data {{ $settings['form_class'] }}" id="form-{{$form_id}}" _lpchecked="1">
                            <input type="hidden" name="action" value="form_builder_submit">
                            <input type="hidden" name="form_id" value="{{ $form_id }}">
                            <div class="row">
                                @php the_content() @endphp
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </article>
    </div>
</section>
