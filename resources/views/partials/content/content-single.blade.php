@php the_post() @endphp
<section class="block content-single">
  <div class="container">
    <article @php post_class() @endphp>
      <header>
        <h1 class="entry-title">{!! get_the_title() !!}</h1>
        @include('partials.entry-meta')
      </header>
      <div class="entry-content">
        @php the_content() @endphp
      </div>
    </article>
  </div>
</section>
