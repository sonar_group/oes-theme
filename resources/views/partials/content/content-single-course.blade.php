@php Course::redirect() @endphp
@php the_post() @endphp
<section class="block content-single">
  <article @php post_class() @endphp>
    <div class="entry-content">

      @if(Course::isUnitPage())
        @include('partials.content.content-single-course-units')
      @else
        @php the_content() @endphp
      @endif

    </div>
  </article>
</section>
