<?php
  the_post();

  $category = get_the_category();
  $catName = $category[0]->name;
?>

<section class="news-banner mb-0">
  <div class="container">
    <div class="row text-white">
      <div class="col-md-6">
        <h3 class="h2 mb-0">News</h3>
      </div>
    </div>
  </div>
</section>
<section class="block content-single">
  <article @php post_class() @endphp>
    <div class="container content-single__content">
      <div class="row">
        <div class="col-12">
          <header>
            <?php
              if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
              }
            ?>
            <h2 class="entry-title h1">{!! the_title() !!}</h2>
            <h1 class="seo-title">{{ get_field('subtitle') }}</h1>
            <div class="post-info">
              <p class="date">{{ get_the_date('M, Y')}}</p>
              <span class="spacer"></span>
              @if(!empty($read_time))
                <p class="time">Approximately {{$read_time}}</p>
              @endif
            </div>
          </header>
          <div class="content-single__banner">
            {!! get_the_post_thumbnail() !!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-8">
          <div class="entry-content">
            @php the_content() @endphp
          </div>

          @if (!empty($related_topics))
            <div class="news_single__topics">
              @foreach ($related_topics as $topic)
                <a class="small-label-outline" href="/news?catID={{$topic->term_id}}">{{$topic->name}}</a>
              @endforeach
            </div>
          @endif

          <div class="news_single__socials">
            <span class="fb-share-button" data-href="{{ get_the_permalink() }}" data-layout="button_count" data-size="small">
              <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ get_the_permalink() }}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore facebook"><img src="{{App\asset_path('images/post-facebook.svg')}}" alt="facebook"></a>
            </span>
            <a target="_blank" href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button twitter" data-show-count="false"><img src="{{App\asset_path('images/post-twitter.svg')}}" alt="twitter"></a>
            <a target="_blank" href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site {{ get_the_permalink() }}" class="email" title="Share by Email"><img src="{{App\asset_path('images/post-mail.svg')}}" alt="email"></a>
            <a target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url={{ get_the_permalink() }}" class="linkedin"><img src="{{App\asset_path('images/post-linkedin.svg')}}" alt="linkedin"></a>
          </div>
        </div>
        <div class="col-12 col-md-4">
          <div class="sidebar-wrapper">
            @if($fields['has_tiles'] == 'yes' && !empty($fields['tiles']))
              @foreach($fields['tiles'] as $i => $content)
                <div class="side-links">
                  <div class="item">
                    @if(isset($content['link']['url']))
                      <a class="link" href="{{ $content['link']['url'] }}">
                    @endif
                    @if(isset($content['title']))
                      <p class="title">{{ $content['title'] }}</p>
                    @endif
                    @if(isset($content['content']))
                      <p class="content">{!! $content['content'] !!}</p>
                    @endif
                    @if(isset($content['link']['url']))
                      </a>
                    @endif
                  </div>
                </div>
              @endforeach
            @endif
            @if($fields['has_form'] == 'yes')
              <div class="subscribe-form">
                <div class="form-header">
                  <h5>Get in touch</h5>
                </div>
                Add form here
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
    @if (!empty($related_news))
      <div class="related-news-wrapper">
        <div class="container">
          <h2>Related Articles</h2>
          <div class="row">
            @foreach ($related_news as $news)
              <div class="col-md-4 content-single__related-news">
                <a href="{{ $news['permalink'] }}">
                  @if (!empty($news['image']))
                    <img src="{{ $news['image'] }}" loading="lazy" alt="{{ $news['title'] }}">
                  @endif
                  <h4 class="title">{{ $news['title'] }}</h4>
                  <div class="category-wrapper">
                    <p class="small-label-outline">{{ $news['term'] }}</p>
                  </div>
                  <!-- <p class="small-label">{{$read_time}}</p> -->
                </a>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    @endif
  </article>
</section>
