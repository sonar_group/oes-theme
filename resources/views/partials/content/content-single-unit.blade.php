@php the_post() @endphp
<section class="block content-single">
  <article @php post_class() @endphp>
    <div class="entry-content">
      @php the_content() @endphp
    </div>
  </article>
</section>
