@php Course::unitsHeader() @endphp
@php $units = Course::units(); @endphp

<div class="units-breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ul class="units-breadcrumb">
          <li><a href="{{ home_url('/') }}">Home</a></li>
          <li><a href="{{ get_the_permalink(Course::unitId()) }}">{{get_the_title(Course::unitId())}}</a></li>
          <li><strong>All Units</strong></li>
        </ul>

      </div>
    </div>
  </div>
</div>

@if(Course::unitsOverview())
  <div class="units-overview">
    <div class="container">
      <div class="row">
        <div class="col-12">
          {!! Course::unitsOverview() !!}
        </div>
      </div>
    </div>
  </div>
@endif

@include('components.units-group', ['units_group' => $units ])

@if(Course::unitsOutcomes())
  <div class="units-lo">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <a class="btn btn-primary" data-toggle="collapse" href="#collapseLO" role="button" aria-expanded="false"
             aria-controls="collapseLO">
            View Course Learning Outcomes
          </a>

          <div class="collapse" id="collapseLO">
            <div class="card card-body">
              {!! Course::unitsOutcomes() !!}
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
@endif
