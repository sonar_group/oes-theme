<head>

  {!! App::headerScripts() !!}

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  @if($post ?? null)
    @if($post->post_type == 'course' && $post->post_parent == true)
      qq11
      <script type='text/javascript' src='{{ \App\google_recaptcha_url() }}'></script>
    @endif
  @endif

  @php wp_head() @endphp

</head>
